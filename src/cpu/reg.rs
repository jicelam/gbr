use std::ops::{AddAssign,SubAssign};
use std::u8;
use std::u16;

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum Reg {
	/* General purpose registers */
	A = 0, B = 1, C = 2, D = 3, E = 4,
	/* Flag register */
	F = 5,
	/* High and Low registers */
	H = 6, L = 7
}

impl Reg {
	pub fn to_string(&self) -> String {
		return match *self {
			Reg::A => String::from("A"),
			Reg::B => String::from("B"),
			Reg::C => String::from("C"),
			Reg::D => String::from("D"),
			Reg::E => String::from("E"),
			Reg::F => String::from("F"),
			Reg::H => String::from("H"),
			Reg::L => String::from("L")
		}
	}
}

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum RegFlag {
	Zero = 0b1000_0000,			//Set when a math operation results in 0, or when two values match during the CP instruction
	Subtract = 0b0100_0000,		//Set when the last operation performed was subtraction
	HalfCarry = 0b0010_0000,	//Set if carry occurred from lower nibble in the last operation
	Carry = 0b0001_0000			//Set if carry occurred from last operation, or if register A is the smaller value during the CP instruction,
}

impl RegFlag {
	pub fn to_string(&self) -> String {
		return match *self {
			RegFlag::Zero => String::from("Zero"),
			RegFlag::Subtract => String::from("Subtract"),
			RegFlag::HalfCarry => String::from("Half Carry"),
			RegFlag::Carry => String::from("Carry")
		}
	}
}

#[derive(Clone)]
pub struct Registers {
	regs: [u8; 8],
	/* Stack pointer */
	pub sp: RegPointer, 
	/* Program counter */
	pub pc: RegPointer
}

impl Registers {
	pub fn new() -> Registers {
		return Registers {
			regs: [0; 8],
			sp: RegPointer::new(0xFFEE), 
			pc: RegPointer::new(0x0100)
		}
	}

	pub fn power_up(&mut self) {
		self.set(Reg::A, 0x01); //GB/SGB; 0xFF -> GB Pocket; 0x11 -> GB Color
		self.set(Reg::B, 0x00);
		self.set(Reg::C, 0x13);
		self.set(Reg::D, 0x00);
		self.set(Reg::E, 0xD8);
		self.set(Reg::F, 0xB0);
		self.set(Reg::H, 0x01);
		self.set(Reg::L, 0x4D);
		self.sp.set(0xFFFE);
		self.pc.set(0x0100);
	}

	#[inline(always)]
	pub fn get(&self, reg: Reg) -> u8 {
		return self.regs[reg as usize];
	}

	#[inline(always)]
	pub fn set(&mut self, reg: Reg, value: u8) {
		self.regs[reg as usize] = value;
	}

	#[inline(always)]
	pub fn get_flag(&self, flag: RegFlag) -> bool {
		return (self.get(Reg::F) & flag as u8) > 0;
	}

	#[inline(always)]
	pub fn set_flag(&mut self, flag: RegFlag, value: bool) {
		let current_flags = self.get(Reg::F);
		self.set(Reg::F, if value {
			current_flags | flag as u8
		} else {
			current_flags & !(flag as u8)
		})
	}	

	/* Methods only available in unit test builds */
	#[cfg(test)]
	pub fn compare_regs<'a, CollectionType>(&self, other: &Registers, regs_to_compare: CollectionType) -> CompareRegsResult
		where CollectionType: IntoIterator<Item = &'a Reg> {
		for reg in regs_to_compare {
			let expected_value = other.get(*reg);
			let actual_value = self.get(*reg);
			if expected_value != actual_value {
				return CompareRegsResult::Different(*reg, expected_value, actual_value);
			}
		}
		return CompareRegsResult::Identical;
	}
}

#[cfg(test)]
pub enum CompareRegsResult { 
	Identical, 
	Different(Reg, u8, u8)
}

#[derive(Clone)]
pub struct RegPointer {
	value: u16
}

impl RegPointer {
	pub fn new(value: u16) -> RegPointer {
		return RegPointer { value };
	}

	#[inline(always)]
	pub fn get(&self) -> u16 {
		return self.value;
	}	

	#[inline(always)]
	pub fn set(&mut self, value: u16) {
		self.value = value;
	}
}

impl AddAssign<u16> for RegPointer {
	#[inline(always)]
	fn add_assign(&mut self, rh: u16) {
		self.value = self.value.wrapping_add(rh);
	}
}

impl SubAssign<u16> for RegPointer {
	#[inline(always)]
	fn sub_assign(&mut self, rh: u16) {
		self.value = self.value.wrapping_sub(rh);
	}
}