use std::ops::AddAssign;
use std::u64;

pub static CLOCK_SPEED: u64 = 4_194_304; //Cycles per second

pub struct Clock {
	ns: u64
}

impl Clock {
	pub fn new() -> Self {
		return Clock { ns: 0 };
	}

	#[inline(always)]
	pub fn get(&self) -> u64 {
		return self.ns;
	}

	#[inline(always)]
	pub fn set(&mut self, value: u64) {
		self.ns = value;
	}
}

impl AddAssign<u64> for Clock {
	#[inline(always)]
	fn add_assign(&mut self, rh: u64) {
		self.ns = self.ns.wrapping_add(rh);
	}
}
