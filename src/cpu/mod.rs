#![allow(dead_code)]

mod clock;
mod reg;
mod test;

pub mod cpu;