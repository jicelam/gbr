use std::thread::sleep;
use std::time::Duration;
use std::u8;

use log;
use mmu::mem::special_registers;
use mmu::mmu::MMU;

pub use super::reg::*;
use super::clock::*;

pub struct CPU {
	pub regs: Registers,
	pub mmu: MMU,

	pub clock: Clock,

	clock_speed: u64
}

impl CPU {
	pub fn new() -> CPUBuilder {
		return CPUBuilder::new();
	}

	pub fn get_clock_speed(&self) -> u64 {
		return self.clock_speed;
	}

	pub fn set_clock_speed(&mut self, value: u64) {
		self.clock_speed = value;
	}

	fn get_interrupt_enable(&self) -> bool {
		return self.mmu.load_word(special_registers::INTERRUPT_ENABLE) != 0;
	}

	fn set_interrupt_enable(&mut self, value: bool) {
		self.mmu.store_word(special_registers::INTERRUPT_ENABLE, value as u8);
	}

	pub fn load_rom(&mut self, rom: Vec<u8>) -> Result<(), String> {
		return self.mmu.load_rom(rom);
	}

	pub fn power_up(&mut self) {
		/* Scroll Nintendo graphic from cartridge */
		/* Play two musical notes */
		/* Compare Nintendo graphic with internal table */
		/* If not SGB, add all cartridge bytes from 0x0134 to 0x014D, and add 25.
		If least significant byte of result is not a 0, halt operations */

		self.regs.power_up();
		self.mmu.power_up();
	}

	//Halt
	pub fn power_down(&mut self) {
		self.clock += 4;
	}

	//Stop
	pub fn stop(&mut self) {
		self.clock += 4;
	}

	pub fn run_cycle(&mut self) {
		let instr_addr = self.fetch_instr();

		let instr = self.mmu.load_word(instr_addr);
		self.execute_instr(instr);

		log::info(format!("Executed instruction 0x{:02x} from address 0x{:04x}", instr, instr_addr));

		//TODO: Have this account for actual execution time
		sleep(Duration::from_secs(1 / self.clock_speed));
	}

	fn fetch_instr(&mut self) -> u16 {
		let instr_addr = self.regs.pc.get();
		self.regs.pc.set(instr_addr + 1);

		return instr_addr;
	}

	fn execute_instr(&mut self, instr: u8) {
		match instr {
			0x00 => self.nop(),
			0x01 => self.load_reg_from_imm16(Reg::B, Reg::C),
			0x02 => self.load_hilo_addr_from_reg(Reg::B, Reg::C, Reg::A),
			0x03 => self.incr_hilo(Reg::B, Reg::C),
			0x04 => self.incr_reg(Reg::B),
			0x05 => self.decr_reg(Reg::B),
			0x06 => self.load_reg_from_imm8(Reg::B),
			0x07 => self.rotate_reg_left(Reg::A, 4),
			0x08 => self.load_imm_addr_from_sp(),
			0x09 => self.add_hilo1_with_hilo2(Reg::H, Reg::L, Reg::B, Reg::C),
			0x0A => self.load_reg_from_hilo_addr(Reg::A, Reg::B, Reg::C),
			0x0B => self.decr_hilo(Reg::B, Reg::C),
			0x0C => self.incr_reg(Reg::C),
			0x0D => self.decr_reg(Reg::C),
			0x0E => self.load_reg_from_imm8(Reg::C),
			0x0F => self.rotate_reg_right(Reg::A, 4),
			0x10 => self.execute_long_instr(),
			0x11 => self.load_reg_from_imm16(Reg::D, Reg::E),
			0x12 => self.load_hilo_addr_from_reg(Reg::D, Reg::E, Reg::A),
			0x13 => self.incr_hilo(Reg::D, Reg::E),
			0x14 => self.incr_reg(Reg::D),
			0x15 => self.decr_reg(Reg::D),
			0x16 => self.load_reg_from_imm8(Reg::D),
			0x17 => self.rotate_reg_left_through_carry(Reg::A, 4),
			0x18 => self.jump_to_current_addr_plus_imm8(),
			0x19 => self.add_hilo1_with_hilo2(Reg::H, Reg::L, Reg::D, Reg::E),
			0x1A => self.load_reg_from_hilo_addr(Reg::A, Reg::D, Reg::E),
			0x1B => self.decr_hilo(Reg::D, Reg::E),
			0x1C => self.incr_reg(Reg::E),
			0x1D => self.decr_reg(Reg::E),
			0x1E => self.load_reg_from_imm8(Reg::E),
			0x1F => self.rotate_reg_right_through_carry(Reg::A, 4),
			0x20 => self.jump_to_current_addr_plus_imm8_if_flag(RegFlag::Zero, |value| !value),
			0x21 => self.load_reg_from_imm16(Reg::H, Reg::L),
			0x22 => self.load_hilo_addr_from_reg_and_incr(Reg::H, Reg::L, Reg::A),
			0x23 => self.incr_hilo(Reg::H, Reg::L),
			0x24 => self.incr_reg(Reg::H),
			0x25 => self.decr_reg(Reg::H),
			0x26 => self.load_reg_from_imm8(Reg::H),
			0x27 => self.decimal_adjust_register(Reg::A),
			0x28 => self.jump_to_current_addr_plus_imm8_if_flag(RegFlag::Zero, |value| value),
			0x29 => self.add_hilo1_with_hilo2(Reg::H, Reg::L, Reg::H, Reg::L),
			0x2A => self.load_reg_from_hilo_addr_and_incr(Reg::A, Reg::H, Reg::L),
			0x2B => self.decr_hilo(Reg::H, Reg::L),
			0x2C => self.incr_reg(Reg::L),
			0x2D => self.decr_reg(Reg::L),
			0x2E => self.load_reg_from_imm8(Reg::L),
			0x2F => self.complement_register(Reg::A),
			0x30 => self.jump_to_current_addr_plus_imm8_if_flag(RegFlag::Carry, |value| !value),
			0x31 => self.load_sp_from_imm16(),
			0x32 => self.load_hilo_addr_from_reg_and_decr(Reg::H, Reg::L, Reg::A),
			0x33 => self.incr_sp(),
			0x34 => self.incr_hilo_addr(Reg::H, Reg::L),
			0x35 => self.decr_hilo_addr(Reg::H, Reg::L),
			0x36 => self.load_hilo_addr_from_imm8(),
			0x37 => self.set_carry_flag(),
			0x38 => self.jump_to_current_addr_plus_imm8_if_flag(RegFlag::Carry, |value| value),
			0x39 => self.add_hilo_with_sp(Reg::H, Reg::L),
			0x3A => self.load_reg_from_hilo_addr_and_decr(Reg::A, Reg::H, Reg::L),
			0x3B => self.decr_sp(),
			0x3C => self.incr_reg(Reg::A),
			0x3D => self.decr_reg(Reg::A),
			0x3E => self.load_reg_from_imm8(Reg::A),
			0x3F => self.complement_carry_flag(),
			0x40 => self.load_reg1_from_reg2(Reg::B, Reg::B),
			0x41 => self.load_reg1_from_reg2(Reg::B, Reg::C),
			0x42 => self.load_reg1_from_reg2(Reg::B, Reg::D),
			0x43 => self.load_reg1_from_reg2(Reg::B, Reg::E),
			0x44 => self.load_reg1_from_reg2(Reg::B, Reg::H),
			0x45 => self.load_reg1_from_reg2(Reg::B, Reg::L),
			0x46 => self.load_reg_from_hilo_addr(Reg::B, Reg::H, Reg::L),
			0x47 => self.load_reg1_from_reg2(Reg::B, Reg::A),
			0x48 => self.load_reg1_from_reg2(Reg::C, Reg::B),
			0x49 => self.load_reg1_from_reg2(Reg::C, Reg::C),
			0x4A => self.load_reg1_from_reg2(Reg::C, Reg::D),
			0x4B => self.load_reg1_from_reg2(Reg::C, Reg::E),
			0x4C => self.load_reg1_from_reg2(Reg::C, Reg::H),
			0x4D => self.load_reg1_from_reg2(Reg::C, Reg::L),
			0x4E => self.load_reg_from_hilo_addr(Reg::C, Reg::H, Reg::L),
			0x4F => self.load_reg1_from_reg2(Reg::C, Reg::A),
			0x50 => self.load_reg1_from_reg2(Reg::D, Reg::B),
			0x51 => self.load_reg1_from_reg2(Reg::D, Reg::C),
			0x52 => self.load_reg1_from_reg2(Reg::D, Reg::D),
			0x53 => self.load_reg1_from_reg2(Reg::D, Reg::E),
			0x54 => self.load_reg1_from_reg2(Reg::D, Reg::H),
			0x55 => self.load_reg1_from_reg2(Reg::D, Reg::L),
			0x56 => self.load_reg_from_hilo_addr(Reg::D, Reg::H, Reg::L),
			0x57 => self.load_reg1_from_reg2(Reg::D, Reg::A),
			0x58 => self.load_reg1_from_reg2(Reg::E, Reg::B),
			0x59 => self.load_reg1_from_reg2(Reg::E, Reg::C),
			0x5A => self.load_reg1_from_reg2(Reg::E, Reg::D),
			0x5B => self.load_reg1_from_reg2(Reg::E, Reg::E),
			0x5C => self.load_reg1_from_reg2(Reg::E, Reg::H),
			0x5D => self.load_reg1_from_reg2(Reg::E, Reg::L),
			0x5E => self.load_reg_from_hilo_addr(Reg::E, Reg::H, Reg::L),
			0x5F => self.load_reg1_from_reg2(Reg::E, Reg::A),
			0x60 => self.load_reg1_from_reg2(Reg::H, Reg::B),
			0x61 => self.load_reg1_from_reg2(Reg::H, Reg::C),
			0x62 => self.load_reg1_from_reg2(Reg::H, Reg::D),
			0x63 => self.load_reg1_from_reg2(Reg::H, Reg::E),
			0x64 => self.load_reg1_from_reg2(Reg::H, Reg::H),
			0x65 => self.load_reg1_from_reg2(Reg::H, Reg::L),
			0x66 => self.load_reg_from_hilo_addr(Reg::H, Reg::H, Reg::L),
			0x67 => self.load_reg1_from_reg2(Reg::H, Reg::A),
			0x68 => self.load_reg1_from_reg2(Reg::L, Reg::B),
			0x69 => self.load_reg1_from_reg2(Reg::L, Reg::C),
			0x6A => self.load_reg1_from_reg2(Reg::L, Reg::D),
			0x6B => self.load_reg1_from_reg2(Reg::L, Reg::E),
			0x6C => self.load_reg1_from_reg2(Reg::L, Reg::H),
			0x6D => self.load_reg1_from_reg2(Reg::L, Reg::L),
			0x6E => self.load_reg_from_hilo_addr(Reg::L, Reg::H, Reg::L),
			0x6F => self.load_reg1_from_reg2(Reg::L, Reg::A),
			0x70 => self.load_hilo_addr_from_reg(Reg::H, Reg::L, Reg::B),
			0x71 => self.load_hilo_addr_from_reg(Reg::H, Reg::L, Reg::C),
			0x72 => self.load_hilo_addr_from_reg(Reg::H, Reg::L, Reg::D),
			0x73 => self.load_hilo_addr_from_reg(Reg::H, Reg::L, Reg::E),
			0x74 => self.load_hilo_addr_from_reg(Reg::H, Reg::L, Reg::H),
			0x75 => self.load_hilo_addr_from_reg(Reg::H, Reg::L, Reg::L),
			0x76 => self.power_down(),
			0x77 => self.load_hilo_addr_from_reg(Reg::H, Reg::L, Reg::A),
			0x78 => self.load_reg1_from_reg2(Reg::A, Reg::B),
			0x79 => self.load_reg1_from_reg2(Reg::A, Reg::C),
			0x7A => self.load_reg1_from_reg2(Reg::A, Reg::D),
			0x7B => self.load_reg1_from_reg2(Reg::A, Reg::E),
			0x7C => self.load_reg1_from_reg2(Reg::A, Reg::H),
			0x7D => self.load_reg1_from_reg2(Reg::A, Reg::L),
			0x7E => self.load_reg_from_hilo_addr(Reg::A, Reg::H, Reg::L),
			0x7F => self.load_reg1_from_reg2(Reg::A, Reg::A),
			0x80 => self.add_reg1_with_reg2(Reg::A, Reg::B),
			0x81 => self.add_reg1_with_reg2(Reg::A, Reg::C),
			0x82 => self.add_reg1_with_reg2(Reg::A, Reg::D),
			0x83 => self.add_reg1_with_reg2(Reg::A, Reg::E),
			0x84 => self.add_reg1_with_reg2(Reg::A, Reg::H),
			0x85 => self.add_reg1_with_reg2(Reg::A, Reg::L),
			0x86 => self.add_reg_with_hilo_addr(Reg::A, Reg::H, Reg::L),
			0x87 => self.add_reg1_with_reg2(Reg::A, Reg::A),
			0x88 => self.add_reg1_with_reg2_carried(Reg::A, Reg::B),
			0x89 => self.add_reg1_with_reg2_carried(Reg::A, Reg::C),
			0x8A => self.add_reg1_with_reg2_carried(Reg::A, Reg::D),
			0x8B => self.add_reg1_with_reg2_carried(Reg::A, Reg::E),
			0x8C => self.add_reg1_with_reg2_carried(Reg::A, Reg::H),
			0x8D => self.add_reg1_with_reg2_carried(Reg::A, Reg::L),
			0x8E => self.add_reg_with_hilo_addr_carried(Reg::A, Reg::H, Reg::L),
			0x8F => self.add_reg1_with_reg2_carried(Reg::A, Reg::A),
			0x90 => self.sub_reg1_with_reg2(Reg::A, Reg::B),
			0x91 => self.sub_reg1_with_reg2(Reg::A, Reg::C),
			0x92 => self.sub_reg1_with_reg2(Reg::A, Reg::D),
			0x93 => self.sub_reg1_with_reg2(Reg::A, Reg::E),
			0x94 => self.sub_reg1_with_reg2(Reg::A, Reg::H),
			0x95 => self.sub_reg1_with_reg2(Reg::A, Reg::L),
			0x96 => self.sub_reg_with_hilo_addr(Reg::A, Reg::H, Reg::L),
			0x97 => self.sub_reg1_with_reg2(Reg::A, Reg::A),
			0x98 => self.sub_reg1_with_reg2_carried(Reg::A, Reg::B),
			0x99 => self.sub_reg1_with_reg2_carried(Reg::A, Reg::C),
			0x9A => self.sub_reg1_with_reg2_carried(Reg::A, Reg::D),
			0x9B => self.sub_reg1_with_reg2_carried(Reg::A, Reg::E),
			0x9C => self.sub_reg1_with_reg2_carried(Reg::A, Reg::H),
			0x9D => self.sub_reg1_with_reg2_carried(Reg::A, Reg::L),
			0x9E => self.sub_reg_with_hilo_addr_carried(Reg::A, Reg::H, Reg::L),
			0x9F => self.sub_reg1_with_reg2_carried(Reg::A, Reg::A),
			0xA0 => self.and_reg1_with_reg2(Reg::A, Reg::B),
			0xA1 => self.and_reg1_with_reg2(Reg::A, Reg::C),
			0xA2 => self.and_reg1_with_reg2(Reg::A, Reg::D),
			0xA3 => self.and_reg1_with_reg2(Reg::A, Reg::E),
			0xA4 => self.and_reg1_with_reg2(Reg::A, Reg::H),
			0xA5 => self.and_reg1_with_reg2(Reg::A, Reg::L),
			0xA6 => self.and_reg_with_hilo_addr(Reg::A, Reg::H, Reg::L),
			0xA7 => self.and_reg1_with_reg2(Reg::A, Reg::A),
			0xA8 => self.xor_reg1_with_reg2(Reg::A, Reg::B),
			0xA9 => self.xor_reg1_with_reg2(Reg::A, Reg::C),
			0xAA => self.xor_reg1_with_reg2(Reg::A, Reg::D),
			0xAB => self.xor_reg1_with_reg2(Reg::A, Reg::E),
			0xAC => self.xor_reg1_with_reg2(Reg::A, Reg::H),
			0xAD => self.xor_reg1_with_reg2(Reg::A, Reg::L),
			0xAE => self.xor_reg_with_hilo_addr(Reg::A, Reg::H, Reg::L),
			0xAF => self.xor_reg1_with_reg2(Reg::A, Reg::A),
			0xB0 => self.or_reg1_with_reg2(Reg::A, Reg::B),
			0xB1 => self.or_reg1_with_reg2(Reg::A, Reg::C),
			0xB2 => self.or_reg1_with_reg2(Reg::A, Reg::D),
			0xB3 => self.or_reg1_with_reg2(Reg::A, Reg::E),
			0xB4 => self.or_reg1_with_reg2(Reg::A, Reg::H),
			0xB5 => self.or_reg1_with_reg2(Reg::A, Reg::L),
			0xB6 => self.or_reg_with_hilo_addr(Reg::A, Reg::H, Reg::L),
			0xB7 => self.or_reg1_with_reg2(Reg::A, Reg::A),
			0xB8 => self.cp_reg1_with_reg2(Reg::A, Reg::B),
			0xB9 => self.cp_reg1_with_reg2(Reg::A, Reg::C),
			0xBA => self.cp_reg1_with_reg2(Reg::A, Reg::D),
			0xBB => self.cp_reg1_with_reg2(Reg::A, Reg::E),
			0xBC => self.cp_reg1_with_reg2(Reg::A, Reg::H),
			0xBD => self.cp_reg1_with_reg2(Reg::A, Reg::L),
			0xBE => self.cp_reg_with_hilo_addr(Reg::A, Reg::H, Reg::L),
			0xBF => self.cp_reg1_with_reg2(Reg::A, Reg::A),
			0xC0 => self.return_to_addr_if_flag(RegFlag::Zero, |value| !value),
			0xC1 => self.pop_hilo(Reg::B, Reg::C),
			0xC2 => self.jump_to_addr_if_flag(RegFlag::Zero, |value| !value),
			0xC3 => self.jump_to_addr(),
			0xC4 => self.call_if_flag(RegFlag::Zero, |value| !value),
			0xC5 => self.push_hilo(Reg::B, Reg::C),
			0xC6 => self.add_reg_with_imm8(Reg::A),
			0xC7 => self.call_restart(0x0000),
			0xC8 => self.return_to_addr_if_flag(RegFlag::Zero, |value| value),
			0xC9 => self.return_to_addr(),
			0xCA => self.jump_to_addr_if_flag(RegFlag::Zero, |value| value),
			0xCB => self.execute_bit_instr(),
			0xCC => self.call_if_flag(RegFlag::Zero, |value| value),
			0xCD => self.call(),
			0xCE => self.add_reg_with_imm8_carried(Reg::A),
			0xCF => self.call_restart(0x0008),
			0xD0 => self.return_to_addr_if_flag(RegFlag::Carry, |value| !value),
			0xD1 => self.pop_hilo(Reg::D, Reg::E),
			0xD2 => self.jump_to_addr_if_flag(RegFlag::Carry, |value| !value),
			0xD4 => self.call_if_flag(RegFlag::Carry, |value| !value),
			0xD5 => self.push_hilo(Reg::D, Reg::E),
			0xD6 => self.sub_reg_with_imm8(Reg::A),
			0xD7 => self.call_restart(0x0010),
			0xD8 => self.return_to_addr_if_flag(RegFlag::Carry, |value| value),
			0xD9 => self.return_to_addr_and_enable_interrupts(),
			0xDA => self.jump_to_addr_if_flag(RegFlag::Carry, |value| value),
			0xDC => self.call_if_flag(RegFlag::Carry, |value| value),
			0xDE => self.sub_reg_with_imm8_carried(Reg::A),
			0xDF => self.call_restart(0x0018),
			0xE0 => self.load_imm_addr_offset_from_reg(0xFF00, Reg::A),
			0xE1 => self.pop_hilo(Reg::H, Reg::L),
			0xE2 => self.load_reg1_addr_offset_from_reg2(Reg::C, 0xFF00, Reg::A),
			0xE5 => self.push_hilo(Reg::H, Reg::L),
			0xE6 => self.and_reg_with_imm8(Reg::A),
			0xE7 => self.call_restart(0x0020),
			0xE8 => self.add_sp_with_signed_imm8(),
			0xE9 => self.jump_to_hilo(Reg::H, Reg::L),
			0xEA => self.load_imm_addr_from_reg(Reg::A),
			0xEE => self.xor_reg_with_imm8(Reg::A),
			0xEF => self.call_restart(0x0028),
			0xF0 => self.load_reg_from_imm_addr_offset(Reg::A, 0xFF00),
			0xF1 => self.pop_hilo(Reg::A, Reg::F),
			0xF2 => self.load_reg1_from_reg2_addr_offset(Reg::A, Reg::C, 0xFF00),
			0xF3 => self.disable_interrupts(),
			0xF5 => self.push_hilo(Reg::A, Reg::F),
			0xF6 => self.or_reg_with_imm8(Reg::A),
			0xF7 => self.call_restart(0x0030),
			0xF8 => self.load_hilo_from_sp_signed_imm_offset(Reg::H, Reg::L),
			0xF9 => self.load_sp_from_hilo(Reg::H, Reg::L),
			0xFA => self.load_reg_from_imm_addr(Reg::A),
			0xFB => self.enable_interrupts(),
			0xFE => self.cp_reg_with_imm8(Reg::A),
			0xFF => self.call_restart(0x0038),
			_ => {
				let error = format!("Unknown instruction: {:02X}", instr);
				log::error(error.clone());
				panic!("{}", error);
			}
		};
	}

	fn execute_long_instr(&mut self) {
		let instr = self.get_instr_param();
		match instr {
			0x00 => self.stop(),
			_ => {
				let error = format!("Unknown longinstruction: {:02X}", instr);
				log::error(error.clone());
				panic!("{}", error);
			}
		}
	}

	#[inline(never)]
	fn execute_bit_instr(&mut self) {
		let instr = self.get_instr_param();
		match instr {
			0x00 => self.rotate_reg_left(Reg::B, 8),
			0x01 => self.rotate_reg_left(Reg::C, 8),
			0x02 => self.rotate_reg_left(Reg::D, 8),
			0x03 => self.rotate_reg_left(Reg::E, 8),
			0x04 => self.rotate_reg_left(Reg::H, 8),
			0x05 => self.rotate_reg_left(Reg::L, 8),
			0x06 => self.rotate_hilo_addr_left(Reg::H, Reg::L),
			0x07 => self.rotate_reg_left(Reg::A, 8),
			0x08 => self.rotate_reg_right(Reg::B, 8),
			0x09 => self.rotate_reg_right(Reg::C, 8),
			0x0A => self.rotate_reg_right(Reg::D, 8),
			0x0B => self.rotate_reg_right(Reg::E, 8),
			0x0C => self.rotate_reg_right(Reg::H, 8),
			0x0D => self.rotate_reg_right(Reg::L, 8),
			0x0E => self.rotate_hilo_addr_right(Reg::H, Reg::L),
			0x0F => self.rotate_reg_right(Reg::A, 8),
			0x10 => self.rotate_reg_left_through_carry(Reg::B, 8),
			0x11 => self.rotate_reg_left_through_carry(Reg::C, 8),
			0x12 => self.rotate_reg_left_through_carry(Reg::D, 8),
			0x13 => self.rotate_reg_left_through_carry(Reg::E, 8),
			0x14 => self.rotate_reg_left_through_carry(Reg::H, 8),
			0x15 => self.rotate_reg_left_through_carry(Reg::L, 8),
			0x16 => self.rotate_hilo_addr_left_through_carry(Reg::H, Reg::L),
			0x17 => self.rotate_reg_left_through_carry(Reg::A, 8),
			0x18 => self.rotate_reg_right_through_carry(Reg::B, 8),
			0x19 => self.rotate_reg_right_through_carry(Reg::C, 8),
			0x1A => self.rotate_reg_right_through_carry(Reg::D, 8),
			0x1B => self.rotate_reg_right_through_carry(Reg::E, 8),
			0x1C => self.rotate_reg_right_through_carry(Reg::H, 8),
			0x1D => self.rotate_reg_right_through_carry(Reg::L, 8),
			0x1E => self.rotate_hilo_addr_right_through_carry(Reg::H, Reg::L),
			0x1F => self.rotate_reg_right_through_carry(Reg::A, 8),
			0x20 => self.shift_reg_left(Reg::B),
			0x21 => self.shift_reg_left(Reg::C),
			0x22 => self.shift_reg_left(Reg::D),
			0x23 => self.shift_reg_left(Reg::E),
			0x24 => self.shift_reg_left(Reg::H),
			0x25 => self.shift_reg_left(Reg::L),
			0x26 => self.shift_hilo_addr_left(Reg::H, Reg::L),
			0x27 => self.shift_reg_left(Reg::A),
			0x28 => self.shift_reg_right_preserved(Reg::B),
			0x29 => self.shift_reg_right_preserved(Reg::C),
			0x2A => self.shift_reg_right_preserved(Reg::D),
			0x2B => self.shift_reg_right_preserved(Reg::E),
			0x2C => self.shift_reg_right_preserved(Reg::H),
			0x2D => self.shift_reg_right_preserved(Reg::L),
			0x2E => self.shift_hilo_addr_right_preserved(Reg::H, Reg::L),
			0x2F => self.shift_reg_right_preserved(Reg::A),
			0x30 => self.swap_reg(Reg::B),
			0x31 => self.swap_reg(Reg::C),
			0x32 => self.swap_reg(Reg::D),
			0x33 => self.swap_reg(Reg::E),
			0x34 => self.swap_reg(Reg::H),
			0x35 => self.swap_reg(Reg::L),
			0x36 => self.swap_hilo_addr(Reg::H, Reg::L),
			0x37 => self.swap_reg(Reg::A),
			0x38 => self.shift_reg_right_reset(Reg::B),
			0x39 => self.shift_reg_right_reset(Reg::C),
			0x3A => self.shift_reg_right_reset(Reg::D),
			0x3B => self.shift_reg_right_reset(Reg::E),
			0x3C => self.shift_reg_right_reset(Reg::H),
			0x3D => self.shift_reg_right_reset(Reg::L),
			0x3E => self.shift_hilo_addr_right_reset(Reg::H, Reg::L),
			0x3F => self.shift_reg_right_reset(Reg::A),
			0x40 => self.test_bit_in_reg(Reg::B, 0),
			0x41 => self.test_bit_in_reg(Reg::C, 0),
			0x42 => self.test_bit_in_reg(Reg::D, 0),
			0x43 => self.test_bit_in_reg(Reg::E, 0),
			0x44 => self.test_bit_in_reg(Reg::H, 0),
			0x45 => self.test_bit_in_reg(Reg::L, 0),
			0x46 => self.test_bit_in_hilo_addr(Reg::H, Reg::L, 0),
			0x47 => self.test_bit_in_reg(Reg::A, 0),
			0x48 => self.test_bit_in_reg(Reg::B, 1),
			0x49 => self.test_bit_in_reg(Reg::C, 1),
			0x4A => self.test_bit_in_reg(Reg::D, 1),
			0x4B => self.test_bit_in_reg(Reg::E, 1),
			0x4C => self.test_bit_in_reg(Reg::H, 1),
			0x4D => self.test_bit_in_reg(Reg::L, 1),
			0x4E => self.test_bit_in_hilo_addr(Reg::H, Reg::L, 1),
			0x4F => self.test_bit_in_reg(Reg::A, 1),
			0x50 => self.test_bit_in_reg(Reg::B, 2),
			0x51 => self.test_bit_in_reg(Reg::C, 2),
			0x52 => self.test_bit_in_reg(Reg::D, 2),
			0x53 => self.test_bit_in_reg(Reg::E, 2),
			0x54 => self.test_bit_in_reg(Reg::H, 2),
			0x55 => self.test_bit_in_reg(Reg::L, 2),
			0x56 => self.test_bit_in_hilo_addr(Reg::H, Reg::L, 2),
			0x57 => self.test_bit_in_reg(Reg::A, 2),
			0x58 => self.test_bit_in_reg(Reg::B, 3),
			0x59 => self.test_bit_in_reg(Reg::C, 3),
			0x5A => self.test_bit_in_reg(Reg::D, 3),
			0x5B => self.test_bit_in_reg(Reg::E, 3),
			0x5C => self.test_bit_in_reg(Reg::H, 3),
			0x5D => self.test_bit_in_reg(Reg::L, 3),
			0x5E => self.test_bit_in_hilo_addr(Reg::H, Reg::L, 3),
			0x5F => self.test_bit_in_reg(Reg::A, 3),
			0x60 => self.test_bit_in_reg(Reg::B, 4),
			0x61 => self.test_bit_in_reg(Reg::C, 4),
			0x62 => self.test_bit_in_reg(Reg::D, 4),
			0x63 => self.test_bit_in_reg(Reg::E, 4),
			0x64 => self.test_bit_in_reg(Reg::H, 4),
			0x65 => self.test_bit_in_reg(Reg::L, 4),
			0x66 => self.test_bit_in_hilo_addr(Reg::H, Reg::L, 4),
			0x67 => self.test_bit_in_reg(Reg::A, 4),
			0x68 => self.test_bit_in_reg(Reg::B, 5),
			0x69 => self.test_bit_in_reg(Reg::C, 5),
			0x6A => self.test_bit_in_reg(Reg::D, 5),
			0x6B => self.test_bit_in_reg(Reg::E, 5),
			0x6C => self.test_bit_in_reg(Reg::H, 5),
			0x6D => self.test_bit_in_reg(Reg::L, 5),
			0x6E => self.test_bit_in_hilo_addr(Reg::H, Reg::L, 5),
			0x6F => self.test_bit_in_reg(Reg::A, 5),
			0x70 => self.test_bit_in_reg(Reg::B, 6),
			0x71 => self.test_bit_in_reg(Reg::C, 6),
			0x72 => self.test_bit_in_reg(Reg::D, 6),
			0x73 => self.test_bit_in_reg(Reg::E, 6),
			0x74 => self.test_bit_in_reg(Reg::H, 6),
			0x75 => self.test_bit_in_reg(Reg::L, 6),
			0x76 => self.test_bit_in_hilo_addr(Reg::H, Reg::L, 6),
			0x77 => self.test_bit_in_reg(Reg::A, 6),
			0x78 => self.test_bit_in_reg(Reg::B, 7),
			0x79 => self.test_bit_in_reg(Reg::C, 7),
			0x7A => self.test_bit_in_reg(Reg::D, 7),
			0x7B => self.test_bit_in_reg(Reg::E, 7),
			0x7C => self.test_bit_in_reg(Reg::H, 7),
			0x7D => self.test_bit_in_reg(Reg::L, 7),
			0x7E => self.test_bit_in_hilo_addr(Reg::H, Reg::L, 7),
			0x7F => self.test_bit_in_reg(Reg::A, 7),
			0x80 => self.reset_bit_in_reg(Reg::B, 0),
			0x81 => self.reset_bit_in_reg(Reg::C, 0),
			0x82 => self.reset_bit_in_reg(Reg::D, 0),
			0x83 => self.reset_bit_in_reg(Reg::E, 0),
			0x84 => self.reset_bit_in_reg(Reg::H, 0),
			0x85 => self.reset_bit_in_reg(Reg::L, 0),
			0x86 => self.reset_bit_in_hilo_addr(Reg::H, Reg::L, 0),
			0x87 => self.reset_bit_in_reg(Reg::A, 0),
			0x88 => self.reset_bit_in_reg(Reg::B, 1),
			0x89 => self.reset_bit_in_reg(Reg::C, 1),
			0x8A => self.reset_bit_in_reg(Reg::D, 1),
			0x8B => self.reset_bit_in_reg(Reg::E, 1),
			0x8C => self.reset_bit_in_reg(Reg::H, 1),
			0x8D => self.reset_bit_in_reg(Reg::L, 1),
			0x8E => self.reset_bit_in_hilo_addr(Reg::H, Reg::L, 1),
			0x8F => self.reset_bit_in_reg(Reg::A, 1),
			0x90 => self.reset_bit_in_reg(Reg::B, 2),
			0x91 => self.reset_bit_in_reg(Reg::C, 2),
			0x92 => self.reset_bit_in_reg(Reg::D, 2),
			0x93 => self.reset_bit_in_reg(Reg::E, 2),
			0x94 => self.reset_bit_in_reg(Reg::H, 2),
			0x95 => self.reset_bit_in_reg(Reg::L, 2),
			0x96 => self.reset_bit_in_hilo_addr(Reg::H, Reg::L, 2),
			0x97 => self.reset_bit_in_reg(Reg::A, 2),
			0x98 => self.reset_bit_in_reg(Reg::B, 3),
			0x99 => self.reset_bit_in_reg(Reg::C, 3),
			0x9A => self.reset_bit_in_reg(Reg::D, 3),
			0x9B => self.reset_bit_in_reg(Reg::E, 3),
			0x9C => self.reset_bit_in_reg(Reg::H, 3),
			0x9D => self.reset_bit_in_reg(Reg::L, 3),
			0x9E => self.reset_bit_in_hilo_addr(Reg::H, Reg::L, 3),
			0x9F => self.reset_bit_in_reg(Reg::A, 3),
			0xA0 => self.reset_bit_in_reg(Reg::B, 4),
			0xA1 => self.reset_bit_in_reg(Reg::C, 4),
			0xA2 => self.reset_bit_in_reg(Reg::D, 4),
			0xA3 => self.reset_bit_in_reg(Reg::E, 4),
			0xA4 => self.reset_bit_in_reg(Reg::H, 4),
			0xA5 => self.reset_bit_in_reg(Reg::L, 4),
			0xA6 => self.reset_bit_in_hilo_addr(Reg::H, Reg::L, 4),
			0xA7 => self.reset_bit_in_reg(Reg::A, 4),
			0xA8 => self.reset_bit_in_reg(Reg::B, 5),
			0xA9 => self.reset_bit_in_reg(Reg::C, 5),
			0xAA => self.reset_bit_in_reg(Reg::D, 5),
			0xAB => self.reset_bit_in_reg(Reg::E, 5),
			0xAC => self.reset_bit_in_reg(Reg::H, 5),
			0xAD => self.reset_bit_in_reg(Reg::L, 5),
			0xAE => self.reset_bit_in_hilo_addr(Reg::H, Reg::L, 5),
			0xAF => self.reset_bit_in_reg(Reg::A, 5),
			0xB0 => self.reset_bit_in_reg(Reg::B, 6),
			0xB1 => self.reset_bit_in_reg(Reg::C, 6),
			0xB2 => self.reset_bit_in_reg(Reg::D, 6),
			0xB3 => self.reset_bit_in_reg(Reg::E, 6),
			0xB4 => self.reset_bit_in_reg(Reg::H, 6),
			0xB5 => self.reset_bit_in_reg(Reg::L, 6),
			0xB6 => self.reset_bit_in_hilo_addr(Reg::H, Reg::L, 6),
			0xB7 => self.reset_bit_in_reg(Reg::A, 6),
			0xB8 => self.reset_bit_in_reg(Reg::B, 7),
			0xB9 => self.reset_bit_in_reg(Reg::C, 7),
			0xBA => self.reset_bit_in_reg(Reg::D, 7),
			0xBB => self.reset_bit_in_reg(Reg::E, 7),
			0xBC => self.reset_bit_in_reg(Reg::H, 7),
			0xBD => self.reset_bit_in_reg(Reg::L, 7),
			0xBE => self.reset_bit_in_hilo_addr(Reg::H, Reg::L, 7),
			0xBF => self.reset_bit_in_reg(Reg::A, 7),
			0xC0 => self.set_bit_in_reg(Reg::B, 0),
			0xC1 => self.set_bit_in_reg(Reg::C, 0),
			0xC2 => self.set_bit_in_reg(Reg::D, 0),
			0xC3 => self.set_bit_in_reg(Reg::E, 0),
			0xC4 => self.set_bit_in_reg(Reg::H, 0),
			0xC5 => self.set_bit_in_reg(Reg::L, 0),
			0xC6 => self.set_bit_in_hilo_addr(Reg::H, Reg::L, 0),
			0xC7 => self.set_bit_in_reg(Reg::A, 0),
			0xC8 => self.set_bit_in_reg(Reg::B, 1),
			0xC9 => self.set_bit_in_reg(Reg::C, 1),
			0xCA => self.set_bit_in_reg(Reg::D, 1),
			0xCB => self.set_bit_in_reg(Reg::E, 1),
			0xCC => self.set_bit_in_reg(Reg::H, 1),
			0xCD => self.set_bit_in_reg(Reg::L, 1),
			0xCE => self.set_bit_in_hilo_addr(Reg::H, Reg::L, 1),
			0xCF => self.set_bit_in_reg(Reg::A, 1),
			0xD0 => self.set_bit_in_reg(Reg::B, 2),
			0xD1 => self.set_bit_in_reg(Reg::C, 2),
			0xD2 => self.set_bit_in_reg(Reg::D, 2),
			0xD3 => self.set_bit_in_reg(Reg::E, 2),
			0xD4 => self.set_bit_in_reg(Reg::H, 2),
			0xD5 => self.set_bit_in_reg(Reg::L, 2),
			0xD6 => self.set_bit_in_hilo_addr(Reg::H, Reg::L, 2),
			0xD7 => self.set_bit_in_reg(Reg::A, 2),
			0xD8 => self.set_bit_in_reg(Reg::B, 3),
			0xD9 => self.set_bit_in_reg(Reg::C, 3),
			0xDA => self.set_bit_in_reg(Reg::D, 3),
			0xDB => self.set_bit_in_reg(Reg::E, 3),
			0xDC => self.set_bit_in_reg(Reg::H, 3),
			0xDD => self.set_bit_in_reg(Reg::L, 3),
			0xDE => self.set_bit_in_hilo_addr(Reg::H, Reg::L, 3),
			0xDF => self.set_bit_in_reg(Reg::A, 3),
			0xE0 => self.set_bit_in_reg(Reg::B, 4),
			0xE1 => self.set_bit_in_reg(Reg::C, 4),
			0xE2 => self.set_bit_in_reg(Reg::D, 4),
			0xE3 => self.set_bit_in_reg(Reg::E, 4),
			0xE4 => self.set_bit_in_reg(Reg::H, 4),
			0xE5 => self.set_bit_in_reg(Reg::L, 4),
			0xE6 => self.set_bit_in_hilo_addr(Reg::H, Reg::L, 4),
			0xE7 => self.set_bit_in_reg(Reg::A, 4),
			0xE8 => self.set_bit_in_reg(Reg::B, 5),
			0xE9 => self.set_bit_in_reg(Reg::C, 5),
			0xEA => self.set_bit_in_reg(Reg::D, 5),
			0xEB => self.set_bit_in_reg(Reg::E, 5),
			0xEC => self.set_bit_in_reg(Reg::H, 5),
			0xED => self.set_bit_in_reg(Reg::L, 5),
			0xEE => self.set_bit_in_hilo_addr(Reg::H, Reg::L, 5),
			0xEF => self.set_bit_in_reg(Reg::A, 5),
			0xF0 => self.set_bit_in_reg(Reg::B, 6),
			0xF1 => self.set_bit_in_reg(Reg::C, 6),
			0xF2 => self.set_bit_in_reg(Reg::D, 6),
			0xF3 => self.set_bit_in_reg(Reg::E, 6),
			0xF4 => self.set_bit_in_reg(Reg::H, 6),
			0xF5 => self.set_bit_in_reg(Reg::L, 6),
			0xF6 => self.set_bit_in_hilo_addr(Reg::H, Reg::L, 6),
			0xF7 => self.set_bit_in_reg(Reg::A, 6),
			0xF8 => self.set_bit_in_reg(Reg::B, 7),
			0xF9 => self.set_bit_in_reg(Reg::C, 7),
			0xFA => self.set_bit_in_reg(Reg::D, 7),
			0xFB => self.set_bit_in_reg(Reg::E, 7),
			0xFC => self.set_bit_in_reg(Reg::H, 7),
			0xFD => self.set_bit_in_reg(Reg::L, 7),
			0xFE => self.set_bit_in_hilo_addr(Reg::H, Reg::L, 7),
			0xFF => self.set_bit_in_reg(Reg::A, 7),
			_ => panic!("Unknown bit instruction: {:X}", instr)
		}
	}

	//NOP
	#[inline(never)]
	fn nop(&mut self) {
		self.clock += 4;
	}

	//LDH (n), A
	#[inline(never)]
	fn load_imm_addr_offset_from_reg(&mut self, offset: u16, reg: Reg) {
		let addr = offset + self.get_instr_param() as u16;

		let value = self.regs.get(reg);

		self.mmu.store_word(addr, value);
		self.clock += 12;
	}

	//LD n, A
	#[inline(never)]
	fn load_imm_addr_from_reg(&mut self, reg: Reg) {
		let lo = self.get_instr_param() as u16;
		let hi = self.get_instr_param() as u16;
		let addr = (hi << 8) + lo;

		let value = self.regs.get(reg);

		self.mmu.store_word(addr, value);
		self.clock += 16;
	}

	//LD (nn), SP
	#[inline(never)]
	fn load_imm_addr_from_sp(&mut self) {
		let lo = self.get_instr_param() as u16;
		let hi = self.get_instr_param() as u16;
		let addr = (hi << 8) + lo;

		let value = self.regs.sp.get();

		self.mmu.store_word(addr, value as u8);
		self.mmu.store_word(addr + 1, (value >> 8) as u8);
		self.clock += 20;
	}

	//LD nn, n
	#[inline(never)]
	fn load_reg_from_imm8(&mut self, reg: Reg) {
		let value = self.get_instr_param();

		self.regs.set(reg, value);
		self.clock += 8;
	}

	//LD n, nn
	#[inline(never)]
	fn load_reg_from_imm16(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let hi = self.get_instr_param();
		let lo = self.get_instr_param();

		self.regs.set(hi_reg, hi);
		self.regs.set(lo_reg, lo);
		self.clock += 12;
	}

	//LD A, n
	#[inline(never)]
	fn load_reg_from_imm_addr(&mut self, reg: Reg) {
		let lo = self.get_instr_param() as u16;
		let hi = self.get_instr_param() as u16;
		let addr = (hi << 8) + lo;

		let value = self.mmu.load_word(addr);

		self.regs.set(reg, value);
		self.clock += 16;
	}

	//LD r1, r2
	#[inline(never)]
	fn load_reg1_from_reg2(&mut self, reg1: Reg, reg2: Reg) {
		let value = self.regs.get(reg2);
		self.regs.set(reg1, value);
		self.clock += 4;
	}

	//LDH A, (n)
	#[inline(never)]
	fn load_reg_from_imm_addr_offset(&mut self, reg: Reg, offset: u16) {
		let addr = self.get_instr_param() as u16 + offset;

		let value = self.mmu.load_word(addr);

		self.regs.set(reg, value);
		self.clock += 12;
	}

	//LD A, (C)
	#[inline(never)]
	fn load_reg1_from_reg2_addr_offset(&mut self, reg1: Reg, reg2: Reg, offset: u16) {
		let addr = offset + self.regs.get(reg2) as u16;

		let value = self.mmu.load_word(addr);

		self.regs.set(reg1, value);
		self.clock += 8;
	}

	//LD (C), A
	#[inline(never)]
	fn load_reg1_addr_offset_from_reg2(&mut self, reg1: Reg, offset: u16, reg2: Reg) {
		let addr = self.regs.get(reg1) as u16 + offset;

		let value = self.regs.get(reg2);

		self.mmu.store_word(addr, value);
		self.clock += 8;
	}

	//LD r1, r2
	#[inline(never)]
	fn load_reg_from_hilo_addr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		self.exec_load_reg_from_hilo_addr(reg, hi_reg, lo_reg);
		self.clock += 8;
	}

	//LDI A, (HL)
	#[inline(never)]
	fn load_reg_from_hilo_addr_and_incr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		self.exec_load_reg_from_hilo_addr(reg, hi_reg, lo_reg);
		self.increment_hilo(hi_reg, lo_reg);
		self.clock += 8;
	}

	//LDD A, (HL)
	#[inline(never)]
	fn load_reg_from_hilo_addr_and_decr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		self.exec_load_reg_from_hilo_addr(reg, hi_reg, lo_reg);
		self.decrement_hilo(hi_reg, lo_reg);
		self.clock += 8;
	}

	//LD r1, r2
	#[inline(never)]
	fn load_hilo_addr_from_imm8(&mut self) {
		let hi = self.regs.get(Reg::H) as u16;
		let lo = self.regs.get(Reg::L) as u16;
		let addr = (hi << 8) + lo;

		let value = self.get_instr_param();
		
		self.mmu.store_word(addr, value);
		self.clock += 12;
	}

	//LD r1, r2
	#[inline(never)]
	fn load_hilo_addr_from_reg(&mut self, hi_reg: Reg, lo_reg: Reg, reg: Reg) {
		self.exec_load_hilo_addr_from_reg(hi_reg, lo_reg, reg);
		self.clock += 8;
	}

	//LDI (HL), A
	#[inline(never)]
	fn load_hilo_addr_from_reg_and_incr(&mut self, hi_reg: Reg, lo_reg: Reg, reg: Reg) {
		self.exec_load_hilo_addr_from_reg(hi_reg, lo_reg, reg);
		self.increment_hilo(hi_reg, lo_reg);
		self.clock += 8;
	}

	//LDD (HL), A
	#[inline(never)]
	fn load_hilo_addr_from_reg_and_decr(&mut self, hi_reg: Reg, lo_reg: Reg, reg: Reg) {
		self.exec_load_hilo_addr_from_reg(hi_reg, lo_reg, reg);
		self.decrement_hilo(hi_reg, lo_reg);
		self.clock += 8;
	}

	//LD HL, SP+n
	#[inline(never)]
	fn load_hilo_from_sp_signed_imm_offset(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let sp = self.regs.sp.get() as i32; //Increase size to avoid going negative
		let value = (self.get_instr_param() as i8) as i32; //Convert to i8 first in order to get two's complement conversion right
		let addr = sp + value;

		if addr >= 0 {
			if addr <= 0xFFFF {
				self.regs.set(hi_reg, (addr >> 8) as u8);
				self.regs.set(lo_reg, addr as u8);
				self.regs.set_flag(RegFlag::Carry, false);
				self.regs.set_flag(
					RegFlag::HalfCarry, 
					Self::has_half_carried_u16(sp as u16, addr as u16)
				);
			} else {
				let wrapped_addr = addr - 0x1_0000;

				self.regs.set(hi_reg, (wrapped_addr >> 8) as u8);
				self.regs.set(lo_reg, wrapped_addr as u8);
				self.regs.set_flag(RegFlag::Carry, true);
				self.regs.set_flag(RegFlag::HalfCarry, false);
			}
		} else {
			let wrapped_addr = 0x1_0000 + addr;

			self.regs.set(hi_reg, (wrapped_addr >> 8) as u8);
			self.regs.set(lo_reg, wrapped_addr as u8);
			self.regs.set_flag(RegFlag::Carry, true);
			self.regs.set_flag(RegFlag::HalfCarry, false);
		}

		self.regs.set_flag(RegFlag::Zero, false);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.clock += 12;
	}

	//LD n, nn
	#[inline(never)]
	fn load_sp_from_imm16(&mut self) {
		let hi = self.get_instr_param() as u16;
		let lo = self.get_instr_param() as u16;
		let value = (hi << 8) + lo;

		self.regs.sp.set(value);
		self.clock += 12;
	}

	//LD SP, HL
	#[inline(never)]
	fn load_sp_from_hilo(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		
		self.regs.sp.set(addr);
		self.clock += 8;
	}

	//Push nn
	#[inline(never)]
	fn push_hilo(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let hi = self.regs.get(hi_reg);
		let lo = self.regs.get(lo_reg);

		self.push_value_to_stack(hi);
		self.push_value_to_stack(lo);
		self.clock += 16;
	}

	//Pop nn
	#[inline(never)]
	fn pop_hilo(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let lo = self.pop_value_from_stack();
		let hi = self.pop_value_from_stack();

		self.regs.set(hi_reg, hi);
		self.regs.set(lo_reg, lo);
		self.clock += 12;
	}

	//Add A, n
	#[inline(never)]
	fn add_reg_with_imm8(&mut self, reg: Reg) {
		let rh = self.get_instr_param();

		self.exec_add(reg, rh);
		self.clock += 8;
	}

	//Add A, n
	#[inline(never)]
	fn add_reg1_with_reg2(&mut self, reg1: Reg, reg2: Reg) {
		let rh = self.regs.get(reg2);
		
		self.exec_add(reg1, rh);
		self.clock += 4;
	}

	//Add A, n
	#[inline(never)]
	fn add_reg_with_hilo_addr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let rh = self.mmu.load_word(addr);

		self.exec_add(reg, rh);
		self.clock += 8;
	}

	//Add A, n
	#[inline(never)]
	fn add_reg_with_imm8_carried(&mut self, reg: Reg) {
		let rh = self.get_instr_param() + self.regs.get_flag(RegFlag::Carry) as u8;

		self.exec_add(reg, rh);
		self.clock += 8;
	}

	//Add HL, n
	#[inline(never)]
	fn add_hilo1_with_hilo2(&mut self, hi_reg1: Reg, lo_reg1: Reg, hi_reg2: Reg, lo_reg2: Reg) {
		let rh = self.get_value_from_hilo(hi_reg2, lo_reg2);

		self.exec_add16(hi_reg1, lo_reg1, rh);
		self.clock += 8;
	}

	//Add HL, n
	#[inline(never)]
	fn add_hilo_with_sp(&mut self, hi_reg1: Reg, lo_reg1: Reg) {
		let rh = self.regs.sp.get();

		self.exec_add16(hi_reg1, lo_reg1, rh);
		self.clock += 8;		
	}

	//Add SP, n
	#[inline(never)]
	fn add_sp_with_signed_imm8(&mut self) {
		let sp = self.regs.sp.get() as i32;
		let value = (self.get_instr_param() as i8) as i32; //Convert to i8 first in order to get two's complement conversion right
		let result = sp + value;
		if result >= 0 {
			if result <= 0xFFFF {
				self.regs.sp.set(result as u16);
				self.regs.set_flag(RegFlag::Carry, false);
				self.regs.set_flag(
					RegFlag::HalfCarry, 
					Self::has_half_carried_u16(sp as u16, value as u16)
				);
			} else {
				let wrapped_sp = result - 0x1_0000;

				self.regs.sp.set(wrapped_sp as u16);
				self.regs.set_flag(RegFlag::Carry, true);
				self.regs.set_flag(RegFlag::HalfCarry, false);
			}
		} else {
			let wrapped_sp = 0x1_0000 + result;

			self.regs.sp.set(wrapped_sp as u16);
			self.regs.set_flag(RegFlag::Carry, true);
			self.regs.set_flag(RegFlag::HalfCarry, false);
		}

		self.regs.set_flag(RegFlag::Zero, false);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.clock += 16;
	}

	//Adc A, n
	#[inline(never)]
	fn add_reg1_with_reg2_carried(&mut self, reg1: Reg, reg2: Reg) {
		let rh = self.regs.get(reg2) + self.regs.get_flag(RegFlag::Carry) as u8;
		
		self.exec_add(reg1, rh);
		self.clock += 4;
	}

	//Adc A, n
	#[inline(never)]
	fn add_reg_with_hilo_addr_carried(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let rh = self.mmu.load_word(addr) + self.regs.get_flag(RegFlag::Carry) as u8;

		self.exec_add(reg, rh);
		self.clock += 8;
	}

	//Sub n
	#[inline(never)]
	fn sub_reg_with_imm8(&mut self, reg: Reg) {
		let rh = self.get_instr_param();

		self.exec_sub(reg, rh);
		self.clock += 8;
	}

	//Sub n
	#[inline(never)]
	fn sub_reg1_with_reg2(&mut self, reg1: Reg, reg2: Reg) {
		let rh = self.regs.get(reg2);

		self.exec_sub(reg1, rh);
		self.clock += 4;
	}

	//Sub n
	#[inline(never)]
	fn sub_reg_with_hilo_addr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let rh = self.mmu.load_word(addr);

		self.exec_sub(reg, rh);
		self.clock += 8;
	}

	//Sub n
	#[inline(never)]
	fn sub_reg_with_imm8_carried(&mut self, reg: Reg) {
		let rh = self.get_instr_param() + self.regs.get_flag(RegFlag::Carry) as u8;

		self.exec_sub(reg, rh);
		self.clock += 8;
	}

	//Sbc A, n
	#[inline(never)]
	fn sub_reg1_with_reg2_carried(&mut self, reg1: Reg, reg2: Reg) {
		let rh = self.regs.get(reg2) + self.regs.get_flag(RegFlag::Carry) as u8;
		
		self.exec_sub(reg1, rh);
		self.clock += 4;
	}

	//Sub n
	#[inline(never)]
	fn sub_reg_with_hilo_addr_carried(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let rh = self.mmu.load_word(addr) + self.regs.get_flag(RegFlag::Carry) as u8;

		self.exec_sub(reg, rh);
		self.clock += 8;
	}

	//And n
	#[inline(never)]
	fn and_reg_with_imm8(&mut self, reg: Reg) {
		let rh = self.get_instr_param();

		self.exec_and(reg, rh);
		self.clock += 8;		
	}

	//And n
	#[inline(never)]
	fn and_reg1_with_reg2(&mut self, reg1: Reg, reg2: Reg) {
		let rh = self.regs.get(reg2);

		self.exec_and(reg1, rh);
		self.clock += 4;
	}

	//And n
	#[inline(never)]
	fn and_reg_with_hilo_addr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let rh = self.mmu.load_word(addr);
		
		self.exec_and(reg, rh);
		self.clock += 8;
	}

	//Or n
	#[inline(never)]
	fn or_reg_with_imm8(&mut self, reg: Reg) {
		let rh = self.get_instr_param();

		self.exec_or(reg, rh);
		self.clock += 8;
	}

	//Or n
	#[inline(never)]
	fn or_reg1_with_reg2(&mut self, reg1: Reg, reg2: Reg) {
		let rh = self.regs.get(reg2);

		self.exec_or(reg1, rh);
		self.clock += 4;
	}

	//Or n
	#[inline(never)]
	fn or_reg_with_hilo_addr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let rh = self.mmu.load_word(addr);

		self.exec_or(reg, rh);
		self.clock += 8;
	}

	//Or n
	#[inline(never)]
	fn xor_reg_with_imm8(&mut self, reg: Reg) {
		let rh = self.get_instr_param();

		self.exec_xor(reg, rh);
		self.clock += 8;
	}

	//Or n
	#[inline(never)]
	fn xor_reg1_with_reg2(&mut self, reg1: Reg, reg2: Reg) {
		let rh = self.regs.get(reg2);

		self.exec_xor(reg1, rh);
		self.clock += 4;
	}

	//Or n
	#[inline(never)]
	fn xor_reg_with_hilo_addr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let rh = self.mmu.load_word(addr);

		self.exec_xor(reg, rh);
		self.clock += 8;
	}

	//Cp n
	#[inline(never)]
	fn cp_reg_with_imm8(&mut self, reg: Reg) {
		let rh = self.get_instr_param();

		self.exec_cp(reg, rh);
		self.clock += 8;
	}

	//Cp n
	#[inline(never)]
	fn cp_reg1_with_reg2(&mut self, reg1: Reg, reg2: Reg) {
		let rh = self.regs.get(reg2);

		self.exec_cp(reg1, rh);
		self.clock += 4;
	}

	//Cp n
	#[inline(never)]
	fn cp_reg_with_hilo_addr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let rh = self.mmu.load_word(addr);

		self.exec_cp(reg, rh);
		self.clock += 8;
	}

	//Inc n
	#[inline(never)]
	fn incr_reg(&mut self, reg: Reg) {
		let value = self.regs.get(reg);

		let (result, has_half_carried, _) = Self::overflowing_add_u8(value, 1);

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, has_half_carried);
		//Carry flag not affected
		self.clock += 4;
	}

	//Inc n
	#[inline(never)]
	fn incr_hilo_addr(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let	addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);

		let (result, has_half_carried, _) = Self::overflowing_add_u8(value, 1);

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, has_half_carried);
		//Carry flag not affected
		self.clock += 12;
	}

	//Inc nn
	#[inline(never)]
	fn incr_hilo(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let mut value = self.get_value_from_hilo(hi_reg, lo_reg);
		value = value.wrapping_add(1);

		self.regs.set(hi_reg, (value >> 8) as u8);
		self.regs.set(lo_reg, value as u8);
		//No flags affected
		self.clock += 8;
	}

	//Inc nn
	#[inline(never)]
	fn incr_sp(&mut self) {
		let mut value = self.regs.sp.get();
		value = value.wrapping_add(1);

		self.regs.sp.set(value);
		//No flags affected
		self.clock += 8;
	}

	//Dec n
	#[inline(never)]
	fn decr_reg(&mut self, reg: Reg) {
		let value = self.regs.get(reg);

		let (result, has_half_borrowed, _) = Self::overflowing_sub_u8(value, 1);

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, true);
		self.regs.set_flag(RegFlag::HalfCarry, !has_half_borrowed);
		//Carry flag not affected
		self.clock += 4;
	}

	//Dec n
	#[inline(never)]
	fn decr_hilo_addr(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let	addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);

		let (result, has_half_borrowed, _) = Self::overflowing_sub_u8(value, 1);

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, true);
		self.regs.set_flag(RegFlag::HalfCarry, !has_half_borrowed);
		//Carry flag not affected
		self.clock += 12;
	}

	//Dec nn
	#[inline(never)]
	fn decr_hilo(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let mut value = self.get_value_from_hilo(hi_reg, lo_reg);
		value = value.wrapping_sub(1);

		self.regs.set(hi_reg, (value >> 8) as u8);
		self.regs.set(lo_reg, value as u8);
		//No flags affected
		self.clock += 8;
	}

	//Dec nn
	#[inline(never)]
	fn decr_sp(&mut self) {
		let mut value = self.regs.sp.get();
		value = value.wrapping_sub(1);

		self.regs.sp.set(value);
		//No flags affected
		self.clock += 8;
	}

	//Swap n
	#[inline(never)]
	fn swap_reg(&mut self, reg: Reg) {
		let value = self.regs.get(reg);
		let result = Self::swap_u8(value);

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, false);
		self.clock += 8;
	}

	//Swap n
	#[inline(never)]
	fn swap_hilo_addr(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let result = Self::swap_u8(value);

		self.mmu.store_word(addr, value);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, false);
		self.clock += 16;
	}

	//RCLA, RLC n
	#[inline(never)]
	fn rotate_reg_left(&mut self, reg: Reg, clock_incr: u64) {
		let value = self.regs.get(reg);
		let result = value.rotate_left(1);

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, value >= 0b1000_0000); //Store bit 7 in Carry flag
		self.clock += clock_incr;
	}

	//RLC n
	#[inline(never)]
	fn rotate_hilo_addr_left(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let result = value.rotate_left(1);

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, value >= 0b1000_0000); //Store bit 7 in Carry flag
		self.clock += 16;
	}

	//RLA, RL n
	#[inline(never)]
	fn rotate_reg_left_through_carry(&mut self, reg: Reg, clock_incr: u64) {
		let value = self.regs.get(reg);
		let carry_flag = self.regs.get_flag(RegFlag::Carry);
		let result = (value << 1) + carry_flag as u8; //Move old carry flag into bit 0

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, value >= 0b1000_0000); //Store bit 7 in Carry flag
		self.clock += clock_incr;
	}

	//RL n
	fn rotate_hilo_addr_left_through_carry(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let carry_flag = self.regs.get_flag(RegFlag::Carry);
		let result = (value << 1) + carry_flag as u8; //Move old carry flag into bit 0

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, value >= 0b1000_0000); //Store bit 7 in Carry flag
		self.clock += 16;
	}

	//RRCA, RRC n
	#[inline(never)]
	fn rotate_reg_right(&mut self, reg: Reg, clock_incr: u64) {
		let value = self.regs.get(reg);
		let result = value.rotate_right(1);

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, (value & 0b0000_0001) == 1); //Store bit 0 in Carry flag
		self.clock += clock_incr;
	}

	//RRC n
	#[inline(never)]
	fn rotate_hilo_addr_right(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let result = value.rotate_right(1);

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, (value & 0b0000_0001) == 1); //Store bit 0 in Carry flag
		self.clock += 16;
	}

	//RRA, RR n
	#[inline(never)]
	fn rotate_reg_right_through_carry(&mut self, reg: Reg, clock_incr: u64) {
		let value = self.regs.get(reg);
		let carry_flag = self.regs.get_flag(RegFlag::Carry);
		let result = ((carry_flag as u8) << 7) + (value >> 1); //Move old carry flag into bit 7

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, (value & 0b0000_0001) == 1); //Store bit 0 in Carry flag
		self.clock += clock_incr;
	}

	//RR n
	#[inline(never)]
	fn rotate_hilo_addr_right_through_carry(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let carry_flag = self.regs.get_flag(RegFlag::Carry);
		let result = ((carry_flag as u8) << 7) + (value >> 1); //Move old carry flag into bit 7

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, (value & 0b0000_0001) == 1); //Store bit 0 in Carry flag
		self.clock += 16;
	}

	//SLA n
	#[inline(never)]
	fn shift_reg_left(&mut self, reg: Reg) {
		let value = self.regs.get(reg);
		let result = value << 1;

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, value >= 0b1000_0000);
		self.clock += 8;
	}

	//SLA n
	#[inline(never)]
	fn shift_hilo_addr_left(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let result = value << 1;

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, value >= 0b1000_0000);
		self.clock += 16;
	}

	//SRA n
	#[inline(never)]
	fn shift_reg_right_preserved(&mut self, reg: Reg) {
		let value = self.regs.get(reg);
		let result = (value & 0b1000_0000) + (value >> 1); //Preserve left-most bit

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, (value & 0b0000_0001) == 1);
		self.clock += 8;
	}

	//SRA n
	#[inline(never)]
	fn shift_hilo_addr_right_preserved(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let result = (value & 0b1000_0000) + (value >> 1); //Preserve left-most bit

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, (value & 0b0000_0001) == 1);
		self.clock += 16;
	}

	//SRL n
	#[inline(never)]
	fn shift_reg_right_reset(&mut self, reg: Reg) {
		let value = self.regs.get(reg);
		let result = value >> 1; //Zero out left-most bit

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, (value & 0b0000_0001) == 1);
		self.clock += 8;
	}

	//SRL n
	#[inline(never)]
	fn shift_hilo_addr_right_reset(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let result = value >> 1; //Zero out left-most bit

		self.mmu.store_word(addr, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, (value & 0b0000_0001) == 1);
		self.clock += 16;
	}

	//Bit b, r
	#[inline(never)]
	fn test_bit_in_reg(&mut self, reg: Reg, bit: u8) {
		let value = self.regs.get(reg);
		
		self.test_bit(value, bit);
		self.clock += 8;
	}

	//Bit b, r
	#[inline(never)]
	fn test_bit_in_hilo_addr(&mut self, hi_reg: Reg, lo_reg: Reg, bit: u8) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		
		self.test_bit(value, bit);
		self.clock += 16;
	}

	#[inline(never)]
	fn test_bit(&mut self, value: u8, bit: u8) {
		let mask = 1 << bit;

		self.regs.set_flag(RegFlag::Zero, (value & mask) > 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, true);
		//Carry flag not affecte
	}

	//Set b, r
	#[inline(never)]
	fn set_bit_in_reg(&mut self, reg: Reg, bit: u8) {
		self.toggle_bit_in_reg(reg, bit, |value, mask| value | mask);
	}

	//Res b, r
	#[inline(never)]
	fn reset_bit_in_reg(&mut self, reg: Reg, bit: u8) {
		self.toggle_bit_in_reg(reg, bit, |value, mask| value & (!mask));
	}

	#[inline(never)]
	fn toggle_bit_in_reg(&mut self, reg: Reg, bit: u8, set_bit: fn(u8, u8) -> u8) {
		let value = self.regs.get(reg);
		let mask = 1 << bit;
		let result = set_bit(value, mask);

		self.regs.set(reg, result);
		//No flags affected
		self.clock += 8;
	}

	//Set b, r
	#[inline(never)]
	fn set_bit_in_hilo_addr(&mut self, hi_reg: Reg, lo_reg: Reg, bit: u8) {
		self.toggle_bit_in_hilo_addr(hi_reg, lo_reg, bit, |value, mask| value | mask);
	}

	//Res b, r
	#[inline(never)]
	fn reset_bit_in_hilo_addr(&mut self, hi_reg: Reg, lo_reg: Reg, bit: u8) {
		self.toggle_bit_in_hilo_addr(hi_reg, lo_reg, bit, |value, mask| value | (!mask));
	}

	#[inline(never)]
	fn toggle_bit_in_hilo_addr(&mut self, hi_reg: Reg, lo_reg: Reg, bit: u8, set_bit: fn(u8, u8) -> u8) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);
		let mask = 1 << bit;
		let result = set_bit(value, mask);

		self.mmu.store_word(addr, result);
		//No flags affected
		self.clock += 16;
	}

	//CPL
	#[inline(never)]
	fn complement_register(&mut self, reg: Reg) {
		let value = self.regs.get(reg);

		self.regs.set(reg, !value);
		self.regs.set_flag(RegFlag::Subtract, true);
		self.regs.set_flag(RegFlag::HalfCarry, true);
		self.clock += 4;
	}

	//SCF
	#[inline(never)]
	fn set_carry_flag(&mut self) {
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, true);
		self.clock += 4;
	}

	//CCF
	#[inline(never)]
	fn complement_carry_flag(&mut self) {
		let carry_flag = self.regs.get_flag(RegFlag::Carry);

		self.regs.set_flag(RegFlag::Carry, !carry_flag);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.clock += 4;
	}

	//JP nn
	#[inline(never)]
	fn jump_to_addr(&mut self) {
		let lo = self.get_instr_param() as u16;
		let hi = self.get_instr_param() as u16;
		let addr = (hi << 8) + lo;

		self.regs.pc.set(addr);
		self.clock += 12;
	}

	//JP cc, nn
	#[inline(never)]
	fn jump_to_addr_if_flag(&mut self, flag: RegFlag, test: fn(bool) -> bool) {
		let lo = self.get_instr_param() as u16;
		let hi = self.get_instr_param() as u16;
		let addr = (hi << 8) + lo;

		let flag_value = self.regs.get_flag(flag);
		if test(flag_value) {
			self.regs.pc.set(addr);
		}

		self.clock += 12;
	}

	//JP (HL)
	#[inline(never)]
	fn jump_to_hilo(&mut self, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);

		self.regs.pc.set(addr);
		self.clock += 4;
	}

	//JR n
	#[inline(never)]
	fn jump_to_current_addr_plus_imm8(&mut self) {
		let current_addr = self.regs.pc.get() as i32; //Increase size to avoid going negative
		let offset = (self.get_instr_param() as i8) as i32; //Convert to i8 first in order to get two's complement conversion right
		let mut target_addr = current_addr + offset;
		if target_addr > 0xFFFF {
			target_addr -= 0x1_0000; //Can't overflow twice, since it's u8
		} else if target_addr < 0x0000 {
			target_addr = 0x1_0000 + target_addr;
		}

		self.regs.pc.set(target_addr as u16);
		self.clock += 8;
	}

	//JR cc, n
	#[inline(never)]
	fn jump_to_current_addr_plus_imm8_if_flag(&mut self, flag: RegFlag, test: fn(bool) -> bool) {
		let flag_value = self.regs.get_flag(flag);
		if test(flag_value) {
			let current_addr = self.regs.pc.get() as i32; //Increase size to avoid going negative
			let offset = (self.get_instr_param() as i8) as i32; //Convert to i8 first in order to get two's complement conversion right
			let mut target_addr = current_addr + offset;
			if target_addr > 0xFFFF {
				target_addr -= 0x1_0000; //Can't overflow twice, since it's u8
			} else if target_addr < 0x0000 {
				target_addr = 0x1_0000 + target_addr;
			}

			self.regs.pc.set(target_addr as u16);
		}
		self.clock += 8;
	}

	//Call nn
	#[inline(never)]
	fn call(&mut self) {
		let lo = self.get_instr_param() as u16;
		let hi = self.get_instr_param() as u16;
		let target_addr = (hi << 8) + lo;
		let next_instr_addr = self.regs.pc.get();

		self.push_value_to_stack((next_instr_addr >> 8) as u8);
		self.push_value_to_stack(next_instr_addr as u8);
		self.regs.pc.set(target_addr);
		self.clock += 12;
	}

	//Call cc, nn
	#[inline(never)]
	fn call_if_flag(&mut self, flag: RegFlag, test: fn(bool) -> bool) {
		let lo = self.get_instr_param() as u16;
		let hi = self.get_instr_param() as u16;
		let target_addr = (hi << 8) + lo;

		let flag_value = self.regs.get_flag(flag);
		if test(flag_value) {
			let next_instr_addr = self.regs.pc.get();

			self.push_value_to_stack((next_instr_addr >> 8) as u8);
			self.push_value_to_stack(next_instr_addr as u8);
			self.regs.pc.set(target_addr);	
		}
		self.clock += 12;
	}

	//Rst n
	#[inline(never)]
	fn call_restart(&mut self, offset: u16) {
		let current_addr = self.regs.pc.get();
		self.push_value_to_stack((current_addr >> 8) as u8);
		self.push_value_to_stack(current_addr as u8);

		self.regs.pc.set(offset);
		self.clock += 32;
	}

	//Ret
	#[inline(never)]
	fn return_to_addr(&mut self) {
		self.exec_return_to_addr();
		self.clock += 8;
	}

	//Ret cc
	#[inline(never)]
	fn return_to_addr_if_flag(&mut self, flag: RegFlag, test: fn(bool) -> bool) {
		let flag_value = self.regs.get_flag(flag);
		if test(flag_value) {
			self.exec_return_to_addr();
		}
		self.clock += 8;
	}

	//RetI
	#[inline(never)]
	fn return_to_addr_and_enable_interrupts(&mut self) {
		let lo = self.pop_value_from_stack();
		let hi = self.pop_value_from_stack();
		let addr = ((hi as u16) << 8) + lo as u16;
		
		self.regs.pc.set(addr);
		self.set_interrupt_enable(true);
		self.clock += 8;
	}

	#[inline(never)]
	fn exec_return_to_addr(&mut self) {
		let lo = self.pop_value_from_stack();
		let hi = self.pop_value_from_stack();
		let addr = ((hi as u16) << 8) + lo as u16;
		
		self.regs.pc.set(addr);
	}

	//DAA
	#[inline(never)]
	fn decimal_adjust_register(&mut self, reg: Reg) {
		let value = self.regs.get(reg);
		let sub_flag = self.regs.get_flag(RegFlag::Subtract);
		let carry_flag = self.regs.get_flag(RegFlag::Carry);
		let half_flag = self.regs.get_flag(RegFlag::HalfCarry);
		let mut result = value;

		//Implementation pulled from here: https://forums.nesdev.com/viewtopic.php?f=20&t=15944
		//Note: assumes reg is a uint8_t and wraps from 0xff to 0
		if !sub_flag { //After an addition, adjust if (half-)carry occurred or if result is out of bounds
			if carry_flag || value > 0x99 {
				result = result.overflowing_add(0x60).0;
				self.regs.set_flag(RegFlag::Carry, true);
			}
			if half_flag || (value & 0x0F) > 0x09 {
				result = result.overflowing_add(0x06).0;
			}
		} else { //After a subtraction, only adjust if (half-)carry occurred
			if carry_flag {
				result = result.overflowing_sub(0x60).0;
			}
			if half_flag {
				result = result.overflowing_sub(0x06).0;
			}
		}

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.clock += 4;
	}

	//EI
	#[inline(never)]
	fn enable_interrupts(&mut self) {
		//Interrupts are actually enabled after the immediately next instruction is executed
		self.clock += 4;

		let next_instr = self.get_instr_param();
		self.execute_instr(next_instr);
		self.set_interrupt_enable(true);
	}

	//DI
	#[inline(never)]
	fn disable_interrupts(&mut self) {
		//Interrupts are actually disabled after the immediately next instruction is executed
		self.clock += 4;

		let next_instr = self.get_instr_param();
		self.execute_instr(next_instr);
		self.set_interrupt_enable(false);
	}

	#[inline(never)]
	fn get_instr_param(&mut self) -> u8 {
		let param_addr = self.regs.pc.get();
		let value = self.mmu.load_word(param_addr);
		self.regs.pc += 1;

		return value;
	}

	#[inline(never)]
	fn get_value_from_hilo(&self, hi_reg: Reg, lo_reg: Reg) -> u16 {
		let hi = self.regs.get(hi_reg) as u16;
		let lo = self.regs.get(lo_reg) as u16;
		return (hi << 8) + lo;
	}

	#[inline(never)]
	fn has_half_carried_u8(before: u8, after: u8) -> bool {
		//((lh & 0x0F) + (rh & 0x0F) & 0x10) == 0x10;
		return (before & 0xF0) < (after & 0xF0);
	}

	#[inline(never)]
	fn has_half_carried_u16(before: u16, after: u16) -> bool {
		//((lh & 0x00FF) + (rh & 0x00FF) & 0x0100) == 0x0100;
		return (before & 0xFF00) < (after & 0xFF00);
	}

	#[inline(never)]
	fn overflowing_add_u8(lh: u8, rh: u8) -> (u8, bool, bool) {
		let (result, has_carried) = lh.overflowing_add(rh);
		let has_half_carried = Self::has_half_carried_u8(lh, result);

		return (result, has_half_carried, has_carried);
	}

	#[inline(never)]
	fn overflowing_add_u16(lh: u16, rh: u16) -> (u16, bool, bool) {
		let (result, has_carried) = lh.overflowing_add(rh);
		let has_half_carried = Self::has_half_carried_u16(lh, result);

		return (result, has_half_carried, has_carried);
	}

	#[inline(never)]
	fn overflowing_sub_u8(lh: u8, rh: u8) -> (u8, bool, bool) {
		let (result, has_borrowed) = lh.overflowing_sub(rh);
		let has_half_borrowed = if has_borrowed { (lh & 0xF0) > 0 } else { (lh & 0xF0) > (result & 0xF0) };

		return (result, has_half_borrowed, has_borrowed);
	}

	#[inline(never)]
	fn increment_hilo(&mut self, hi_reg: Reg, lo_reg: Reg) {
		self.change_hilo(hi_reg, lo_reg, &|mut hi, mut lo| {
			if lo < 0xFF {
				lo += 1;
			} else {
				if hi < 0xFF {
					hi += 1;
				} else {
					hi = 0x00;
				}
				lo = 0x00;
			}
			return (hi, lo);
		});
	}

	#[inline(never)]
	fn decrement_hilo(&mut self, hi_reg: Reg, lo_reg: Reg) {
		self.change_hilo(hi_reg, lo_reg, &|mut hi, mut lo| {
			if lo > 0 {
				lo -= 1;
			} else {
				if hi > 0 {
					hi -= 1;
				} else {
					hi = 0xFF;
				}
				lo = 0xFF;
			}
			return (hi, lo);
		});
	}

	#[inline(never)]
	fn change_hilo(&mut self, hi_reg: Reg, lo_reg: Reg, modify: &Fn(u8, u8) -> (u8, u8)) {
		let (hi, lo) = modify(
			self.regs.get(hi_reg), 
			self.regs.get(lo_reg)
		);

		self.regs.set(hi_reg, hi);
		self.regs.set(lo_reg, lo);
	}

	#[inline(never)]
	fn exec_load_reg_from_hilo_addr(&mut self, reg: Reg, hi_reg: Reg, lo_reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.mmu.load_word(addr);

		self.regs.set(reg, value);
	}

	#[inline(never)]
	fn exec_load_hilo_addr_from_reg(&mut self, hi_reg: Reg, lo_reg: Reg, reg: Reg) {
		let addr = self.get_value_from_hilo(hi_reg, lo_reg);
		let value = self.regs.get(reg);

		self.mmu.store_word(addr, value);
	}

	#[inline(never)]
	fn exec_add(&mut self, reg: Reg, rh: u8) {
		let lh = self.regs.get(reg);

		let (result, has_half_carried, has_carried) = Self::overflowing_add_u8(lh, rh);

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, has_half_carried);
		self.regs.set_flag(RegFlag::Carry, has_carried);
	}

	#[inline(never)]
	fn exec_add16(&mut self, hi_reg: Reg, lo_reg: Reg, rh: u16) {
		let lh = self.get_value_from_hilo(hi_reg, lo_reg);

		let (result, has_half_carried, has_carried) = Self::overflowing_add_u16(lh, rh);

		let hi = (result >> 8) as u8;
		let lo = result as u8;

		self.regs.set(hi_reg, hi);
		self.regs.set(lo_reg, lo);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, has_half_carried);
		self.regs.set_flag(RegFlag::Carry, has_carried);
	}

	#[inline(never)]
	fn exec_sub(&mut self, reg: Reg, rh: u8) {
		let lh = self.regs.get(reg);

		let (result, has_half_borrowed, has_borrowed) = Self::overflowing_sub_u8(lh, rh);

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, true);
		self.regs.set_flag(RegFlag::HalfCarry, !has_half_borrowed);
		self.regs.set_flag(RegFlag::Carry, !has_borrowed);
	}

	#[inline(never)]
	fn exec_and(&mut self, reg: Reg, rh: u8) {
		let lh = self.regs.get(reg);
		let result = lh & rh;

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, true);
		self.regs.set_flag(RegFlag::Carry, false);
	}

	#[inline(never)]
	fn exec_or(&mut self, reg: Reg, rh: u8) {
		let lh = self.regs.get(reg);
		let result = lh | rh;

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, false);
	}

	#[inline(never)]
	fn exec_xor(&mut self, reg: Reg, rh: u8) {
		let lh = self.regs.get(reg);
		let result = lh ^ rh;

		self.regs.set(reg, result);
		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, false);
		self.regs.set_flag(RegFlag::HalfCarry, false);
		self.regs.set_flag(RegFlag::Carry, false);
	}

	#[inline(never)]
	fn exec_cp(&mut self, reg: Reg, rh: u8) {
		let lh = self.regs.get(reg);

		let (result, has_half_borrowed, has_borrowed) = Self::overflowing_sub_u8(lh, rh);

		self.regs.set_flag(RegFlag::Zero, result == 0);
		self.regs.set_flag(RegFlag::Subtract, true);
		self.regs.set_flag(RegFlag::HalfCarry, !has_half_borrowed);
		self.regs.set_flag(RegFlag::Carry, !has_borrowed);
	}

	#[inline(never)]
	fn push_value_to_stack(&mut self, value: u8) {
		self.regs.sp -= 1;
		self.mmu.store_word(self.regs.sp.get(), value);
	}

	#[inline(never)] 
	fn pop_value_from_stack(&mut self) -> u8 {
		let value = self.mmu.load_word(self.regs.sp.get());

		self.regs.sp += 1;
		return value;
	}

	#[inline(never)]
	fn swap_u8(value: u8) -> u8 {
		return (value << 4) + (value >> 4);
	}

	/* Methods only available in unit test builds */
	#[cfg(test)]
	pub fn load_test_rom(&mut self, rom: Vec<u8>) {
		self.mmu.load_test_rom(rom);
	}
}

pub struct CPUBuilder {
	free_write_mode: bool
}

impl CPUBuilder {
	pub fn new() -> Self {
		return Self {
			free_write_mode: false
		};
	}

	pub fn with_free_write_mode(mut self) -> Self {
		self.free_write_mode = true;

		return self;
	}

	pub fn build(&self) -> CPU {
		let mut mmu_builder = MMU::new();
		if self.free_write_mode {
			mmu_builder = mmu_builder.with_free_write_mode();
		}
		let mmu = mmu_builder.build();

		return CPU {
			regs: Registers::new(),
			mmu: mmu,

			clock: Clock::new(),

			clock_speed: CLOCK_SPEED
		};
	}
}