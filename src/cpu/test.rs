#[cfg(test)]
mod tests {
	use std::collections::HashSet;
	
	use cpu::cpu::*;
	use mmu::mmu::*;
	use mmu::mem::mem_ranges;

	fn get_test_rom() -> Vec<u8> {
		return vec![
			0x3E, 0x01, 								//Load 1 into register A
			0x06, 0x01, 								//Load 1 into register B
			0x80,										//Add register A with register B and store in A
			0x26, (mem_ranges::RAM_START >> 8) as u8,	//Load RAM start address into hi register
			0x2E, (mem_ranges::RAM_START & 0xFF) as u8,	//Load RAM start address into lo register
			0x77										//Load addition result into RAM
		];
	}

	fn create_powered_up_cpu() -> CPU {
		let mut cpu = CPU::new()
			.with_free_write_mode()
			.build();
		cpu.power_up();

		return cpu;
	}

	fn load_test_rom(cpu: &mut CPU, rom: &Vec<u8>) {
		cpu.load_test_rom(rom.clone());
		cpu.regs.pc.set(0x00); //Normally starts at 0x0100
	}

	fn execute_n_cpu_cycles(cpu: &mut CPU, times: u64) {
		let mut counter = 0;
		while counter < times {
			cpu.run_cycle();

			counter += 1;
		}
	}

	#[test]
	fn test_power_up() {
		let cpu = create_powered_up_cpu();

		assert_eq!(0x01, cpu.regs.get(Reg::A));
		assert_eq!(0x80, cpu.mmu.load_word(0xFF10));
	}

	#[test]
	fn test_run() {
		let mut cpu = create_powered_up_cpu();
		let rom = get_test_rom();
		load_test_rom(&mut cpu, &rom);

		execute_n_cpu_cycles(&mut cpu, rom.len() as u64);
		assert_eq!(1, 1); //Just make sure it finishes
	}

	struct InstructionTest {
		name: String,
		reg_data: Option<Vec<RegData>>,
		flag_data: Option<Vec<FlagData>>,
		sp_data: Option<RegPointerData>,
		mem_data: Option<Vec<MemData>>,
		instructions: Vec<u8>,
		instruction_count: u64,
		
		expected_clock_change: u64,

		custom_test: Option<fn(&CPU) -> bool>
	}

	impl InstructionTest {
		fn new(name: &str, instructions: &[u8], instruction_count: u64, expected_clock_change: u64) -> Self {
			return Self::new_with_formatted_name(name.to_string(), instructions, instruction_count, expected_clock_change);
		}

		fn new_with_formatted_name(name: String, instructions: &[u8], instruction_count: u64, expected_clock_change: u64) -> Self {
			return Self {
				name: name,
				reg_data: None,
				flag_data: None,
				sp_data: None,
				mem_data: None,
				instructions: instructions.to_vec(),
				instruction_count,
				expected_clock_change,
				custom_test: None
			};
		}

		fn regs(mut self, reg_data: &[(Reg, u8, u8)]) -> Self {
			self.reg_data = Some(reg_data.iter().map(|reg| RegData { reg: reg.0, before: reg.1, after: reg.2 }).collect());

			return self;
		}

		fn flags(mut self, flag_data: &[(RegFlag, bool, bool)]) -> Self {
			self.flag_data = Some(flag_data.iter().map(|flag| FlagData { flag: flag.0, before: flag.1, after: flag.2 }).collect());

			return self;
		}

		fn sp(mut self, before: u16, after: u16) -> Self {
			self.sp_data = Some(RegPointerData { before, after });

			return self;
		}

		fn mem(mut self, mem_data: &[(u16, u8, u8)]) -> Self {
			self.mem_data = Some(mem_data.iter().map(|mem| MemData { address: mem.0, before: mem.1, after: mem.2 }).collect());

			return self;
		}

		fn test(mut self, custom_test: fn(&CPU) -> bool) -> Self {
			self.custom_test = Some(custom_test);

			return self;
		}

		fn run_test(&self) {
			println!("Running test: {}", self.name);

			let mut cpu = self.get_cpu();

			let initial_regs = cpu.regs.clone();
			let initial_memory = cpu.mmu.clone();

			execute_n_cpu_cycles(&mut cpu, self.instruction_count as u64);

			//cpu.regs.set(Reg::C, 32);
			//cpu.mmu.store_word(0xC032, 32);

			self.test_regs(&cpu, &initial_regs);
			self.test_sp(&cpu);
			self.test_memory(&cpu, &initial_memory);
			self.test_clock(&cpu);
			self.run_custom_test(&cpu);

			println!("");
		}

		fn get_cpu(&self) -> CPU {
			let mut cpu = CPU::new()
				.with_free_write_mode()
				.build();

			load_test_rom(&mut cpu, &self.instructions);
			if let Some(regs) = self.reg_data.as_ref() {
				for reg_data in regs {
					cpu.regs.set(reg_data.reg, reg_data.before);
				}
			}

			if let Some(flags) = self.flag_data.as_ref() {
				for flag_data in flags {
					cpu.regs.set_flag(flag_data.flag, flag_data.before);
				}
			}

			if let Some(sp) = self.sp_data.as_ref() {
				cpu.regs.sp.set(sp.before);
			}

			if let Some(mem) = self.mem_data.as_ref() {
				for mem_data in mem {
					cpu.mmu.store_word(mem_data.address, mem_data.before);
				}
			}

			return cpu;
		}

		fn test_regs(&self, cpu: &CPU, initial_regs: &Registers) {
			if self.reg_data.is_none() && self.flag_data.is_none() {
				return;
			}

			let mut regs_changed: HashSet<Reg> = vec![Reg::A, Reg::B, Reg::C, Reg::D, Reg::E, Reg::F, Reg::H, Reg::L]
				.into_iter()
				.collect();

			if let Some(regs) = self.reg_data.as_ref() {
				println!("Comparing expected register values...");
				for result in regs {
					let expected_value = result.after;
					let actual_value = cpu.regs.get(result.reg);

					assert_eq!(
						expected_value, 
						actual_value, 
						"A register value is different than expected\nRegister:\t  {0}\nExpected:\t{1:3} ({1:#08b})\nActual:\t\t{2:3} ({2:#08b})",
						result.reg.to_string(),
						expected_value,
						actual_value,
					);
					regs_changed.remove(&result.reg);
				}
			}

			let mut regs_to_compare = initial_regs.clone();
			self.test_flags(cpu, &mut regs_to_compare);
			
			println!("Comparing registers...");
			match cpu.regs.compare_regs(&regs_to_compare, &regs_changed) {
				CompareRegsResult::Identical => println!("Registers have expected values"),
				CompareRegsResult::Different(reg, expected_value, actual_value) => {
					panic!(
						"A register value is different than expected\nRegister:\t  {0}\nExpected:\t{1:3} ({1:#08b})\nActual:\t\t{2:3} ({2:#08b})",
						reg.to_string(), 
						expected_value,  
						actual_value
					);
				}
			}
		}

		fn test_flags(&self, cpu: &CPU, regs_to_compare: &mut Registers) {
			let flags_to_compare = match self.flag_data.as_ref() {
				Some(flags) => {
					let mut flags_clone = flags.clone();

					let mut explicit_flags: HashSet<RegFlag> = flags_clone
						.iter()
						.map(|flag_data| flag_data.flag)
						.collect();

					for flag in &vec![RegFlag::Zero, RegFlag::Subtract, RegFlag::HalfCarry, RegFlag::Carry] {
						if !explicit_flags.contains(&flag) {
							flags_clone.push(FlagData { flag: *flag, before: false, after: false });
						} 
					}

					flags_clone
				},
				None => {
					vec![
						FlagData { flag: RegFlag::Zero, before: false, after: false },
						FlagData { flag: RegFlag::Subtract, before: false, after: false },
						FlagData { flag: RegFlag::HalfCarry, before: false, after: false },
						FlagData { flag: RegFlag::Carry, before: false, after: false }
					]
				}
			};

			println!("Comparing flags...");
			for result in flags_to_compare {
				let expected_value = result.after;
				let actual_value = cpu.regs.get_flag(result.flag);

				assert_eq!(
					expected_value, 
					actual_value,
					"A register flag is different than expected\nFlag:\t\t{0}\nExpected:\t{1}\nActual:\t\t{2}",
					result.flag.to_string(),
					expected_value,
					actual_value
				);
				regs_to_compare.set_flag(result.flag, expected_value);
			}
		}

		fn test_sp(&self, cpu: &CPU) {
			if self.sp_data.is_none() {
				return;
			}

			println!("Comparing stack pointers...");
			let expected_sp = self.sp_data.as_ref().unwrap().after;
			let actual_sp = cpu.regs.sp.get();
			if expected_sp != actual_sp {
				panic!("Stack pointer value is different than expected\nExpected: 0x{:X}\nActual: 0x{:X}", expected_sp, actual_sp);
			}
		}

		fn test_memory(&self, cpu: &CPU, initial_memory: &MMU) {
			let mut mem_to_compare = initial_memory.clone();

			println!("Comparing memory...");
			if let Some(mem) = self.mem_data.as_ref() {
				for mem_data in mem {
					mem_to_compare.store_word(mem_data.address, mem_data.after);
				}
			}

			match cpu.mmu.compare(&mem_to_compare) {
				CompareMemResult::Identical => println!("Memory has expected values"),
				CompareMemResult::DifferentLengths(actual_mem_length, expected_mem_length) => {
					panic!("Resultant memory length is different.\nExpected: {}\nActual: {}", expected_mem_length, actual_mem_length);
				},
				CompareMemResult::DifferentValues(actual_address, expected_address) => {
					panic!(
						"Resultant memory is different than expected\nAddress 0x{:X}.\nExpected: {}\nActual: {}", 
						expected_address.address, 
						expected_address.value, 
						actual_address.value
					);
				}
			}
		}

		fn test_clock(&self, cpu: &CPU) {
			println!("Checking clock...");
			assert_eq!(self.expected_clock_change, cpu.clock.get());
		}

		fn run_custom_test(&self, cpu: &CPU) {
			if self.custom_test.is_none() {
				return;
			}

			println!("Performing specific tests");
			assert!((self.custom_test.unwrap())(cpu));
		}
	}

	#[derive(Clone)]
	struct RegData {
		reg: Reg,
		before: u8,
		after: u8
	}

	#[derive(Clone)]
	struct FlagData {
		flag: RegFlag,
		before: bool,
		after: bool
	}

	#[derive(Clone)]
	struct RegPointerData {
		before: u16,
		after: u16
	}

	#[derive(Clone)]
	struct MemData {
		address: u16,
		before: u8,
		after: u8
	}

	#[test]
	fn test_load_imm_instructions() {
		[
			InstructionTest::new("LD B, n", &[0x06, 4], 1, 8)
				.regs(&[(Reg::B, 0, 4)]),
			InstructionTest::new("LD C, n", &[0x0E, 8], 1, 8)
				.regs(&[(Reg::C, 0, 8)]),
			InstructionTest::new("LD D, n", &[0x16, 15], 1, 8)
				.regs(&[(Reg::D, 0, 15)]),
			InstructionTest::new("LD E, n", &[0x1E, 16], 1, 8)
				.regs(&[(Reg::E, 0, 16)]),
			InstructionTest::new("LD H, n", &[0x26, 23], 1, 8)
				.regs(&[(Reg::H, 0, 23)]),
			InstructionTest::new("LD L, n", &[0x2E, 42], 1, 8)
				.regs(&[(Reg::L, 0, 42)]),
		].iter().for_each(|instr_set| instr_set.run_test());
	}

	#[test]
	fn test_8_bit_load_register_instructions() {
		[
			//A loads
			InstructionTest::new("LD A, A", &[0x7F], 1, 4)
				.regs(&[(Reg::A, 112, 112)]),
			InstructionTest::new("LD A, B", &[0x78], 1, 4)
				.regs(&[(Reg::A, 0, 12), (Reg::B, 12, 12)]),
			InstructionTest::new("LD A, C", &[0x79], 1, 4)
				.regs(&[(Reg::A, 0, 22), (Reg::C, 22, 22)]),
			InstructionTest::new("LD A, D", &[0x7A], 1, 4)
				.regs(&[(Reg::A, 0, 32), (Reg::D, 32, 32)]),
			InstructionTest::new("LD A, E", &[0x7B], 1, 4)
				.regs(&[(Reg::A, 0, 42), (Reg::E, 42, 42)]),
			InstructionTest::new("LD A, H", &[0x7C], 1, 4)
				.regs(&[(Reg::A, 0, 52), (Reg::H, 52, 52)]),
			InstructionTest::new("LD A, L", &[0x7D], 1, 4)
				.regs(&[(Reg::A, 0, 62), (Reg::L, 62, 62)]),
			InstructionTest::new("LD A, (BC)", &[0x0A], 1, 8)
				.mem(&[(0xC000, 72, 72)])
				.regs(&[(Reg::A, 0, 72), (Reg::B, 0xC0, 0xC0), (Reg::C, 0x00, 0x00)]),
			InstructionTest::new("LD A, (DE)", &[0x1A], 1, 8)
				.mem(&[(0xC000, 82, 82)])
				.regs(&[(Reg::A, 0, 82), (Reg::D, 0xC0, 0xC0), (Reg::E, 0x00, 0x00)]),
			InstructionTest::new("LD A, (HL)", &[0x7E], 1, 8)
				.mem(&[(0xC000, 92, 92)])
				.regs(&[(Reg::A, 0, 92), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x00, 0x00)]),
			InstructionTest::new("LD A, (nn)", &[0xFA, 0x00, 0xC0], 1, 16)
				.mem(&[(0xC000, 102, 102)])
				.regs(&[(Reg::A, 0, 102)]),
			InstructionTest::new("LD A, (0xFF00+C)", &[0xF2], 1, 8)
				.mem(&[(0xFF30, 12, 12)])
				.regs(&[(Reg::A, 0, 12), (Reg::C, 0x30, 0x30)]),
			InstructionTest::new("LD A, (0xFF00+n)", &[0xF0, 0x30], 1, 12)
				.mem(&[(0xFF30, 12, 12)])
				.regs(&[(Reg::A, 0, 12)]),
			InstructionTest::new("LD A, n", &[0x3E, 122], 1, 8)
				.regs(&[(Reg::A, 0, 122)]),
			//B loads
			InstructionTest::new("LD B, A", &[0x47], 1, 4)
				.regs(&[(Reg::B, 0, 2), (Reg::A, 2, 2)]),
			InstructionTest::new("LD B, B", &[0x40], 1, 4)
				.regs(&[(Reg::B, 12, 12)]),
			InstructionTest::new("LD B, C", &[0x41], 1, 4)
				.regs(&[(Reg::B, 0, 22), (Reg::C, 22, 22)]),
			InstructionTest::new("LD B, D", &[0x42], 1, 4)
				.regs(&[(Reg::B, 0, 32), (Reg::D, 32, 32)]),
			InstructionTest::new("LD B, E", &[0x43], 1, 4)
				.regs(&[(Reg::B, 0, 42), (Reg::E, 42, 42)]),
			InstructionTest::new("LD B, H", &[0x44], 1, 4)
				.regs(&[(Reg::B, 0, 52), (Reg::H, 52, 52)]),
			InstructionTest::new("LD B, L", &[0x45], 1, 4)
				.regs(&[(Reg::B, 0, 62), (Reg::L, 62, 62)]),
			InstructionTest::new("LD B, (HL)", &[0x46], 1, 8)
				.mem(&[(0xC000, 72, 72)])
				.regs(&[(Reg::B, 0, 72), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x00, 0x00)]),
			//C loads
			InstructionTest::new("LD C, A", &[0x4F], 1, 4)
				.regs(&[(Reg::C, 0, 2), (Reg::A, 2, 2)]),
			InstructionTest::new("LD C, B", &[0x48], 1, 4)
				.regs(&[(Reg::C, 0, 12), (Reg::B, 12, 12)]),
			InstructionTest::new("LD C, C", &[0x49], 1, 4)
				.regs(&[(Reg::C, 22, 22)]),
			InstructionTest::new("LD C, D", &[0x4A], 1, 4)
				.regs(&[(Reg::C, 0, 32), (Reg::D, 32, 32)]),
			InstructionTest::new("LD C, E", &[0x4B], 1, 4)
				.regs(&[(Reg::C, 0, 42), (Reg::E, 42, 42)]),
			InstructionTest::new("LD C, H", &[0x4C], 1, 4)
				.regs(&[(Reg::C, 0, 52), (Reg::H, 52, 52)]),
			InstructionTest::new("LD C, L", &[0x4D], 1, 4)
				.regs(&[(Reg::C, 0, 62), (Reg::L, 62, 62)]),
			InstructionTest::new("LD C, (HL)", &[0x4E], 1, 8)
				.mem(&[(0xC000, 72, 72)])
				.regs(&[(Reg::C, 0, 72), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x00, 0x00)]),
			//D loads
			InstructionTest::new("LD D, A", &[0x57], 1, 4)
				.regs(&[(Reg::D, 0, 2), (Reg::A, 2, 2)]),
			InstructionTest::new("LD D, B", &[0x50], 1, 4)
				.regs(&[(Reg::D, 0, 12), (Reg::B, 12, 12)]),
			InstructionTest::new("LD D, C", &[0x51], 1, 4)
				.regs(&[(Reg::D, 0, 22), (Reg::C, 22, 22)]),
			InstructionTest::new("LD D, D", &[0x52], 1, 4)
				.regs(&[(Reg::D, 32, 32)]),
			InstructionTest::new("LD D, E", &[0x53], 1, 4)
				.regs(&[(Reg::D, 0, 42), (Reg::E, 42, 42)]),
			InstructionTest::new("LD D, H", &[0x54], 1, 4)
				.regs(&[(Reg::D, 0, 52), (Reg::H, 52, 52)]),
			InstructionTest::new("LD D, L", &[0x55], 1, 4)
				.regs(&[(Reg::D, 0, 62), (Reg::L, 62, 62)]),
			InstructionTest::new("LD D, (HL)", &[0x56], 1, 8)
				.mem(&[(0xC000, 72, 72)])
				.regs(&[(Reg::D, 0, 72), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x00, 0x00)]),
			//E loads
			InstructionTest::new("LD E, A", &[0x5F], 1, 4)
				.regs(&[(Reg::E, 0, 2), (Reg::A, 2, 2)]),
			InstructionTest::new("LD E, B", &[0x58], 1, 4)
				.regs(&[(Reg::E, 0, 12), (Reg::B, 12, 12)]),
			InstructionTest::new("LD E, C", &[0x59], 1, 4)
				.regs(&[(Reg::E, 0, 22), (Reg::C, 22, 22)]),
			InstructionTest::new("LD E, D", &[0x5A], 1, 4)
				.regs(&[(Reg::E, 0, 32), (Reg::D, 32, 32)]),
			InstructionTest::new("LD E, E", &[0x5B], 1, 4)
				.regs(&[(Reg::E, 42, 42)]),
			InstructionTest::new("LD E, H", &[0x5C], 1, 4)
				.regs(&[(Reg::E, 0, 52), (Reg::H, 52, 52)]),
			InstructionTest::new("LD E, L", &[0x5D], 1, 4)
				.regs(&[(Reg::E, 0, 62), (Reg::L, 62, 62)]),
			InstructionTest::new("LD E, (HL)", &[0x5E], 1, 8)
				.mem(&[(0xC000, 72, 72)])
				.regs(&[(Reg::E, 0, 72), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x00, 0x00)]),
			//H loads
			InstructionTest::new("LD H, A", &[0x67], 1, 4)
				.regs(&[(Reg::H, 0, 2), (Reg::A, 2, 2)]),
			InstructionTest::new("LD H, B", &[0x60], 1, 4)
				.regs(&[(Reg::H, 0, 12), (Reg::B, 12, 12)]),
			InstructionTest::new("LD H, C", &[0x61], 1, 4)
				.regs(&[(Reg::H, 0, 22), (Reg::C, 22, 22)]),
			InstructionTest::new("LD H, D", &[0x62], 1, 4)
				.regs(&[(Reg::H, 0, 32), (Reg::D, 32, 32)]),
			InstructionTest::new("LD H, E", &[0x63], 1, 4)
				.regs(&[(Reg::H, 0, 42), (Reg::E, 42, 42)]),
			InstructionTest::new("LD H, H", &[0x64], 1, 4)
				.regs(&[(Reg::H, 52, 52)]),
			InstructionTest::new("LD H, L", &[0x65], 1, 4)
				.regs(&[(Reg::H, 0, 62), (Reg::L, 62, 62)]),
			InstructionTest::new("LD H, (HL)", &[0x66], 1, 8)
				.mem(&[(0xC000, 72, 72)])
				.regs(&[(Reg::H, 0xC0, 72), (Reg::L, 0x00, 0x00)]),
			//L loads
			InstructionTest::new("LD L, A", &[0x6F], 1, 4)
				.regs(&[(Reg::L, 0, 2), (Reg::A, 2, 2)]),
			InstructionTest::new("LD L, B", &[0x68], 1, 4)
				.regs(&[(Reg::L, 0, 12), (Reg::B, 12, 12)]),
			InstructionTest::new("LD L, C", &[0x69], 1, 4)
				.regs(&[(Reg::L, 0, 22), (Reg::C, 22, 22)]),
			InstructionTest::new("LD L, D", &[0x6A], 1, 4)
				.regs(&[(Reg::L, 0, 32), (Reg::D, 32, 32)]),
			InstructionTest::new("LD L, E", &[0x6B], 1, 4)
				.regs(&[(Reg::L, 0, 42), (Reg::E, 42, 42)]),
			InstructionTest::new("LD L, H", &[0x6C], 1, 4)
				.regs(&[(Reg::L, 0, 52), (Reg::H, 52, 52)]),
			InstructionTest::new("LD L, L", &[0x6D], 1, 4)
				.regs(&[(Reg::L, 62, 62)]),
			InstructionTest::new("LD L, (HL)", &[0x6E], 1, 8)
				.mem(&[(0xC000, 72, 72)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x00, 72)]),
			//(HL) loads
			InstructionTest::new("LD (BC), A", &[0x02], 1, 8)
				.mem(&[(0xC010, 0, 12)])
				.regs(&[(Reg::B, 0xC0, 0xC0), (Reg::C, 0x10, 0x10), (Reg::A, 12, 12)]),
			InstructionTest::new("LD (DE), A", &[0x12], 1, 8)
				.mem(&[(0xC010, 0, 12)])
				.regs(&[(Reg::D, 0xC0, 0xC0), (Reg::E, 0x10, 0x10), (Reg::A, 12, 12)]),
			InstructionTest::new("LD (HL), A", &[0x77], 1, 8)
				.mem(&[(0xC010, 0, 12)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10), (Reg::A, 12, 12)]),
			InstructionTest::new("LD (HL), B", &[0x70], 1, 8)
				.mem(&[(0xC010, 0, 12)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10), (Reg::B, 12, 12)]),
			InstructionTest::new("LD (HL), C", &[0x71], 1, 8)
				.mem(&[(0xC010, 0, 22)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10), (Reg::C, 22, 22)]),
			InstructionTest::new("LD (HL), D", &[0x72], 1, 8)
				.mem(&[(0xC010, 0, 32)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10), (Reg::D, 32, 32)]),
			InstructionTest::new("LD (HL), E", &[0x73], 1, 8)
				.mem(&[(0xC010, 0, 42)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10), (Reg::E, 42, 42)]),
			InstructionTest::new("LD (HL), H", &[0x74], 1, 8)
				.mem(&[(0xC010, 0, 0xC0)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10)]),
			InstructionTest::new("LD (HL), L", &[0x75], 1, 8)
				.mem(&[(0xC010, 0, 0x10)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10)]),
			InstructionTest::new("LD (HL), n", &[0x36, 72], 1, 12)
				.mem(&[(0xC010, 0, 72)])
				.regs(&[(Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10)]),
			InstructionTest::new("LD (0xFF00+C), A", &[0xE2], 1, 8).
				mem(&[(0xFF30, 0, 32)])
				.regs(&[(Reg::A, 32, 32), (Reg::C, 0x30, 0x30)]),
			InstructionTest::new("LD (0xFF00+n), A", &[0xE0, 0x30], 1, 12)
				.regs(&[(Reg::A, 12, 12)]).mem(&[(0xFF30, 0, 12)]),
			InstructionTest::new("LD (nn), A", &[0xEA, 0x10, 0xC0], 1, 16)
				.mem(&[(0xC010, 0, 82)])
				.regs(&[(Reg::A, 82, 82)])
		].iter().for_each(|instr_set| instr_set.run_test());
	}

	#[test]
	fn test_16_bit_load_register_instructions() {
		[
			InstructionTest::new("LD BC, nn", &[0x01, 19, 95], 1, 12)
				.regs(&[(Reg::B, 0, 19), (Reg::C, 0, 95)]),
			InstructionTest::new("LD DE, nn", &[0x11, 20, 96], 1, 12)
				.regs(&[(Reg::D, 0, 20), (Reg::E, 0, 96)]),
			InstructionTest::new("LD HL, nn", &[0x21, 21, 97], 1, 12)
				.regs(&[(Reg::H, 0, 21), (Reg::L, 0, 97)]),
			InstructionTest::new("LD HL, SP+n Positive", &[0xF8, 8i8 as u8], 1, 12)
				.regs(&[(Reg::H, 0, 0xC1), (Reg::L, 0, 0x18)])
				.sp(0xC110, 0xC110),
			InstructionTest::new("LD HL, SP+n Positive Half Carry 1", &[0xF8, 100i8 as u8], 1, 12)
				.flags(&[(RegFlag::HalfCarry, false, true)])
				.regs(&[(Reg::H, 0, 0x01), (Reg::L, 0, 0x54)])
				.sp(0x00F0, 0x00F0),
			InstructionTest::new("LD HL, SP+n Positive Half Carry 2", &[0xF8, 96i8 as u8], 1, 12)
				.flags(&[(RegFlag::HalfCarry, false, true)])
				.regs(&[(Reg::H, 0, 0xC2), (Reg::L, 0, 0x45)])
				.sp(0xC1E5, 0xC1E5),
			InstructionTest::new("LD HL, SP+n Positive Half Carry Edge 1", &[0xF8, 1i8 as u8], 1, 12)
				.flags(&[(RegFlag::HalfCarry, false, false)])
				.regs(&[(Reg::H, 0, 0x14), (Reg::L, 0, 0xFF)])
				.sp(0x14FE, 0x14FE),
			InstructionTest::new("LD HL, SP+n Positive Half Carry Edge 2", &[0xF8, 2i8 as u8], 1, 12)
				.flags(&[(RegFlag::HalfCarry, false, true)])
				.regs(&[(Reg::H, 0, 0x15), (Reg::L, 0, 0x00)])
				.sp(0x14FE, 0x14FE),
			InstructionTest::new("LD HL, SP+n Positive Carry 1", &[0xF8, 11i8 as u8], 1, 12)
				.flags(&[(RegFlag::Carry, false, true)])
				.regs(&[(Reg::H, 0, 0x00), (Reg::L, 0, 0x04)])
				.sp(0xFFF9, 0xFFF9),
			InstructionTest::new("LD HL, SP+n Positive Carry 2", &[0xF8, 44i8 as u8], 1, 12)
				.flags(&[(RegFlag::Carry, false, true)])
				.regs(&[(Reg::H, 0, 0x00), (Reg::L, 0, 0x0D)])
				.sp(0xFFE1, 0xFFE1),
			InstructionTest::new("LD HL, SP+n Positive Carry Edge 1", &[0xF8, 1i8 as u8], 1, 12)
				.flags(&[(RegFlag::Carry, false, false)])
				.regs(&[(Reg::H, 0, 0xFF), (Reg::L, 0, 0xFF)])
				.sp(0xFFFE, 0xFFFE),
			InstructionTest::new("LD HL, SP+n Positive Carry Edge 2", &[0xF8, 2i8 as u8], 1, 12)
				.flags(&[(RegFlag::Carry, false, true)])
				.regs(&[(Reg::H, 0, 0x00), (Reg::L, 0, 0x00)])
				.sp(0xFFFE, 0xFFFE),
			InstructionTest::new("LD HL, SP+n Negative", &[0xF8, -8i8 as u8], 1, 12)
				.regs(&[(Reg::H, 0, 0xC1), (Reg::L, 0, 0x08)])
				.sp(0xC110, 0xC110),
			InstructionTest::new("LD HL, SP+n Negative Half Borrow 1", &[0xF8, -100i8 as u8], 1, 12)
				.regs(&[(Reg::H, 0, 0x00), (Reg::L, 0, 0xF0)])
				.sp(0x0154, 0x0154),
			InstructionTest::new("LD HL, SP+n Negative Half Borrow 2", &[0xF8, -83i8 as u8], 1, 12)
				.regs(&[(Reg::H, 0, 0x88), (Reg::L, 0, 0xDF)])
				.sp(0x8932, 0x8932),
			InstructionTest::new("LD SP, HL", &[0xF9], 1, 8).sp(0, 0x23A0)
				.regs(&[(Reg::H, 0x23, 0x23), (Reg::L, 0xA0, 0xA0)]),
			InstructionTest::new("LD SP, nn", &[0x31, 0x23, 0x99], 1, 12)
				.sp(0x0000, 0x2399),
			InstructionTest::new("LD (nn), SP", &[0x08, 0x10, 0xC0], 1, 20)
				.mem(&[(0xC010, 0, 0x56), (0xC011, 0, 0x01)])
				.sp(0x0156, 0x0156)
		].iter().for_each(|instr_set| instr_set.run_test());
	}

	#[test]
	fn test_load_increment_decrement_instructions() {
		[
			InstructionTest::new("LDI A, (HL)", &[0x2A], 1, 8)
				.mem(&[(0xC010, 12, 12)])
				.regs(&[(Reg::A, 0, 12), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x11)]),
			InstructionTest::new("LDI A, (HL) Overflow", &[0x2A], 1, 8)
				.mem(&[(0xFFFF, 12, 12)])
				.regs(&[(Reg::A, 0, 12), (Reg::H, 0xFF, 0x00), (Reg::L, 0xFF, 0x00)]),
			InstructionTest::new("LDI (HL), A", &[0x22], 1, 8)
				.mem(&[(0xC010, 0, 22)])
				.regs(&[(Reg::A, 22, 22), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x11)]),			
			InstructionTest::new("LDI (HL), A Overflow", &[0x22], 1, 8)
				.mem(&[(0xFFFF, 0, 22)])
				.regs(&[(Reg::A, 22, 22), (Reg::H, 0xFF, 0x00), (Reg::L, 0xFF, 0x00)]),			
			InstructionTest::new("LDD A, (HL)", &[0x3A], 1, 8)
				.mem(&[(0xC010, 12, 12)])
				.regs(&[(Reg::A, 0, 12), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x0F)]),
			InstructionTest::new("LDD A, (HL) Underflow", &[0x3A], 1, 8)
				.regs(&[(Reg::A, 0, 0x3A), (Reg::H, 0x00, 0xFF), (Reg::L, 0x00, 0xFF)]),
			InstructionTest::new("LDD (HL), A", &[0x32], 1, 8)
				.mem(&[(0xC010, 0, 22)])
				.regs(&[(Reg::A, 22, 22), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x0F)]),			
			InstructionTest::new("LDD (HL), A Underflow", &[0x32], 1, 8)
				.mem(&[(0x0000, 0x32, 22)])
				.regs(&[(Reg::A, 22, 22), (Reg::H, 0x00, 0xFF), (Reg::L, 0x00, 0xFF)]),
		].iter().for_each(|instr_set| instr_set.run_test());
	}

	#[test]
	fn test_push_pop_instructions() {
		//Pushes
		[
			InstructionTest::new("PUSH AF", &[0xF5], 1, 16)
				.flags(&[(RegFlag::Subtract, true, true), (RegFlag::Carry, true, true)])
				.mem(&[(0xC323, 0, 12), (0xC322, 0, 80)])
				.regs(&[(Reg::A, 12, 12)])
				.sp(0xC324, 0xC322),
			InstructionTest::new("PUSH AF Underflow", &[0xF5], 1, 16)
				.flags(&[(RegFlag::Subtract, true, true), (RegFlag::Carry, true, true)])
				.mem(&[(0xFFFF, 0, 12), (0xFFFE, 0, 80)])
				.regs(&[(Reg::A, 12, 12)])
				.sp(0x0000, 0xFFFE),
			InstructionTest::new("PUSH BC", &[0xC5], 1, 16)
				.mem(&[(0xC335, 0, 13), (0xC334, 0, 49)])
				.regs(&[(Reg::B, 13, 13), (Reg::C, 49, 49)])
				.sp(0xC336, 0xC334),
			InstructionTest::new("PUSH BC Underflow", &[0xC5], 1, 16)
				.mem(&[(0xFFFF, 0, 13), (0xFFFE, 0, 49)])
				.regs(&[(Reg::B, 13, 13), (Reg::C, 49, 49)])
				.sp(0x0000, 0xFFFE),
			InstructionTest::new("PUSH DE", &[0xD5], 1, 16)
				.mem(&[(0xC346, 0, 14), (0xC345, 0, 50)])
				.regs(&[(Reg::D, 14, 14), (Reg::E, 50, 50)])
				.sp(0xC347, 0xC345),
			InstructionTest::new("PUSH DE Underflow", &[0xD5], 1, 16)
				.mem(&[(0xFFFF, 0, 14), (0xFFFE, 0, 50)])
				.regs(&[(Reg::D, 14, 14), (Reg::E, 50, 50)])
				.sp(0x0000, 0xFFFE),
			InstructionTest::new("PUSH HL", &[0xE5], 1, 16)
				.mem(&[(0xC357, 0, 15), (0xC356, 0, 51)])
				.regs(&[(Reg::H, 15, 15), (Reg::L, 51, 51)])
				.sp(0xC358, 0xC356),
			InstructionTest::new("PUSH HL Underflow", &[0xE5], 1, 16)
				.mem(&[(0xFFFF, 0, 15), (0xFFFE, 0, 51)])
				.regs(&[(Reg::H, 15, 15), (Reg::L, 51, 51)])
				.sp(0x0000, 0xFFFE),
		].iter().for_each(|instr_set| instr_set.run_test());

		//Pops
		[
			InstructionTest::new("POP AF", &[0xF1], 1, 12)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::Carry, false, true)])
				.mem(&[(0xC323, 12, 12), (0xC322, 80, 80)])
				.regs(&[(Reg::A, 0, 12)])
				.sp(0xC322, 0xC324),
			InstructionTest::new("POP AF Overflow", &[0xF1], 1, 12)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::Carry, false, true)])
				.mem(&[(0xFFFF, 12, 12), (0xFFFE, 80, 80)])
				.regs(&[(Reg::A, 0, 12)])
				.sp(0xFFFE, 0x0000),
			InstructionTest::new("POP BC", &[0xC1], 1, 12)
				.mem(&[(0xC334, 13, 13), (0xC333, 49, 49)])
				.regs(&[(Reg::B, 0, 13), (Reg::C, 0, 49)])
				.sp(0xC333, 0xC335),
			InstructionTest::new("POP BC Overflow", &[0xC1], 1, 12)
				.mem(&[(0xFFFF, 13, 13), (0xFFFE, 49, 49)])
				.regs(&[(Reg::B, 0, 13), (Reg::C, 0, 49)])
				.sp(0xFFFE, 0x0000),
			InstructionTest::new("POP DE", &[0xD1], 1, 12)
				.mem(&[(0xC345, 14, 14), (0xC344, 50, 50)])
				.regs(&[(Reg::D, 0, 14), (Reg::E, 0, 50)])
				.sp(0xC344, 0xC346),
			InstructionTest::new("POP DE Overflow", &[0xD1], 1, 12)
				.mem(&[(0xFFFF, 14, 14), (0xFFFE, 50, 50)])
				.regs(&[(Reg::D, 0, 14), (Reg::E, 0, 50)])
				.sp(0xFFFE, 0x0000),
			InstructionTest::new("POP HL", &[0xE1], 1, 12)
				.mem(&[(0xC356, 15, 15), (0xC355, 51, 51)])
				.regs(&[(Reg::H, 0, 15), (Reg::L, 0, 51)])
				.sp(0xC355, 0xC357),
			InstructionTest::new("POP HL Overflow", &[0xE1], 1, 12)
				.mem(&[(0xFFFF, 15, 15), (0xFFFE, 51, 51)])
				.regs(&[(Reg::H, 0, 15), (Reg::L, 0, 51)])
				.sp(0xFFFE, 0x0000),
		].iter().for_each(|instr_set| instr_set.run_test());
	}

	#[test]
	fn test_8_bit_add_instructions() {
		struct AddInstruction { opcode: u8, reg: Reg }

		//Test Register adds
		for instr in &vec![
			AddInstruction { opcode: 0x87, reg: Reg::A },
			AddInstruction { opcode: 0x80, reg: Reg::B },
			AddInstruction { opcode: 0x81, reg: Reg::C },
			AddInstruction { opcode: 0x82, reg: Reg::D },
			AddInstruction { opcode: 0x83, reg: Reg::E },
			AddInstruction { opcode: 0x84, reg: Reg::H },
			AddInstruction { opcode: 0x85, reg: Reg::L },
		] {
			[
				InstructionTest::new_with_formatted_name(format!("Add A, {}", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false)])
					.regs(&[(Reg::A, 1, 2), (instr.reg, 1, if instr.reg == Reg::A { 2 } else { 1 })]),
				InstructionTest::new_with_formatted_name(format!("Add A, {} Zero", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 128, 0), (instr.reg, 128, if instr.reg == Reg::A { 0 } else { 128 })]),
				InstructionTest::new_with_formatted_name(format!("Add A, {} Half Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true)])
					.regs(&[(Reg::A, 0x0A, 0x14), (instr.reg, 0x0A, if instr.reg == Reg::A { 0x14 } else { 0x0A })]),
				InstructionTest::new_with_formatted_name(format!("Add A, {} Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 0xDE, 0xBC), (instr.reg, 0xDE, if instr.reg == Reg::A { 0xBC } else { 0xDE })]),
			].iter().for_each(|instr_set| instr_set.run_test());
		}

		//Test Register address add
		[
			InstructionTest::new("Add A, (HL)", &[0x86], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false)])
				.mem(&[(0xC010, 1, 1)])
				.regs(&[(Reg::A, 1, 2), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10)]),
			InstructionTest::new("Add A, (HL) Zero", &[0x86], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
				.mem(&[(0xC121, 230, 230)])
				.regs(&[(Reg::A, 26, 0), (Reg::H, 0xC1, 0xC1), (Reg::L, 0x21, 0x21)]),
			InstructionTest::new("Add A, (HL) Half Carry", &[0x86], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true)])
				.mem(&[(0xC232, 31, 31)])
				.regs(&[(Reg::A, 5, 36), (Reg::H, 0xC2, 0xC2), (Reg::L, 0x32, 0x32)]),
			InstructionTest::new("Add A, (HL) Carry", &[0x86], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
				.mem(&[(0xC343, 49, 49)])
				.regs(&[(Reg::A, 231, 24), (Reg::H, 0xC3, 0xC3), (Reg::L, 0x43, 0x43)]),
		].iter().for_each(|instr_set| instr_set.run_test());

		//Test register immediate add
		[
			InstructionTest::new("Add A, n", &[0xC6, 14], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false)])
				.regs(&[(Reg::A, 1, 15)]),
			InstructionTest::new("Add A, n Zero", &[0xC6, 44], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 212, 0)]),
			InstructionTest::new("Add A, n Half Carry", &[0xC6, 15], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true)])
				.regs(&[(Reg::A, 1, 16)]),
			InstructionTest::new("Add A, n Carry", &[0xC6, 81], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 176, 1)]),
		].iter().for_each(|instr_set| instr_set.run_test());
	}

	#[test]
	fn test_8_bit_add_carry_instructions() {
		struct AddInstruction { opcode: u8, reg: Reg }

		//Test Register adds
		for instr in &vec![
			AddInstruction { opcode: 0x8F, reg: Reg::A },
			AddInstruction { opcode: 0x88, reg: Reg::B },
			AddInstruction { opcode: 0x89, reg: Reg::C },
			AddInstruction { opcode: 0x8A, reg: Reg::D },
			AddInstruction { opcode: 0x8B, reg: Reg::E },
			AddInstruction { opcode: 0x8C, reg: Reg::H },
			AddInstruction { opcode: 0x8D, reg: Reg::L },
		] {
			[
				InstructionTest::new_with_formatted_name(format!("ADC A, {}", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false)])
					.regs(&[(Reg::A, 1, 2), (instr.reg, 1, if instr.reg == Reg::A { 2 } else { 1 })]),
				InstructionTest::new_with_formatted_name(format!("ADC A, {} Carried", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, true, false)])
					.regs(&[(Reg::A, 1, 3), (instr.reg, 1, if instr.reg == Reg::A { 3 } else { 1 })]),
				InstructionTest::new_with_formatted_name(format!("ADC A, {} Zero", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 128, 0), (instr.reg, 128, if instr.reg == Reg::A { 0 } else { 128 })]),
				if instr.reg == Reg::A {
					//Not actually possible to total to zero when adding A to itself with the carry flag on
					InstructionTest::new("ADC A, A Zero Carried", &[instr.opcode], 1, 4)
						.flags(&[(RegFlag::Zero, false, false), (RegFlag::Subtract, true, false), (RegFlag::Carry, true, true)])
						.regs(&[(Reg::A, 128, 1)])
				} else {
					InstructionTest::new_with_formatted_name(format!("ADC A, {} Zero Carried", instr.reg.to_string()), &[instr.opcode], 1, 4)
						.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, true, true)])
						.regs(&[(Reg::A, 128, 0), (instr.reg, 127, 127)])
				},
				InstructionTest::new_with_formatted_name(format!("ADC A, {} Half Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true)])
					.regs(&[(Reg::A, 0x0A, 0x14), (instr.reg, 0x0A, if instr.reg == Reg::A { 0x14 } else { 0x0A })]),
				InstructionTest::new_with_formatted_name(format!("ADC A, {} Carried Half Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, false)])
					.regs(&[(Reg::A, 0x0A, 0x15), (instr.reg, 0x0A, if instr.reg == Reg::A { 0x15 } else { 0x0A })]),
				InstructionTest::new_with_formatted_name(format!("ADC A, {} Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 0xDE, 0xBC), (instr.reg, 0xDE, if instr.reg == Reg::A { 0xBC } else { 0xDE })]),
				InstructionTest::new_with_formatted_name(format!("ADC A, {} Carried Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, true, true)])
					.regs(&[(Reg::A, 0xDE, 0xBD), (instr.reg, 0xDE, if instr.reg == Reg::A { 0xBD } else { 0xDE })]),
			].iter().for_each(|instr_set| instr_set.run_test());
		}

		//Test Register address add
		[
			InstructionTest::new("ADC A, (HL)", &[0x8E], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false)])
				.mem(&[(0xC010, 1, 1)])
				.regs(&[(Reg::A, 1, 2), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10)]),
			InstructionTest::new("ADC A, (HL) Carried", &[0x8E], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, true, false)])
				.mem(&[(0xC010, 1, 1)])
				.regs(&[(Reg::A, 1, 3), (Reg::H, 0xC0, 0xC0), (Reg::L, 0x10, 0x10)]),
			InstructionTest::new("ADC A, (HL) Zero", &[0x8E], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
				.mem(&[(0xC121, 230, 230)])
				.regs(&[(Reg::A, 26, 0), (Reg::H, 0xC1, 0xC1), (Reg::L, 0x21, 0x21)]),
			InstructionTest::new("ADC A, (HL) Carried Zero", &[0x8E], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, true, true)])
				.mem(&[(0xC121, 230, 230)])
				.regs(&[(Reg::A, 25, 0), (Reg::H, 0xC1, 0xC1), (Reg::L, 0x21, 0x21)]),
			InstructionTest::new("ADC A, (HL) Half Carry", &[0x8E], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true)])
				.mem(&[(0xC232, 31, 31)])
				.regs(&[(Reg::A, 5, 36), (Reg::H, 0xC2, 0xC2), (Reg::L, 0x32, 0x32)]),
			InstructionTest::new("ADC A, (HL) Carried Half Carry", &[0x8E], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, false)])
				.mem(&[(0xC232, 31, 31)])
				.regs(&[(Reg::A, 5, 37), (Reg::H, 0xC2, 0xC2), (Reg::L, 0x32, 0x32)]),
			InstructionTest::new("ADC A, (HL) Carry", &[0x8E], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
				.mem(&[(0xC343, 49, 49)])
				.regs(&[(Reg::A, 231, 24), (Reg::H, 0xC3, 0xC3), (Reg::L, 0x43, 0x43)]),
			InstructionTest::new("ADC A, (HL) Carried Carry", &[0x8E], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, true, true)])
				.mem(&[(0xC343, 49, 49)])
				.regs(&[(Reg::A, 231, 25), (Reg::H, 0xC3, 0xC3), (Reg::L, 0x43, 0x43)]),
		].iter().for_each(|instr_set| instr_set.run_test());

		//Test register immediate add
		[
			InstructionTest::new("ADC A, n", &[0xCE, 14], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false)])
				.regs(&[(Reg::A, 1, 15)]),
			InstructionTest::new("ADC A, n Carried", &[0xCE, 13], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, true, false)])
				.regs(&[(Reg::A, 1, 15)]),
			InstructionTest::new("ADC A, n Zero", &[0xCE, 44], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 212, 0)]),
			InstructionTest::new("ADC A, n Carried Zero", &[0xCE, 43], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, true, false), (RegFlag::Carry, true, true)])
				.regs(&[(Reg::A, 212, 0)]),
			InstructionTest::new("ADC A, n Half Carry", &[0xCE, 15], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true)])
				.regs(&[(Reg::A, 1, 16)]),
			InstructionTest::new("ADC A, n Carried Half Carry", &[0xCE, 15], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, false)])
				.regs(&[(Reg::A, 1, 17)]),
			InstructionTest::new("ADC A, n Carry", &[0xCE, 81], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 176, 1)]),
			InstructionTest::new("ADC A, n Carried Carry", &[0xCE, 81], 1, 8)
				.flags(&[(RegFlag::Subtract, true, false), (RegFlag::Carry, true, true)])
				.regs(&[(Reg::A, 176, 2)]),
		].iter().for_each(|instr_set| instr_set.run_test());
	}

	#[test]
	fn test_8_bit_sub_instructions() {
		struct SubInstruction { opcode: u8, reg: Reg }

		//Impossible to sub A with itself and not get zero
		[
			InstructionTest::new("Sub A, A", &[0x97], 1, 4)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 2, 0)]),
			InstructionTest::new("Sub A, A Half Carry", &[0x97], 1, 4)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 212, 0)])
		].iter().for_each(|instr_set| instr_set.run_test());

		//Test Register subs
		for instr in &vec![
			SubInstruction { opcode: 0x90, reg: Reg::B },
			SubInstruction { opcode: 0x91, reg: Reg::C },
			SubInstruction { opcode: 0x92, reg: Reg::D },
			SubInstruction { opcode: 0x93, reg: Reg::E },
			SubInstruction { opcode: 0x94, reg: Reg::H },
			SubInstruction { opcode: 0x95, reg: Reg::L },
		] {
			[
				InstructionTest::new_with_formatted_name(format!("Sub A, {}", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 8, 4), (instr.reg, 4, 4)]),
				InstructionTest::new_with_formatted_name(format!("Sub A, {} Zero", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 15, 0), (instr.reg, 15, 15)]),
				InstructionTest::new_with_formatted_name(format!("Sub A, {} Half Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 0x13, 0x09), (instr.reg, 0x0A, 0x0A)]),
				InstructionTest::new_with_formatted_name(format!("Sub A, {} Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, false)])
					.regs(&[(Reg::A, 0x09, 0xFE), (instr.reg, 0x0B, 0x0B)]),
			].iter().for_each(|instr_set| instr_set.run_test());
		}

		//Test Register address sub
		[
			InstructionTest::new("Sub A, (HL)", &[0x96], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.mem(&[(0xE442, 10, 10)])
				.regs(&[(Reg::A, 12, 2), (Reg::H, 0xE4, 0xE4), (Reg::L, 0x42, 0x42)]),
			InstructionTest::new("Sub A, (HL) Zero", &[0x96], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.mem(&[(0xE554, 6, 6)])
				.regs(&[(Reg::A, 6, 0), (Reg::H, 0xE5, 0xE5), (Reg::L, 0x54, 0x54)]),
			InstructionTest::new("Sub A, (HL) Half Carry", &[0x96], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, false, true)])
				.mem(&[(0xE665, 0x17, 0x17)])
				.regs(&[(Reg::A, 0x1C, 0x05), (Reg::H, 0xE6, 0xE6), (Reg::L, 0x65, 0x65)]),
			InstructionTest::new("Sub A, (HL) Carry", &[0x96], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, false)])
				.mem(&[(0xE776, 0x08, 0x08)])
				.regs(&[(Reg::A, 0x04, 0xFC), (Reg::H, 0xE7, 0xE7), (Reg::L, 0x76, 0x76)]),
		].iter().for_each(|instr_set| instr_set.run_test());

		//Test Register immediate sub
		[
			InstructionTest::new("Sub A, n", &[0xD6, 6], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 7, 1)]),
			InstructionTest::new("Sub A, n Zero", &[0xD6, 7], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 7, 0)]),
			InstructionTest::new("Sub A, n Half Carry", &[0xD6, 0x08], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 0x23, 0x1B)]),
			InstructionTest::new("Sub A, n Carry", &[0xD6, 0x08], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, false)])
				.regs(&[(Reg::A, 0x07, 0xFF)]),
		].iter().for_each(|instr_set| instr_set.run_test());
	}

	#[test]
	fn test_8_bit_sub_carry_instructions() {
		struct SubInstruction { opcode: u8, reg: Reg }

		//Impossible to sub A with itself and not get zero with carry off; impossible to get zero with it on
		[
			InstructionTest::new("SBC A, A", &[0x9F], 1, 4)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 2, 0)]),
			InstructionTest::new("SBC A, A Carried", &[0x9F], 1, 4)
				.flags(&[(RegFlag::Zero, false, false), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, false)])
				.regs(&[(Reg::A, 2, 255)]),
			InstructionTest::new("SBC A, A Half Carry", &[0x9F], 1, 4)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 212, 0)]),
			InstructionTest::new("SBC A, A Carried Half Carry", &[0x9F], 1, 4)
				.flags(&[(RegFlag::Zero, false, false), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, true, false)])
				.regs(&[(Reg::A, 212, 255)])
		].iter().for_each(|instr_set| instr_set.run_test());

		//Test Register subs
		for instr in &vec![
			SubInstruction { opcode: 0x98, reg: Reg::B },
			SubInstruction { opcode: 0x99, reg: Reg::C },
			SubInstruction { opcode: 0x9A, reg: Reg::D },
			SubInstruction { opcode: 0x9B, reg: Reg::E },
			SubInstruction { opcode: 0x9C, reg: Reg::H },
			SubInstruction { opcode: 0x9D, reg: Reg::L },
		] {
			[
				InstructionTest::new_with_formatted_name(format!("SBC A, {}", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 8, 4), (instr.reg, 4, 4)]),
				InstructionTest::new_with_formatted_name(format!("SBC A, {} Carried", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, true)])
					.regs(&[(Reg::A, 8, 3), (instr.reg, 4, 4)]),
				InstructionTest::new_with_formatted_name(format!("SBC A, {} Zero", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 15, 0), (instr.reg, 15, 15)]),
				InstructionTest::new_with_formatted_name(format!("SBC A, {} Zero Carried", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, true)])
					.regs(&[(Reg::A, 15, 0), (instr.reg, 14, 14)]),
				InstructionTest::new_with_formatted_name(format!("SBC A, {} Half Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, false, true)])
					.regs(&[(Reg::A, 0x13, 0x09), (instr.reg, 0x0A, 0x0A)]),
				InstructionTest::new_with_formatted_name(format!("SBC A, {} Carried Half Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, true, true)])
					.regs(&[(Reg::A, 0x13, 0x08), (instr.reg, 0x0A, 0x0A)]),
				InstructionTest::new_with_formatted_name(format!("SBC A, {} Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, false)])
					.regs(&[(Reg::A, 0x09, 0xFE), (instr.reg, 0x0B, 0x0B)]),
				InstructionTest::new_with_formatted_name(format!("SBC A, {} Carried Carry", instr.reg.to_string()), &[instr.opcode], 1, 4)
					.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, false)])
					.regs(&[(Reg::A, 0x09, 0xFD), (instr.reg, 0x0B, 0x0B)]),
			].iter().for_each(|instr_set| instr_set.run_test());
		}

		//Test Register address sub
		[
			InstructionTest::new("SBC A, (HL)", &[0x9E], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.mem(&[(0xE442, 10, 10)])
				.regs(&[(Reg::A, 12, 2), (Reg::H, 0xE4, 0xE4), (Reg::L, 0x42, 0x42)]),
			InstructionTest::new("SBC A, (HL) Carried", &[0x9E], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, true)])
				.mem(&[(0xE442, 10, 10)])
				.regs(&[(Reg::A, 12, 1), (Reg::H, 0xE4, 0xE4), (Reg::L, 0x42, 0x42)]),
			InstructionTest::new("SBC A, (HL) Zero", &[0x9E], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.mem(&[(0xE554, 6, 6)])
				.regs(&[(Reg::A, 6, 0), (Reg::H, 0xE5, 0xE5), (Reg::L, 0x54, 0x54)]),
			InstructionTest::new("SBC A, (HL) Carried Zero", &[0x9E], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, true)])
				.mem(&[(0xE554, 5, 5)])
				.regs(&[(Reg::A, 6, 0), (Reg::H, 0xE5, 0xE5), (Reg::L, 0x54, 0x54)]),
			InstructionTest::new("SBC A, (HL) Half Carry", &[0x9E], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, false, true)])
				.mem(&[(0xE665, 0x17, 0x17)])
				.regs(&[(Reg::A, 0x1C, 0x05), (Reg::H, 0xE6, 0xE6), (Reg::L, 0x65, 0x65)]),
			InstructionTest::new("SBC A, (HL) Carried Half Carry", &[0x9E], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, true, true)])
				.mem(&[(0xE665, 0x17, 0x17)])
				.regs(&[(Reg::A, 0x1C, 0x04), (Reg::H, 0xE6, 0xE6), (Reg::L, 0x65, 0x65)]),
			InstructionTest::new("SBC A, (HL) Carry", &[0x9E], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, false)])
				.mem(&[(0xE776, 0x08, 0x08)])
				.regs(&[(Reg::A, 0x04, 0xFC), (Reg::H, 0xE7, 0xE7), (Reg::L, 0x76, 0x76)]),
			InstructionTest::new("SBC A, (HL) Carried Carry", &[0x9E], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, false)])
				.mem(&[(0xE776, 0x08, 0x08)])
				.regs(&[(Reg::A, 0x04, 0xFB), (Reg::H, 0xE7, 0xE7), (Reg::L, 0x76, 0x76)]),
		].iter().for_each(|instr_set| instr_set.run_test());

		//Test Register immediate sub
		[
			InstructionTest::new("Sub A, n", &[0xDE, 6], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 7, 1)]),
			InstructionTest::new("Sub A, n Carried", &[0xDE, 5], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, true)])
				.regs(&[(Reg::A, 7, 1)]),
			InstructionTest::new("Sub A, n Zero", &[0xDE, 7], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 7, 0)]),
			InstructionTest::new("Sub A, n Carried Zero", &[0xDE, 6], 1, 8)
				.flags(&[(RegFlag::Zero, false, true), (RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, true)])
				.regs(&[(Reg::A, 7, 0)]),
			InstructionTest::new("Sub A, n Half Carry", &[0xDE, 0x08], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, false, true)])
				.regs(&[(Reg::A, 0x23, 0x1B)]),
			InstructionTest::new("Sub A, n Carried Half Carry", &[0xDE, 0x08], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, false), (RegFlag::Carry, true, true)])
				.regs(&[(Reg::A, 0x23, 0x1A)]),
			InstructionTest::new("Sub A, n Carry", &[0xDE, 0x08], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, false, false)])
				.regs(&[(Reg::A, 0x07, 0xFF)]),
			InstructionTest::new("Sub A, n Carried Carry", &[0xDE, 0x08], 1, 8)
				.flags(&[(RegFlag::Subtract, false, true), (RegFlag::HalfCarry, false, true), (RegFlag::Carry, true, false)])
				.regs(&[(Reg::A, 0x07, 0xFE)]),
		].iter().for_each(|instr_set| instr_set.run_test());
	}
}