extern crate sdl2;

use std::fs::File;
use std::io::Read;
use std::process;

mod cpu;
mod gb;
mod log;
mod mmu;
mod video;

use gb::GB;

fn main() {
	let file_name = "./data/pk-r.gb";
	let file = File::open(&file_name).unwrap_or_else(|_| {
		println!("The file with the name \"{}\" was not found", &file_name);
		process::exit(1);
	});

	let bytes: Vec<u8> = file.bytes()
		.map(|byte| if let Ok(byte) = byte { byte } else { 0 })
		.collect();
	
	let mut gb = GB::new();
	gb.load(bytes).unwrap_or_else(|error| {
		println!("{}", error);
		process::exit(1);
	});
	gb.run_n_cycles(100);
}