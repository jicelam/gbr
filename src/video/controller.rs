use sdl2::{Sdl};
use sdl2::pixels::{Color};
use sdl2::render::{Canvas};
use sdl2::video::{Window};

pub struct Video {
	canvas: Canvas<Window>
}

impl Video {
	pub fn new(sdl_context: &Sdl) -> Self {
		let video_subsystem = sdl_context.video().unwrap();

		let window = video_subsystem.window("Demo", 800, 600)
			.position_centered()
			.build()
			.unwrap();

		return Self { 
			canvas: window
				.into_canvas()
				.build()
				.unwrap()
		};
	}

	pub fn render(&mut self, video_ram: &[u8]) {
		//Main GB screen buffer consists of 256x256 pixels, or 32x32 tiles (8 pixels each)
		//Only 164x144 pixels on screen at one time (registers SCROLLX and SCROLLY determine position)
		//Backgrounds wrap around screen
		
		//32x32 byte are in VRAM called Background Tile Map contains tile numbers to be displayed
		//Taken from Tile Data Table from 0x8000-0x8FFF (where patterns are unsigned, i.e. pattern #0 lies at 0x8000) 
		//Used for sprites, background, and window
		//Or taken from 0x8800-0x97FF (signed, i.e. pattern #0 lies at 0x9000)
		//Used for background and window display
		//Tile Data Table address for background can be selected from LCDC register (0xFF40, bit 4)
		
		//Also a "window" overlaying the background
		//Non-scrollable, always displayed starting from upper left corner
		//Controlled with WNDPOSX and WNDPOSY registers (0xFF4A and 0xFF4B)
		//Tile numbers for window stored in Tile Data Table
		//Background and Window can be disabled via LCDC register (bit 0 and bit 5)

		//If window open and scan line interrupt disables it (either setting LCDC or setting WX to > 166)
		//but is then re-enabled slightly later by another scan line interrupt, then position of the window
		//is not changed

		//Tile images stored in Tile Pattern Tables
		//Each 8x8 image occupies 16 bytes, where each 2 bytes represent a line (each half-nibble represents a color)
		//Example: part of the letter "A"
		// 333(3)3  | 	-> 0111(1)100
		//				-> 0111(1)100
		//2(2)   22 |	-> 0(0)000000
		//				-> 1(1)000110
		//(1)1   11 |	-> (1)1000110
		//				-> (0)0000000
		//222222(2) |	-> 000000(0)0
		//				-> 111111(1)0
		//33   (3)3 |	-> 11000(1)10
		//				-> 11000(1)10

		//Can display up to 40 sprites, either in 8x8 or 8x16 pixels
		//Taken from 0x8000-0x8FFF
		//Sprite attributes located in Sprite Attribute Table (OAM - Object Attribute Memory) at 0xFE00-0xFE9F
		//OAM divided into 40 4-byte blocks each corresponding to a sprite
		//In 8x16 sprite mode, least significant bit of sprite pattern number is ignored and treated as 0
		//If sprites with different X coordinates overlap, the smaller X coordinate one appears above
		//If sprites with same X coordinates overlap, take precedence from table ordering (0xFE00 highest, 0xFE04 next, etc.)
		//sprite_upper_left_corner_x == sprite_x - 8
		//sprite_upper_left_corner_y == sprite_y - 16
		//X=0, Y=0 hides the sprite; x=8, y=16 displays it in the upper left corner
		//Only 10 sprites per scan line (lower priority sprites dropped when exceeded)
		//Unused sprites can have Y set to 0 or 144+16 to stop from affecting those onscreen
		//Setting X to 0 or 160+8 will hide the sprite, but will still affect others on the same line

		//Blocks have following format:
		//Byte 0: Y position on screen
		//Byte 1: X position on screen
		//Byte 2: Pattern number (0-255) (least significant byte treated as 0 in 8x16 mode)
		//Byte 3: Flags
		//	- Bit 7: Priority (if 0, sprite displayed on top of background and window. If 1, then hidden behind colors 1, 2, and 3 of background and window)
		//	- Bit 6: Flipped vertically if set to 1
		//	- Bit 5: Flipped horizontally if set to 1
		//	- Bit 4: Palette number (taken from OBP1 (0xFF49) if 1, OBP0 (0xFF48) if 0)

		//A Sprite RAM bug exists in the hardware that causes trash to be written to 0AM if the following commands
		//are used while their 16-bit content is between 0xFE00-0xFEFF:
		//	- Inc XX
		//	- Dec XX
		//	- LDI A (HL)
		//	- LDD A (HL)
		//	- LDI (HL) A
		//	- LDD (HL) A
		//Sprites 1 (0xFE00) and 2 (0xFE04) are unaffected

		self.canvas.set_draw_color(Color::RGB(0, 64, 255));
		self.canvas.clear();

		self.canvas.present();
	}
}