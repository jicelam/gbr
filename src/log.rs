extern crate ansi_term;

use self::ansi_term::Colour;

static mut TERMINAL_INITIALIZED: bool = false;

fn init() {
	unsafe {
		if TERMINAL_INITIALIZED {
			return;
		}

		TERMINAL_INITIALIZED = ansi_term::enable_ansi_support().is_ok();
	}
}

pub fn info(message: String) {
	if cfg!(test) {
		init();
		println!("{}", Colour::Cyan.paint(format!("Info: {}", message)));
	}
}

pub fn warning(message: String) {
	if cfg!(test) {
		init();
		println!("{}", Colour::Yellow.paint(format!("Warning: {}", message)));
	}
}

pub fn error(message: String) {
	if cfg!(test) {
		init();
		println!("{}", Colour::Red.paint(format!("Error: {}", message)));
	}
}