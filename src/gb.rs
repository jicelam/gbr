#![allow(dead_code, unused_variables)]

use sdl2;
use sdl2::{Sdl};
use sdl2::event::{Event};
use sdl2::keyboard::{Keycode};

use cpu::cpu::{CPU};
use video::controller::{Video};

pub struct GB {
	cpu: CPU,
	video: Option<Video>,

	is_running: bool,
	sdl_context: Sdl
}

impl GB {
	pub fn new() -> Self {
		let sdl_context = sdl2::init().unwrap();

		return Self {
			cpu: CPU::new().build(),
			video: None,

			is_running: false,
			sdl_context: sdl_context
		};
	}

	pub fn use_video(mut self: Self) -> Self {
		self.video = Some(Video::new(&self.sdl_context));

		return self;
	}

	pub fn load(&mut self, rom: Vec<u8>) -> Result<(), String> {
		return self.cpu.load_rom(rom);
	}

	pub fn run(&mut self) {
		self.cpu.power_up();

		self.is_running = true;

		let mut event_pump = self.sdl_context.event_pump().unwrap();
		while self.is_running {
			self.cpu.run_cycle();

			if let Some(video) = self.video.as_mut() {
				video.render(&self.cpu.mmu.get_video_ram());
			}

			for event in event_pump.poll_iter() {
				match event {
					Event::Quit { .. } | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
						self.is_running = false;
					},
					_ => {}
				}
			}
		}
	}

	pub fn run_n_cycles(&mut self, times: u64) {
		self.cpu.power_up();

		self.is_running = true;

		let mut counter = 0;
		while self.is_running && counter < times {
			self.cpu.run_cycle();

			counter += 1;
		}

		self.is_running = false;
	}
}