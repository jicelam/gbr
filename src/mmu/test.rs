#[cfg(test)]
mod tests {
	use std::collections::{HashMap,HashSet};
	use std::fs::File;
	use std::io::Read;

	use mmu::mem::*;
	use mmu::mem::mem_ranges::*;
	use mmu::mmu::*;

	struct AddressValuePair {
		address: u16,
		value: u8
	}

	impl AddressValuePair {
		pub fn new(addr: u16, value: u8) -> AddressValuePair {
			return AddressValuePair {
				address: addr,
				value: value
			};
		}
	}

	fn get_mmu() -> MMU {
		return MMU::new()
			.with_free_write_mode()
			.build();
	}

	fn convert_ram_addr_to_echoed(addr: u16) -> u16 {
		return addr - RAM_START + ECHOED_RAM_START;
	}

	fn convert_echoed_addr_to_ram(addr: u16) -> u16 {
		return addr - ECHOED_RAM_START + RAM_START;
	}

	fn load_test_rom_file(file_name: Option<&str>) -> Vec<u8> {
		let file_name = file_name.unwrap_or("m2-ros");
		let file_name = format!("./data/{}.gb", file_name);

		let file = File::open(file_name).unwrap();

		let bytes: Vec<u8> = file.bytes()
			.map(|byte| if let Ok(byte) = byte { byte } else { 0 })
			.collect();

		return bytes;
	}

	fn assert_memory_banks_eq(starting_addr: u16, left: &[u8], right: &[u8], message: &str) {
		assert_eq!(left.len(), right.len(), "The left and right memory banks have different lengths");

		for addr in 0..left.len() {
			assert_eq!(left[addr], right[addr], "{}. Value at 0x{:04X} is incorrect", message, starting_addr + addr as u16)
		}
	}

	fn assert_memory_banks_ne(starting_addr: u16, left: &[u8], right: &[u8], message: &str) {
		assert_eq!(starting_addr, starting_addr);
		assert_eq!(left.len(), right.len(), "The left and right memory banks have different lengths");

		let mut difference_found = false;
		for addr in 0..left.len() {
			if left[addr] != right[addr] {
				difference_found = true;
				break;
			}
		}

		assert_eq!(difference_found, true, "{}", message);
	}

	fn compare_memory_banks(starting_addr: u16, left: &[u8], right: &[u8], addresses: &[u16], message: &str) {
		assert_eq!(left.len(), right.len(), "The left and right memory banks have different lengths");
		
		let addr_set: HashSet<usize> = Vec::from(addresses).
			into_iter()
			.map(|value| (value - starting_addr) as usize)
			.collect();

		for addr in 0..left.len() {
			if addr_set.contains(&addr) {
				assert_ne!(left[addr], right[addr], "{}. Value at 0x{:04X} is incorrect", message, starting_addr + addr as u16);
			} else {
				assert_eq!(left[addr], right[addr], "{}. Value at 0x{:04X} is incorrect", message, starting_addr + addr as u16)
			}
		}
	}

	#[test]
	fn store_single() {
		let mut mmu = get_mmu();

		let address = 0x0050;
		let value = 155;
		mmu.store_word(address, value);
		assert_eq!(mmu.load_word(address), value);
	}

	#[test]
	fn store_single_echoed() {
		let mut mmu = get_mmu();

		let ram = AddressValuePair::new(0xD045, 55);
		mmu.store_word(ram.address, ram.value);
		assert_eq!(mmu.load_word(ram.address), ram.value);
		assert_eq!(mmu.load_word(convert_ram_addr_to_echoed(ram.address)), ram.value);

		let echoed_ram = AddressValuePair::new(0xE812, 71);
		mmu.store_word(echoed_ram.address, echoed_ram.value);
		assert_eq!(mmu.load_word(echoed_ram.address), echoed_ram.value);
		assert_eq!(mmu.load_word(convert_echoed_addr_to_ram(echoed_ram.address)), echoed_ram.value);
	}

	#[test]
	fn store_multi() {
		let mut mmu = get_mmu();

		let pairs: [AddressValuePair; 10] = [
			AddressValuePair::new(0x0031, 250), AddressValuePair::new(0xFF29, 42), 
			AddressValuePair::new(0xC112, 16), AddressValuePair::new(0xCC81, 7), 
			AddressValuePair::new(0xB222, 150),AddressValuePair::new(0x9123, 8), 
			AddressValuePair::new(0x3002, 108), AddressValuePair::new(0x4125, 15), 
			AddressValuePair::new(0xE099, 4), AddressValuePair::new(0xDDD0, 23)
		];
		for pair in pairs.iter() {
			mmu.store_word(pair.address, pair.value);
		} 
		for pair in pairs.iter() {
			println!("Checking value at address {0:X}", pair.address);
			assert_eq!(mmu.load_word(pair.address), pair.value);
		}
	}

	#[test]
	fn store_edges() {
		let mut mmu = get_mmu();

		let pairs: [AddressValuePair; 19] = [
			AddressValuePair::new(0x0000, 10), AddressValuePair::new(0x3FFF, 20), 
			AddressValuePair::new(0x4000, 30), AddressValuePair::new(0x7FFF, 40),
			AddressValuePair::new(0x8000, 50), AddressValuePair::new(0x9FFF, 60), 
			AddressValuePair::new(0xA000, 70), AddressValuePair::new(0xBFFF, 80), 
			AddressValuePair::new(0xFE00, 130), AddressValuePair::new(0xFE9F, 140), 
			AddressValuePair::new(0xFEA0, 150), AddressValuePair::new(0xFEFF, 160), 
			AddressValuePair::new(0xFF00, 170), AddressValuePair::new(0xFF4B, 180), 
			AddressValuePair::new(0xFF4C, 190), AddressValuePair::new(0xFF7F, 200), 
			AddressValuePair::new(0xFF80, 210), AddressValuePair::new(0xFFFE, 220), 
			AddressValuePair::new(0xFFFF, 230)
		];

		println!("Storing values...");
		for pair in pairs.iter() {
			mmu.store_word(pair.address, pair.value);
		} 
		for pair in pairs.iter() {
			println!("Checking value at address {0:X}", pair.address);
			assert_eq!(mmu.load_word(pair.address), pair.value);
		}
	}

	#[test]
	fn store_echoed_edges() {
		let mut mmu = get_mmu();

		println!("Checking RAM start");
		let ram_start = AddressValuePair::new(RAM_START, 50);
		mmu.store_word(ram_start.address, ram_start.value);
		assert_eq!(mmu.load_word(ram_start.address), ram_start.value);
		assert_eq!(mmu.load_word(ECHOED_RAM_START), ram_start.value);

		println!("Checking RAM end");
		let ram_end = AddressValuePair::new(RAM_END, 100);
		mmu.store_word(ram_end.address, ram_end.value);
		assert_eq!(mmu.load_word(ram_end.address), ram_end.value);
		assert_ne!(mmu.load_word(ECHOED_RAM_END), ram_end.value);

		println!("Checking Echoed RAM start");
		let echoed_ram_start = AddressValuePair::new(ECHOED_RAM_START, 200);
		mmu.store_word(echoed_ram_start.address, echoed_ram_start.value);
		assert_eq!(mmu.load_word(echoed_ram_start.address), echoed_ram_start.value);
		assert_eq!(mmu.load_word(RAM_START), echoed_ram_start.value);

		println!("Checking Echoed RAM end");
		let echoed_ram_end = AddressValuePair::new(ECHOED_RAM_END, 250);
		mmu.store_word(echoed_ram_end.address, echoed_ram_end.value);
		assert_eq!(mmu.load_word(echoed_ram_end.address), echoed_ram_end.value);
		assert_eq!(mmu.load_word(convert_echoed_addr_to_ram(ECHOED_RAM_END)), echoed_ram_end.value);
	}

	#[test]
	fn load_short_rom() {
		let mut mmu = get_mmu();

		let rom: Vec<u8> = vec![
			0x3E, 0x01, 					//Load 1 into register A
			0x06, 0x01, 					//Load 1 into register B
			0x80,							//Add register A with register B and store in A
			0x26, (RAM_START >> 8) as u8,	//Load RAM start address into hi register
			0x2E, (RAM_START & 0xFF) as u8,	//Load RAM start address into lo register
			0x77							//Load addition result into RAM
		];

		mmu.load_test_rom(rom.clone());

		//Assume short roms are loaded straight into cartridge header space
		for (index, instr_addr) in rom.iter().enumerate() {
			assert_eq!(mmu.load_word(index as u16), *instr_addr);
		}
	}

	#[test]
	fn load_full_rom() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("loz-la"));
		mmu.load_rom(test_rom.clone()).unwrap();

		assert_eq!(mmu.get_rom_bank_number(), 0x01, "The ROM bank is incorrect");
		assert_eq!(mmu.get_memory_bank_mode(), MemoryBankMode::Mbc1, "The memory bank mode is incorrect");
		assert_eq!(mmu.get_rom_ram_size_mode(), RomRamSizeMode::Rom2MByteRam8KByte, "The ROM RAM size mode is incorrect");
	}

	#[test]
	fn switch_memory_mode_mbc1() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("loz-la"));
		mmu.load_rom(test_rom.clone()).unwrap();

		assert_eq!(mmu.get_rom_ram_size_mode(), RomRamSizeMode::Rom2MByteRam8KByte, "The ROM RAM size mode is incorrect");

		let rom_bank_0 = mmu.get_ram().get_rom_bank_0().clone();
		let rom_bank_length = rom_bank_0.len();
		let rom_bank_1 = &test_rom[rom_bank_length * 1..rom_bank_length * 1 + rom_bank_length];

		println!("Switching to 512 KB ROM/32 KB RAM mode...");
		mmu.store_word(0x06400, 0x01);
		assert_eq!(mmu.get_rom_bank_number(), 0x01, "The ROM bank number is incorrect");
		assert_eq!(mmu.get_rom_ram_size_mode(), RomRamSizeMode::Rom512KByteRam32KByte, "The RAM ROM size mode is incorrect");
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
		assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], &rom_bank_1[..], "ROM bank 1 has changed");

		println!("Switching back to 2 MByte ROM, 8 KByte RAM mode...");
		mmu.store_word(0x06200, 0x00);
		assert_eq!(mmu.get_rom_bank_number(), 0x01, "The ROM bank number is incorrect");
		assert_eq!(mmu.get_rom_ram_size_mode(), RomRamSizeMode::Rom2MByteRam8KByte, "The RAM ROM size mode is incorrect");
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
		assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], &rom_bank_1[..], "ROM bank 1 has changed");
	}

	#[test]
	fn switch_rom_banks_small_rom_mbc1() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("loz-la"));
		mmu.load_rom(test_rom.clone()).unwrap();

		let rom_bank_0 = mmu.get_ram().get_rom_bank_0().clone();
		let rom_bank_length = rom_bank_0.len();

		let banks_to_check: Vec<u8> = vec![2, 4, 9, 7, 7, 2, 1];

		let banks: HashMap<u8, &[u8]> = banks_to_check
			.iter()
			.map(|number| (*number, &test_rom[rom_bank_length * *number as usize..rom_bank_length * *number as usize + rom_bank_length]))
			.collect();

		for bank_number in banks_to_check {
			println!("Switching to ROM bank {}...", bank_number);

			mmu.store_word(0x2800, bank_number);

			let bank = banks.get(&bank_number).unwrap();
			assert_eq!(mmu.get_rom_bank_number(), bank_number, "The ROM bank number is incorrect");
			assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
			assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], bank, &format!("ROM bank {} has changed", bank_number).to_string());
		}
	}

	#[test]
	fn switch_rom_banks_large_rom_mbc1() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("pk-r"));
		mmu.load_rom(test_rom.clone()).unwrap();
		mmu.set_memory_bank_mode(MemoryBankMode::Mbc1); //pk-r is normally Mbc3

		assert_eq!(mmu.get_rom_ram_size_mode(), RomRamSizeMode::Rom2MByteRam8KByte, "The ROM RAM size mode is incorrect");

		let rom_bank_0 = mmu.get_ram().get_rom_bank_0().clone();
		let rom_bank_length = rom_bank_0.len();

		let banks_to_check: Vec<u8> = vec![8, 15, 16, 23, 42, 50, 31, 8, 42, 1];

		let banks: HashMap<u8, &[u8]> = banks_to_check
			.iter()
			.map(|number| (*number, &test_rom[rom_bank_length * *number as usize..rom_bank_length * *number as usize + rom_bank_length]))
			.collect();

		/* Test writes to rom bank 0, where only banks 0-31 may be selected */
		println!("Testing ROM bank 0 writes...");
		for bank_number in &banks_to_check {
			let bank_number = *bank_number;
			println!("Switching to ROM bank {}...", bank_number);

			mmu.store_word(0x2800, bank_number);
			assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
			
			let bank = banks.get(&bank_number).unwrap();
			if bank_number < 32 {
				assert_eq!(mmu.get_rom_bank_number(), bank_number, "The ROM bank number is incorrect");
				assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], bank, &format!("ROM bank {} has changed", bank_number).to_string());
			} else {
				assert_eq!(mmu.get_rom_bank_number(), bank_number & 0b0001_1111, "The ROM bank number is incorrect");
				if mmu.get_rom_bank_number() != bank_number {
					assert_memory_banks_ne(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], bank, &format!("ROM bank {} hasn't changed", bank_number).to_string());
				}
			}
		}

		/* Test writes to switchable rom bank, where banks 0-127 may be selected */
		println!("Testing switchable ROM writes...");
		for bank_number in &banks_to_check {
			let bank_number = *bank_number;
			println!("Switching to ROM bank {}...", bank_number);

			mmu.store_word(0x3100, bank_number & 0b0001_1111);
			mmu.store_word(0x4400, (bank_number & 0b0110_0000) >> 5);
			assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
			
			let bank = banks.get(&bank_number).unwrap();
			assert_eq!(mmu.get_rom_bank_number(), bank_number, "The ROM bank number is incorrect");
			if mmu.get_rom_bank_number() != bank_number {
				assert_memory_banks_ne(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], bank, &format!("ROM bank {} hasn't changed", bank_number).to_string());
			}
		}
	}

	#[test]
	fn switch_inaccessible_rom_banks_mbc1() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("pk-r"));
		mmu.load_rom(test_rom.clone()).unwrap();
		mmu.set_memory_bank_mode(MemoryBankMode::Mbc1); //pk-r is normally Mbc3

		assert_eq!(mmu.get_rom_ram_size_mode(), RomRamSizeMode::Rom2MByteRam8KByte, "The ROM RAM size mode is incorrect");

		let rom_bank_0 = mmu.get_ram().get_rom_bank_0().clone();
		let rom_bank_length = rom_bank_0.len();

		let banks_to_check: Vec<u8> = vec![0, 20, 40, 60];

		let banks: HashMap<u8, &[u8]> = banks_to_check
			.iter()
			.map(|number| *number + 1)
			.map(|number| (number, &test_rom[rom_bank_length * number as usize..rom_bank_length * number as usize + rom_bank_length]))
			.collect();

		for bank_number in &banks_to_check {
			let bank_number = *bank_number;
			println!("Switching to ROM bank {}...", bank_number);

			mmu.store_word(0x3100, bank_number & 0b0001_1111);
			mmu.store_word(0x4400, (bank_number & 0b0110_0000) >> 5);
			assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
			
			let adjusted_bank_number = bank_number + 1;
			let bank = banks.get(&adjusted_bank_number).unwrap();
			assert_eq!(mmu.get_rom_bank_number(), adjusted_bank_number, "The ROM bank number is incorrect");
			if mmu.get_rom_bank_number() != adjusted_bank_number {
				assert_memory_banks_ne(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], bank, &format!("ROM bank {} hasn't changed", bank_number).to_string());
			}
		}
	}

	#[test]
	fn switch_ram_banks_mbc1() {
		let mut mmu = MMU::new().build();
		
		let test_rom = load_test_rom_file(Some("loz-la"));
		mmu.load_rom(test_rom.clone()).unwrap();

		/* Write different data into external RAM, since it will initialize to 0 */
		mmu.get_ram_mut().set_switchable_ram_bank(&[1; 8_192]);

		assert_eq!(mmu.load_word(CartridgeMemoryLocations::RamSize as u16), RamSize::KByte8 as u8);
		assert_eq!(mmu.get_external_ram_bank_operations_enabled(), false, "Switchable RAM writes are enabled");
		
		let rom_bank_0 = mmu.get_ram().get_rom_bank_0().clone();
		let rom_bank_length = rom_bank_0.len();
		let rom_bank_1 = &test_rom[rom_bank_length * 1..rom_bank_length * 1 + rom_bank_length];
		let switchable_ram_bank = mmu.get_ram().get_switchable_ram_bank().clone();

		let target_bank_write_addr = 0xA300;
		let target_bank_number = 0x01;

		println!("Writing to switchable RAM when writes disabled...");
		mmu.store_word(target_bank_write_addr, target_bank_number);
		assert_memory_banks_eq(SWITCHABLE_RAM_BANK_START, &mmu.get_ram().get_switchable_ram_bank()[..], &switchable_ram_bank[..], "Switchable RAM has changed");

		println!("Enabling Switchable RAM writes...");
		mmu.store_word(RAM_BANK_0_START + 0x0100, 0b0010_1010);
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
		assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], &rom_bank_1[..], "ROM bank 1 has changed");
		assert_eq!(mmu.get_external_ram_bank_operations_enabled(), true, "Switchable RAM writes are still disabled");
		
		println!("Switching to 512 KB ROM/32 KB RAM mode...");
		mmu.store_word(0x6900, 0b1110_0001);
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
		assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], &rom_bank_1[..], "ROM bank 1 has changed");
		assert_eq!(mmu.get_rom_ram_size_mode(), RomRamSizeMode::Rom512KByteRam32KByte, "The RAM ROM size mode is incorrect");

		println!("Switching to RAM bank 1...");
		mmu.store_word(SWITCHABLE_ROM_BANK_START + 0x1100, 1);
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
		assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], &rom_bank_1[..], "ROM bank 1 has changed");
		assert_memory_banks_ne(SWITCHABLE_RAM_BANK_START, &mmu.get_ram().get_switchable_ram_bank()[..], &switchable_ram_bank[..], "Switchable RAM hasn't changed");

		println!("Switching back to RAM bank 0...");
		mmu.store_word(SWITCHABLE_ROM_BANK_START + 0x0700, 0);
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
		assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], &rom_bank_1[..], "ROM bank 1 has changed");
		assert_memory_banks_eq(SWITCHABLE_RAM_BANK_START, &mmu.get_ram().get_switchable_ram_bank()[..], &switchable_ram_bank[..], "Switchable RAM has changed");
	}

	#[test]
	fn read_ram_banks_mbc2() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("loz-la"));
		mmu.load_rom(test_rom.clone()).unwrap();
		mmu.set_memory_bank_mode(MemoryBankMode::Mbc2);
		mmu.set_external_ram_bank_operations_enabled(true);

		let raw_value = 0b1111_0011;
		let stored_value = raw_value & 0b0000_1111;
		mmu.get_ram_mut().set_switchable_ram_bank(&[raw_value; 8_192]);

		for addr in 0xA000..0xBFFF {
			let value = mmu.load_word(addr);
			if addr <= 0xA1FF {
				assert_eq!(value, stored_value, "Value read from 0x{:04X} is incorrect", addr);
			} else {
				assert_ne!(value, stored_value, "Value read from 0x{:04X} is incorrect", addr);
			}
		}
	}

	#[test]
	fn toggle_ram_bank_operations_mbc2() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("loz-la"));
		mmu.load_rom(test_rom.clone()).unwrap();
		mmu.set_memory_bank_mode(MemoryBankMode::Mbc2);

		let rom_bank_0 = mmu.get_ram().get_rom_bank_0().clone();

		println!("Enabling RAM bank operations with incorrect address...");
		mmu.store_word(RAM_BANK_0_START + 0x1300, 0b0000_1010);
		assert_eq!(mmu.get_external_ram_bank_operations_enabled(), false, "RAM operations have been enabled");
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");

		println!("Enabling RAM bank operations with correct address...");
		mmu.store_word(RAM_BANK_0_START + 0x1200, 0b0000_1010);
		assert_eq!(mmu.get_external_ram_bank_operations_enabled(), true, "RAM operations are still disabled");
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");

		println!("Disabling RAM bank operations with incorrect address...");
		mmu.store_word(RAM_BANK_0_START + 0x0700, 0b0000_1110);
		assert_eq!(mmu.get_external_ram_bank_operations_enabled(), true, "RAM operations have been disabled");
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");

		println!("Disabling RAM bank operations with correct address...");
		mmu.store_word(RAM_BANK_0_START + 0x0800, 0b0000_1110);
		assert_eq!(mmu.get_external_ram_bank_operations_enabled(), false, "RAM operations are still enabled");
		assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
	}

	#[test]
	fn switch_rom_banks_mbc2() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("loz-la"));
		mmu.load_rom(test_rom.clone()).unwrap();
		mmu.set_memory_bank_mode(MemoryBankMode::Mbc2);

		let rom_bank_0 = mmu.get_ram().get_rom_bank_0().clone();
		let rom_bank_length = rom_bank_0.len();

		let banks_to_check: Vec<u8> = vec![3, 8, 4, 9, 3, 1, 10, 4];

		let banks: HashMap<u8, &[u8]> = banks_to_check
			.iter()
			.map(|number| (*number, &test_rom[rom_bank_length * *number as usize..rom_bank_length * *number as usize + rom_bank_length]))
			.collect();

		for bank_number in banks_to_check {
			println!("Switching to ROM bank {} with incorrect address...", bank_number);
			let bank = banks.get(&bank_number).unwrap();

			mmu.store_word(0x2800, bank_number);
			assert_ne!(mmu.get_rom_bank_number(), bank_number, "The ROM bank number is incorrect");
			assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
			assert_memory_banks_ne(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], bank, &format!("ROM bank {} has changed", bank_number).to_string());

			println!("Switching to ROM bank {} with correct address...", bank_number);
			mmu.store_word(0x2900, bank_number);
			assert_eq!(mmu.get_rom_bank_number(), bank_number, "The ROM bank number is incorrect");
			assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
			assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], bank, &format!("ROM bank {} has changed", bank_number).to_string());
		}
	}

	#[test]
	fn switch_rom_banks_out_of_bounds_mbc2() {
		let mut mmu = MMU::new().build();

		let test_rom = load_test_rom_file(Some("loz-la"));
		mmu.load_rom(test_rom.clone()).unwrap();
		mmu.set_memory_bank_mode(MemoryBankMode::Mbc2);

		let rom_bank_0 = mmu.get_ram().get_rom_bank_0().clone();
		let rom_bank_length = rom_bank_0.len();

		let banks_to_check: Vec<u8> = vec![23, 19];

		let banks: HashMap<u8, &[u8]> = banks_to_check
			.iter()
			.map(|number| *number & 0b000_1111)
			.map(|number| (number, &test_rom[rom_bank_length * number as usize..rom_bank_length * number as usize + rom_bank_length]))
			.collect();

		for bank_number in banks_to_check {
			println!("Switching to ROM bank {} with correct address...", bank_number);
			mmu.store_word(0x2900, bank_number);

			let adjusted_bank_number = bank_number & 0b0000_1111;
			let bank = banks.get(&adjusted_bank_number).unwrap();
			assert_eq!(mmu.get_rom_bank_number(), adjusted_bank_number, "The ROM bank number is incorrect");
			assert_memory_banks_eq(RAM_BANK_0_START, &mmu.get_ram().get_rom_bank_0()[..], &rom_bank_0[..], "ROM bank 0 has changed");
			assert_memory_banks_eq(SWITCHABLE_ROM_BANK_START, &mmu.get_ram().get_switchable_rom_bank()[..], bank, &format!("ROM bank {} has changed", adjusted_bank_number).to_string());
		}
	}
}