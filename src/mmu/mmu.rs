use log;

use super::mem::*;

#[derive(Clone)]
pub struct MMU {
	ram: RAM,
	external_ram: Vec<u8>,
	cartridge_rom: Vec<u8>,

	rom_bank_number: u8,
	ram_bank_number: u8,

	free_write_mode: bool,
	external_ram_bank_operations_enabled: bool,
	memory_bank_mode: MemoryBankMode,
	rom_ram_size_mode: RomRamSizeMode
}

impl MMU {
	pub fn new() -> MMUBuilder {
		return MMUBuilder::new();
	}

	pub fn get_video_ram(&self) -> &[u8] {
		return &self.ram.video_ram;
	}

	pub fn power_up(&mut self) {
		self.store_word(special_registers::TIMER_COUNTER, 0x00); //TIMA
		self.store_word(special_registers::TIMER_MODULE, 0x00); //TMA
		self.store_word(special_registers::TIMER_CONTROL, 0x00); //TAC
		self.store_word(special_registers::SOUND_MODE_1_SWEEP, 0x80); //NR10
		self.store_word(special_registers::SOUND_MODE_1_LENGTH_WAVE, 0xBF); //NR11
		self.store_word(special_registers::SOUND_MODE_1_ENVELOPE, 0xF3); //NR12
		self.store_word(special_registers::SOUND_MODE_1_FREQUENCY_HI, 0xBF); //NR14
		self.store_word(special_registers::SOUND_MODE_2_LENGTH_WAVE, 0x3F); //NR21
		self.store_word(special_registers::SOUND_MODE_2_ENVELOPE, 0x00); //NR22
		self.store_word(special_registers::SOUND_MODE_2_FREQUENCY_HI, 0xBF); //NR24
		self.store_word(special_registers::SOUND_MODE_3_ON_OFF, 0x7F); //NR30
		self.store_word(special_registers::SOUND_MODE_3_LENGTH, 0xFF); //NR31
		self.store_word(special_registers::SOUND_MODE_3_OUTPUT_LEVEL, 0x9F); //NR32
		self.store_word(special_registers::SOUND_MODE_3_FREQUENCY_HI, 0xBF); //NR33
		self.store_word(special_registers::SOUND_MODE_4_LENGTH, 0xFF); //NR41
		self.store_word(special_registers::SOUND_MODE_4_ENVELOPE, 0x00); //NR42
		self.store_word(special_registers::SOUND_MODE_4_POLYNOMIAL_COUNTER, 0x00); //NR43
		self.store_word(special_registers::SOUND_MODE_4_COUNTER_CONSECUTIVE, 0xBF); //NR30
		self.store_word(special_registers::CHANNEL_VOLUME_CONTROL, 0x77); //NR50
		self.store_word(special_registers::SOUND_OUTPUT_TERMINAL, 0xF3); //NR51
		self.store_word(special_registers::SOUND_CONTROL, 0xF1); //NR52
		//self.store_word(SpecialRegisters::SoundControl, 0xF0); // SGB
		self.store_word(special_registers::LCD_CONTROL, 0x91); //LCDC
		self.store_word(special_registers::SCROLL_Y, 0x00); //SCY
		self.store_word(special_registers::SCROLL_X, 0x00); //SCX
		self.store_word(special_registers::LY_COMPARE, 0x00); //LYC
		self.store_word(special_registers::BACKGROUND_AND_PALETTE_DATA, 0xFC); //BGP
		self.store_word(special_registers::OBJECT_PALETTE_0_DATA, 0xFF); //0BP0
		self.store_word(special_registers::OBJECT_PALETTE_1_DATA, 0xFF); //OBP1
		self.store_word(special_registers::WINDOW_Y_POSITION, 0xFF); //WY
		self.store_word(special_registers::WINDOW_X_POSITION, 0x00); //WX
		self.store_word(special_registers::INTERRUPT_ENABLE, 0x00); //IE
	}

	pub fn load_rom(&mut self, rom: Vec<u8>) -> Result<(), String> {
		if rom.len() % 256 != 0 {
			return Err(String::from("Invalid rom size. Make sure the loaded rom size is divisible by 256"));
		}

		self.cartridge_rom = rom;

		self.load_rom_banks();
		self.load_ram_banks();
		self.set_memory_mode();

		return Ok(());
	}

	pub fn load_word(&self, addr: u16) -> u8 {
		let ram = &self.ram;
		let addr = addr as usize;

		log::info(format!("Reading from memory address 0x{:04X}...", addr));

		return match addr {
			0x0000 ... 0x3FFF => ram.rom_bank_0[addr],
			0x4000 ... 0x7FFF => ram.switchable_rom_bank[addr - 0x4000],
			0x8000 ... 0x9FFF => ram.video_ram[addr - 0x8000],
			0xA000 ... 0xBFFF => {
				if self.external_ram_bank_operations_enabled {
					if self.memory_bank_mode == MemoryBankMode::Mbc2 {
						if addr <= 0xA1FF {
							/* Only returns data in lower nibble in this mode */
							ram.switchable_ram_bank[addr - 0xA000] & 0b0000_1111
						} else {
							log::warning(format!("Reading from invalid memory address 0x{:04X} in MBC2 mode", addr));
							0x00
						}
					} else {
						ram.switchable_ram_bank[addr - 0xA000]
					} 
				} else {
					0xFF 
				}
			},
			0xC000 ... 0xDFFF => ram.internal_ram[addr - 0xC000],
			0xE000 ... 0xFDFF => ram.internal_ram_echo[addr - 0xE000],
			0xFE00 ... 0xFE9F => ram.sprite_attrib_mem[addr - 0xFE00],
			0xFEA0 ... 0xFEFF => ram.io_scratchpad_1[addr - 0xFEA0],
			0xFF00 ... 0xFF4B => ram.io_ports[addr - 0xFF00],
			0xFF4C ... 0xFF7F => ram.io_scratchpad_2[addr - 0xFF4C],
			0xFF80 ... 0xFFFE => ram.interrupt_internal_ram[addr - 0xFF80],
			0xFFFF => ram.interrupt_enable_register,
			_ => return 0x00
		};
	}

	pub fn store_word(&mut self, addr: u16, value: u8) {
		let addr = addr as usize;

		log::info(format!("Writing value {} to memory address 0x{:04X}...", value, addr));

		match addr {
			0x0000 ... 0x3FFF => {
				if cfg!(test) && self.free_write_mode {
					self.ram.rom_bank_0[addr] = value;
				}

				match self.memory_bank_mode {
					MemoryBankMode::Mbc1 => self.handle_rom_0_write_mbc1(addr, value),
					MemoryBankMode::Mbc2 => self.handle_rom_0_write_mbc2(addr, value),
					_ => {}
				}
			},
			0x4000 ... 0x7FFF => {
				if cfg!(test) && self.free_write_mode {
					self.ram.switchable_rom_bank[addr - 0x4000] = value;
				}

				match self.memory_bank_mode {
					MemoryBankMode::Mbc1 => self.handle_switchable_rom_write_mbc1(addr, value),
					MemoryBankMode::Mbc2 => self.handle_switchable_rom_write_mbc2(addr, value),
					_ => {}
				}
			},
			0x8000 ... 0x9FFF => self.ram.video_ram[addr - 0x8000] = value,
			0xA000 ... 0xBFFF => {
				if self.external_ram_bank_operations_enabled {
					self.ram.switchable_ram_bank[addr - 0xA000] = value;
				}
			},
			0xC000 ... 0xDFFF => {
				let addr_offset = addr - 0xC000;
				self.ram.internal_ram[addr_offset] = value;

				//The internal ram echo is shorter than the original, so make sure we're not writing out of bounds
				if addr_offset < 7_680 { //Length of internal ram echo
					self.ram.internal_ram_echo[addr_offset] = value;
				}
			},
			0xE000 ... 0xFDFF => {
				let addr_offset = addr - 0xE000;
				self.ram.internal_ram[addr_offset] = value;
				self.ram.internal_ram_echo[addr_offset] = value;
			},
			0xFE00 ... 0xFE9F => self.ram.sprite_attrib_mem[addr - 0xFE00] = value,
			0xFEA0 ... 0xFEFF => self.ram.io_scratchpad_1[addr - 0xFEA0] = value,
			0xFF00 ... 0xFF4B => self.ram.io_ports[addr - 0xFF00] = value,
			0xFF4C ... 0xFF7F => self.ram.io_scratchpad_2[addr - 0xFF4C] = value,
			0xFF80 ... 0xFFFE => self.ram.interrupt_internal_ram[addr - 0xFF80] = value,
			0xFFFF => self.ram.interrupt_enable_register = value,
			_ => return
		}
	}

	fn load_rom_banks(&mut self) {
		let rom_bank_0_length = self.ram.rom_bank_0.len();

		/* Load rom starting with bank 0 */
		for index in 0..rom_bank_0_length {
			self.ram.rom_bank_0[index] = self.cartridge_rom[index];
		}

		/* Load rom bank 1 into switchable rom bank */
		for index in 0..self.ram.switchable_rom_bank.len() {
			self.ram.switchable_rom_bank[index] = self.cartridge_rom[index + rom_bank_0_length];
		}
	}

	fn load_ram_banks(&mut self) {
		let ram_bank_size = self.ram.switchable_ram_bank.len();
		let ram_size = self.cartridge_rom[CartridgeMemoryLocations::RamSize as usize];
		match ram_size {
			s if s == RamSize::KByte2 as u8 => self.external_ram = vec![0; ram_bank_size + 8_192], //Actually 2_064
			s if s == RamSize::KByte8 as u8 => self.external_ram = vec![0; ram_bank_size + 8_192],
			s if s == RamSize::KByte32 as u8 => self.external_ram = vec![0; ram_bank_size + 32_768],
			s if s == RamSize::KByte128 as u8 => self.external_ram = vec![0; ram_bank_size + 131_072],
			_ => {}
		};
	}

	fn set_memory_mode(&mut self) {
		let cartridge_type = self.cartridge_rom[CartridgeMemoryLocations::CartridgeType as usize];
		match cartridge_type {
			t if t == CartridgeType::RomMbc1 as u8 => {
				self.memory_bank_mode = MemoryBankMode::Mbc1;
			},
			t if t == CartridgeType::RomMbc1 as u8 
				|| t == CartridgeType::RomMbc1Ram as u8
				|| t == CartridgeType::RomMbc1RamBatt as u8 => {
				self.memory_bank_mode = MemoryBankMode::Mbc1;
				self.rom_ram_size_mode = RomRamSizeMode::Rom2MByteRam8KByte;
			},	
			t if t == CartridgeType::RomMbc2 as u8
				|| t == CartridgeType::RomMbc2Batt as u8 => {
				self.memory_bank_mode = MemoryBankMode::Mbc2;
			},
			t if t == CartridgeType::RomMbc3 as u8
				|| t == CartridgeType::RomMbc3Ram as u8
				|| t == CartridgeType::RomMbc3RamBatt as u8
				|| t == CartridgeType::RomMbc3TimerBatt as u8
				|| t == CartridgeType::RomMbc3TimerRamBatt as u8 => {
				self.memory_bank_mode = MemoryBankMode::Mbc3;
			},
			t if t == CartridgeType::RomMbc5 as u8
				|| t == CartridgeType::RomMbc5Ram as u8
				|| t == CartridgeType::RomMbc5RamBatt as u8
				|| t == CartridgeType::RomMbc5Rumble as u8
				|| t == CartridgeType::RomMbc5RumbleSram as u8
				|| t == CartridgeType::RomMbc5RumbleSramBatt as u8 => {
				self.memory_bank_mode = MemoryBankMode::Mbc5;
			},
			_ => {
				self.memory_bank_mode = MemoryBankMode::RomOnly
			}
		}
	}

	fn handle_rom_0_write_mbc1(&mut self, addr: usize, value: u8) {
		if addr <= 0x1FFF {
			/* For writes between 0x0000 and 0x1FFF */
			let external_ram_bank_operations_enabled = (value & 0b0000_1111) == 0b0000_1010;

			log::info(format!("Setting RAM bank operations to {}", external_ram_bank_operations_enabled));
			self.external_ram_bank_operations_enabled = external_ram_bank_operations_enabled;
		} else {
			/* For writes between 0x2000 and 0x3FFF */
			let bank_number = value & 0b0001_1111;
			self.switch_rom_bank(bank_number);
		}
	}

	fn handle_rom_0_write_mbc2(&mut self, addr: usize, value: u8) {
		if addr <= 0x1FFF {
			/* For writes between 0x0000 and 0x1FFF */
			if addr & 0x0100 == 0 {
				let external_ram_bank_operations_enabled = (value & 0b0000_1111) == 0b0000_1010;

				log::info(format!("Setting RAM bank operations to {}", external_ram_bank_operations_enabled));
				self.external_ram_bank_operations_enabled = external_ram_bank_operations_enabled;
			} else {
				log::warning(format!("Trying to set RAM operations to {} with invalid address 0x{:04X}", value, addr));
			}		
		} else {
			/* For writes between 0x2000 and 0x3FFF */
			if addr & 0x0100 != 0 {
				let bank_number = value & 0b0000_1111;
				self.switch_rom_bank(bank_number);
			} else {
				log::warning(format!("Trying to set ROM bank to {} with invalid address 0x{:04x}", value, addr));
			}
		}
	}

	fn handle_switchable_rom_write_mbc1(&mut self, addr: usize, value: u8) {
		if addr <= 0x5FFF {
			/* For writes between 0x4000 and 0x5FFF */
			if self.rom_ram_size_mode == RomRamSizeMode::Rom512KByteRam32KByte {
				if self.external_ram_bank_operations_enabled {
					let bank_number = value & 0b0000_0011;
					self.switch_ram_bank(bank_number);
				} else {
					log::warning(String::from("Trying to write to external RAM when such operations disabled"));
				}
			} else { //if self.rom_ram_size_mode == RomRamSizeMode::Rom2MByteRam8KByte
				let bank_number = if self.memory_bank_mode == MemoryBankMode::Mbc2 {
					self.rom_bank_number & 0b0000_1111
				} else {
					(self.rom_bank_number & 0b0001_1111) + ((value & 0b0000_0011) << 5)
				};
				self.switch_rom_bank(bank_number);
			}
		} else {
			/* For writes between 0x6000 and 0x7FFF */
			self.rom_ram_size_mode = if value & 0b0000_0001 == 0 { 
				RomRamSizeMode::Rom2MByteRam8KByte 
			} else { 
				RomRamSizeMode::Rom512KByteRam32KByte 
			};
			log::info(format!("Switching to ROM RAM Size mode {:?}", self.rom_ram_size_mode));
		}
	}

	fn handle_switchable_rom_write_mbc2(&mut self, addr: usize, value: u8) {
		log::warning(format!("Trying to write to ROM bank with value {} at address 0x{:04X} in MBC2 memory mode", value, addr));
	}

	fn switch_rom_bank(&mut self, bank_number: u8) {
		//TODO: Merge with switch_ram_bank
		log::info(format!("Switching to ROM bank {} from bank {}", bank_number, self.rom_bank_number));
		
		let bank_number = if bank_number == 0 
			|| bank_number == 20 
			|| bank_number == 40
			|| bank_number == 60 {
			log::info(format!("Setting ROM bank to inaccessible value {}, so incrementing...", bank_number));
			bank_number + 1 
		} else { 
			bank_number 
		};

		if self.rom_bank_number == bank_number {
			log::info(format!("ROM bank already set to {}, so no switch performed", self.rom_bank_number));
			return;
		}

		let switchable_bank_length = self.ram.switchable_rom_bank.len();
		let switchable_bank_copy = self.ram.switchable_rom_bank.clone();
		
		{
			/* Write into switchable ROM bank */
			let switchable_bank = &mut self.ram.switchable_rom_bank;

			let from_starting_addr = bank_number as usize * switchable_bank_length;
			let from_ending_addr = from_starting_addr + switchable_bank_length;
			let bank_to_write_from = &mut self.cartridge_rom[from_starting_addr..from_ending_addr];
			switchable_bank.copy_from_slice(bank_to_write_from);
		}

		{
			/* Write back to previous ROM bank */
			let to_starting_addr = self.rom_bank_number as usize * switchable_bank_length;
			let to_ending_addr = to_starting_addr + switchable_bank_length;
			let bank_to_write_back_to = &mut self.cartridge_rom[to_starting_addr..to_ending_addr];
			bank_to_write_back_to.copy_from_slice(&switchable_bank_copy[..]);
		}

		self.rom_bank_number = bank_number;
		log::info(format!("Switched to ROM bank {}", self.rom_bank_number));
	}

	fn switch_ram_bank(&mut self, bank_number: u8) {
		//TODO: Merge with switch_rom_bank
		log::info(format!("Switching to RAM bank {} from bank {}", bank_number, self.ram_bank_number));

		if self.ram_bank_number == bank_number || self.external_ram.len() == 0 {
			return;
		}

		let switchable_bank_length = self.ram.switchable_ram_bank.len();
		let switchable_bank_copy = self.ram.switchable_ram_bank.clone();
		println!("Value at bank copy index 0: {}", switchable_bank_copy[0]);

		{
			/* Write into switchable RAM bank */
			let switchable_bank = &mut self.ram.switchable_ram_bank;

			let from_starting_addr = bank_number as usize * switchable_bank_length;
			let from_ending_addr = from_starting_addr + switchable_bank_length;
			let bank_to_write_from = &mut self.external_ram[from_starting_addr..from_ending_addr];
			
			log::info(format!("Writing to switchable bank from bank {} at 0x{:04X}-0x{:04X}...", bank_number, from_starting_addr, from_ending_addr));
			switchable_bank.copy_from_slice(bank_to_write_from);
		}

		{
			/* Write back to previous RAM bank */
			let to_starting_addr = self.ram_bank_number as usize * switchable_bank_length;
			let to_ending_addr = to_starting_addr + switchable_bank_length;
			let bank_to_write_back_to = &mut self.external_ram[to_starting_addr..to_ending_addr];

			log::info(format!("Writing back to original bank {} at 0x{:04X}-0x{:04X}...", self.ram_bank_number, to_starting_addr, to_ending_addr));
			bank_to_write_back_to.copy_from_slice(&switchable_bank_copy[..]);
		}

		self.ram_bank_number = bank_number;
		log::info(format!("Switched to RAM bank {}", self.ram_bank_number));
	}

	/* Methods only available in unit test builds */
	#[cfg(test)] pub fn get_ram(&self) -> &RAM { return &self.ram; }
	#[cfg(test)] pub fn get_ram_mut(&mut self) -> &mut RAM { return &mut self.ram; }
	#[cfg(test)] pub fn get_rom_bank_number(&self) -> u8 { return self.rom_bank_number; }
	#[cfg(test)] pub fn get_ram_bank_number(&self) -> u8 { return self.ram_bank_number; }
	#[cfg(test)] pub fn get_memory_bank_mode(&self) -> MemoryBankMode { return self.memory_bank_mode; }
	#[cfg(test)] pub fn set_memory_bank_mode(&mut self, value: MemoryBankMode) { self.memory_bank_mode = value; }
	#[cfg(test)] pub fn get_rom_ram_size_mode(&self) -> RomRamSizeMode { return self.rom_ram_size_mode; }
	#[cfg(test)] pub fn get_external_ram_bank_operations_enabled(&self) -> bool { return self.external_ram_bank_operations_enabled; }
	#[cfg(test)] pub fn set_external_ram_bank_operations_enabled(&mut self, value: bool) { self.external_ram_bank_operations_enabled = value; } 

	#[cfg(test)] pub fn compare(&self, other: &MMU) -> CompareMemResult { return self.ram.compare(&other.ram); }

	#[cfg(test)]
	pub fn load_test_rom(&mut self, rom: Vec<u8>) {
		self.cartridge_rom = rom;

		for (index, byte) in self.cartridge_rom.iter().enumerate() {
			self.ram.rom_bank_0[index] = *byte;
		}
	}
}

pub struct MMUBuilder {
	free_write_mode: bool
}

impl MMUBuilder {
	pub fn new() -> Self {
		return MMUBuilder {
			free_write_mode: false
		};
	}

	pub fn with_free_write_mode(mut self) -> Self {
		self.free_write_mode = true;

		return self;
	}

	pub fn build(&self) -> MMU {
		return MMU {
			ram: RAM::new(),
			external_ram: Vec::new(),
			cartridge_rom: Vec::new(),

			free_write_mode: self.free_write_mode,

			rom_bank_number: 1,
			ram_bank_number: 0,
			external_ram_bank_operations_enabled: false,
			memory_bank_mode: MemoryBankMode::Mbc1,
			rom_ram_size_mode: RomRamSizeMode::Rom2MByteRam8KByte
		};
	}
}

#[cfg(test)]
pub enum CompareMemResult {
	Identical,
	DifferentLengths(u16, u16),
	DifferentValues(MemAddress, MemAddress)
}

#[cfg(test)]
pub struct MemAddress {
	pub address: u16,
	pub value: u8
}

#[derive(Clone)]
pub struct RAM {
	rom_bank_0: [u8; 16_384],				//0x0000-0x3FFF; Cartridge header bank
	switchable_rom_bank: [u8; 16_384],		//0x4000-0x7FFF; Swappable cartridge rom banks
	video_ram: [u8; 8_192],					//0x8000-0x9FFF
	switchable_ram_bank: [u8; 8_192],		//0xA000-0xBFFF
	internal_ram: [u8; 8_192],				//0xC000-0xDFFF
	internal_ram_echo: [u8; 7_680],			//0xE000-0xFDFF
	sprite_attrib_mem: [u8; 160],			//0xFE00-0xFE9F
	io_scratchpad_1: [u8; 96],				//0xFEA0-0xFEFF
	io_ports: [u8; 76],						//0xFF00-0xFF4B
	io_scratchpad_2: [u8; 52],				//0xFF4C-0xFF7F
	interrupt_internal_ram: [u8; 127],		//0xFF80-0xFFFE
	interrupt_enable_register: u8			//0xFFFF
}

impl RAM {
	pub fn new() -> Self {
		return Self {
			rom_bank_0: [0; 16_384],
			switchable_rom_bank: [0; 16_384],
			video_ram: [0; 8_192],
			switchable_ram_bank: [0; 8_192],
			internal_ram: [0; 8_192],
			internal_ram_echo: [0; 7_680],
			sprite_attrib_mem: [0; 160],
			io_scratchpad_1: [0; 96],
			io_ports: [0; 76],
			io_scratchpad_2: [0; 52],
			interrupt_internal_ram: [0; 127],
			interrupt_enable_register: 0
		};
	}

	/* Methods only available in unit test builds */
	#[cfg(test)] pub fn get_rom_bank_0(&self) -> &[u8; 16_384] { return &self.rom_bank_0; }
	#[cfg(test)] pub fn get_switchable_rom_bank(&self) -> &[u8; 16_384] { return &self.switchable_rom_bank; }
	#[cfg(test)] pub fn get_switchable_ram_bank(&self) -> &[u8; 8_192] { return &self.switchable_ram_bank; }
	#[cfg(test)] pub fn set_switchable_ram_bank(&mut self, value: &[u8; 8_192]) {
		self.switchable_ram_bank[..].copy_from_slice(&value[..]);
	}

	#[cfg(test)]
	pub fn compare(&self, other_mmu: &RAM) -> CompareMemResult {
		let self_memory = self.get_raw_memory();
		let other_memory = other_mmu.get_raw_memory();
		if self_memory.len() != other_memory.len() {
			return CompareMemResult::DifferentLengths(
				self_memory.len() as u16, 
				other_memory.len() as u16
			);
		}

		for index in 0..self_memory.len() {
			if self_memory[index] != other_memory[index] {
				return CompareMemResult::DifferentValues(
					MemAddress { address: index as u16, value: self_memory[index] },
					MemAddress { address: index as u16, value: other_memory[index] }
				);
			}
		}

		return CompareMemResult::Identical;
	}

	#[cfg(test)]
	fn get_raw_memory(&self) -> Vec<u8> {
		let mut result: Vec<u8> = Vec::with_capacity(0xFFFF);
		result.extend(self.rom_bank_0.iter().cloned());
		result.extend(self.switchable_rom_bank.iter().cloned());
		result.extend(self.video_ram.iter().cloned());
		result.extend(self.switchable_ram_bank.iter().cloned());
		result.extend(self.internal_ram.iter().cloned());
		result.extend(self.internal_ram_echo.iter().cloned());
		result.extend(self.sprite_attrib_mem.iter().cloned());
		result.extend(self.io_scratchpad_1.iter().cloned());
		result.extend(self.io_ports.iter().cloned());
		result.extend(self.io_scratchpad_2.iter().cloned());
		result.extend(self.interrupt_internal_ram.iter().cloned());
		result.push(self.interrupt_enable_register);

		return result;
	}
}