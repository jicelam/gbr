pub enum CartridgeMemoryLocations {
	Address00 = 0x0000,
	Address08 = 0x0008,
	Address10 = 0x0010,
	Address18 = 0x0018,
	Address20 = 0x0020,
	Address28 = 0x0028,
	Address30 = 0x0030,
	Address38 = 0x0038,
	VerticalBlankInterrupt = 0x0040,
	LCDCStatusInterrupt = 0x0048,
	TimerOverflowInterrupt = 0x0050,
	SerialTransferCompletionInterrupt = 0x0058,
	HighToLowInterrupt = 0x0060,
	CodeExecutionStart = 0x0100,
	NintendoGraphic = 0x0104,
	GameTitle = 0x0134,
	ColorMode = 0x0143,
	LicenseeCodeHi = 0x0144,
	LicenseeCodeLo = 0x0145,
	SuperGBMode = 0x0146,
	CartridgeType = 0x0147,
	RomSize = 0x0148,
	RamSize = 0x0149,
	DestinationCode = 0x014A,
	LicenseeCodeOld = 0x014B,
	MaskROMVersionNumber = 0x14C,
	ComplementCheck = 0x014D,
	Checksum = 0x014E
}

/* Special cartridge memory locations */

//0x0100-0x0103 -> Begin code execution point
//0x0104-0x0133 -> Scrolling Nintendo graphic
pub static NINTENDO_GRAPHIC: [u8; 48] = [
	0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
	0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
	0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E
];

//0x0134-0x0142 -> Title of the game in UPPER CASE ASCII (remaining space filled with 0's)
//0x0143 -> 0x80 = Color GB, 0x00 or other = not Color GB
//0x0144 -> Ascii hex digit, high nibble of licensee code
//0x0145 -> Ascii hex digit, low nibble of licensee code (normally 0x00 if 0x0014B != 0x33)
//0x0146 -> GB/SGB indicator (0x00 == GameBoy, 0x03 = Super GameBoy)
//0x0147 -> Cartridge type
pub enum CartridgeType {
	RomOnly = 0x00,
	RomMbc1 = 0x01,
	RomMbc1Ram = 0x02,
	RomMbc1RamBatt = 0x03,
	RomMbc2 = 0x05,
	RomMbc2Batt = 0x06,
	RomRam = 0x08,
	RomRamBatt = 0x09,
	RomMmm01 = 0x0B,
	RomMmm01Sram = 0x0C,
	RomMmm01SramBatt = 0x0D,
	RomMbc3TimerBatt = 0x0F,
	RomMbc3TimerRamBatt = 0x10,
	RomMbc3 = 0x11,
	RomMbc3Ram = 0x12,
	RomMbc3RamBatt = 0x13,
	RomMbc5 = 0x19,
	RomMbc5Ram = 0x1A,
	RomMbc5RamBatt = 0x1B,
	RomMbc5Rumble = 0x1C,
	RomMbc5RumbleSram = 0x1D,
	RomMbc5RumbleSramBatt = 0x1E,
	PocketCamera = 0x1F,
	BandaiTama5 = 0xFD,
	HudsonHuc3 = 0xFE,
	HudsonHuc1 = 0xFF
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum MemoryBankMode { RomOnly, Mbc1, Mbc2, Mbc3, Mbc5 }

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum RomRamSizeMode {
	Rom2MByteRam8KByte,
	Rom512KByteRam32KByte
} 

//0x0148 -> ROM size
#[derive(Eq, PartialEq)]
pub enum RomSize {
	KByte32 = 0x00,		//2 banks
	KByte64 = 0x01,		//4 banks
	KByte128 = 0x02,	//8 banks
	KByte256 = 0x03,	//16 banks
	KByte512 = 0x04,	//32 banks
	MByte1 = 0x05,		//64 banks
	MByte2 = 0x06,		//128 banks
	MByte11 = 0x52,		//72 banks
	MByte12 = 0x53,		//80 banks
	MByte15 = 0x54		//96 banks
}

//0x0149 -> RAM size
#[derive(Eq, PartialEq)]
pub enum RamSize {
	None = 0x00,
	KByte2 = 0x01,	//1 bank
	KByte8 = 0x02,	//1 bank
	KByte32 = 0x03,	//4 banks
	KByte128 = 0x04	//16 banks
}

//0x014A -> Destination code
pub enum DestinationCode {
	Japanese = 0x00,
	NonJapanese = 0x01
}

//0x014B -> Licensee code (old) (Super Gameboy won't work if != 0x33)
pub enum LicenseeCodeOld {
	Check0144 = 0x33,
	Accolade = 0x79,
	Konami = 0xA4
}

//0x014C -> Mask ROM version number (usually 0x00)
//0x014D -> Complement check
//0x014E-0x014F -> Checksum (higher byte first) produced by adding all bytes of a cartridge except for two checksum bytes and taking two lower bytes of the result

pub mod mem_ranges {
	pub const RAM_BANK_0_START: u16 = 0x0000;
	pub const SWITCHABLE_ROM_BANK_START: u16 = 0x4000;
	pub const SWITCHABLE_RAM_BANK_START: u16 = 0xA000;
	pub const RAM_START: u16 = 0xC000;
	pub const RAM_END: u16 = 0xDFFF;
	pub const ECHOED_RAM_START: u16 = 0xE000;
	pub const ECHOED_RAM_END: u16 = 0xFDFF;
}

pub mod special_registers {
	pub const JOYPAD_INFO: u16 = 0xFF00; 						//P1
	pub const SERIAL_TRANSFER_DATA: u16 = 0xFF01; 				//SB (8 bits of data to be read/written)
	pub const SERIAL_IO_CONTROL: u16 = 0xFF02; 					//SC
	pub const TIME_DIVIDER: u16 = 0xFF04;						//DIV
	pub const TIMER_COUNTER: u16 = 0xFF05;						//TIMA
	pub const TIMER_MODULE: u16 = 0xFF06;						//TMA
	pub const TIMER_CONTROL: u16 = 0xFF07;						//TAC
	pub const INTERRUPT_FLAG: u16 = 0xFF0F;						//IF
	pub const SOUND_MODE_1_SWEEP: u16 = 0xFF10;					//NR 10
	pub const SOUND_MODE_1_LENGTH_WAVE: u16 = 0xFF11;			//NR 11
	pub const SOUND_MODE_1_ENVELOPE: u16 = 0xFF12;				//NR 12
	pub const SOUND_MODE_1_FREQUENCY_LO: u16 = 0xFF13;			//NR 13
	pub const SOUND_MODE_1_FREQUENCY_HI: u16 = 0xFF14;			//NR 14
	pub const SOUND_MODE_2_LENGTH_WAVE: u16 = 0xFF16;			//NR 21
	pub const SOUND_MODE_2_ENVELOPE: u16 = 0xFF17;				//NR 22
	pub const SOUND_MODE_2_FREQUENCY_LO: u16 = 0xFF18;			//NR 23
	pub const SOUND_MODE_2_FREQUENCY_HI: u16 = 0xFF19;			//NR 24
	pub const SOUND_MODE_3_ON_OFF: u16 = 0xFF1A;				//NR 30
	pub const SOUND_MODE_3_LENGTH: u16 = 0xFF1B;				//NR 31
	pub const SOUND_MODE_3_OUTPUT_LEVEL: u16 = 0xFF1C;			//NR 32
	pub const SOUND_MODE_3_FREQUENCY_LO: u16 = 0xFF1D;			//NR 33
	pub const SOUND_MODE_3_FREQUENCY_HI: u16 = 0xFF1E;			//NR 34
	pub const SOUND_MODE_4_LENGTH: u16 = 0xFF20;				//NR 41
	pub const SOUND_MODE_4_ENVELOPE: u16 = 0xFF21;				//NR 42
	pub const SOUND_MODE_4_POLYNOMIAL_COUNTER: u16 = 0xFF22;	//NR 43
	pub const SOUND_MODE_4_COUNTER_CONSECUTIVE: u16 = 0xFF23;	//NR 44
	pub const CHANNEL_VOLUME_CONTROL: u16 = 0xFF24;				//NR 50
	pub const SOUND_OUTPUT_TERMINAL: u16 = 0xFF25;				//NR 51
	pub const SOUND_CONTROL: u16 = 0xFF26; 						//NR 52
	pub const WAVE_PATTERN_RAM_START: u16 = 0xFF30;				//Wave Pattern RAM (storage for arbitrary sound data)
	pub const WAVE_PATTERN_RAM_END: u16 = 0xFF3F;				//Wave Pattern RAM (from range 0xFF30-0xFF3F)
	pub const LCD_CONTROL: u16 = 0xFF40;						//LCDC
	pub const LCDC_STATUS: u16 = 0xFF41;						//STAT
	pub const SCROLL_Y: u16 = 0xFF42;							//SCY
	pub const SCROLL_X: u16 = 0xFF43;							//SCX
	pub const LCDC_Y_COORDINATE: u16 = 0xFF44;					//LY
	pub const LY_COMPARE: u16 = 0xFF45;							//LYC
	pub const DMA_TRANSFER_START_ADDRESS: u16 = 0xFF46;			//DMA (Direct Memory Access)
	pub const BACKGROUND_AND_PALETTE_DATA: u16 = 0xFF47;		//BGP
	pub const OBJECT_PALETTE_0_DATA: u16 = 0xFF48;				//OBP0
	pub const OBJECT_PALETTE_1_DATA: u16 = 0xFF49;				//OBP1
	pub const WINDOW_Y_POSITION: u16 = 0xFF4A;					//WY
	pub const WINDOW_X_POSITION: u16 = 0xFF4B;					//WX
	pub const INTERRUPT_ENABLE: u16 = 0xFFFF;					//IE
}